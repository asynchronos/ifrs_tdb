﻿
--:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Scripts\Utility\sql_ale_monitor.sql"

--sp_help t_etl_adj_data_crv_curve;

declare @lot_fn_code						d_std_record_function	=	N'ECLSEG'
declare @src_table_name1					d_name					=	N't_nonretail_lgd_output_tbank';
declare @src_table_name2					d_name					=	N''
declare @src_table_name3					d_name					=	N''

--declare @proc_name				sysname		= 'proc_test'
declare @cob_date				datetime	= '2018-03-31'
--declare @max_date				datetime	= '9999-12-31'
--declare @adjustment_id			d_id		= 0
--declare @scenario_context_id	d_id		= 301003
declare @max_lot_id				d_id		= (select max(lot.lot_id) from t_lot_definition lot where lot.std_record_function_code = @lot_fn_code);
--declare @param_scenario_id		d_name		= '3'
--declare @param_scenario_name	d_name		= 'Stage 1 BAU'
--declare @param_stage			d_name		= '1'
--declare @param_src_value_type	d_name		= 'LGD'
--declare @param_src_table_name	d_name		= 't_nonretail_lgd_output_tbank'
--declare @param_src_field_name	d_name		= 'lgd'
--declare @param_model_name		d_name		= 'NonRetail'

--INSERT INTO t_etl_adj_data_crv_curve
--			(adjustment_id, line_date, curve_code, curve_name, curve_desc, curve_type, source_system, curve_value_type, usage_type, curve_reference, forward_nbr_of_time_units, forward_time_unit, currency, day_count_convention, business_day_convention, calendar, retry_count, start_validity_date, end_validity_date, value_perc1, value_amount1, term_start_date1, char_cust_element1, char_cust_element2, char_cust_element3, char_cust_element4, char_cust_element5, num_cust_element1, num_cust_element2, num_cust_element3, num_cust_element4, num_cust_element5, last_modified, modified_by)

select scenario_id, scenario_name, count(distinct s.curve_code) cnt_crv_code
	, count(distinct s.curve_name) cnt_crv_name, count(s.term_start_date1) cnt_term_start_date
from (
	select scenario_id, scenario_name, stage, value_type, TABLE_NAME, src_field_name, model_name,
		CONCAT(ISNULL(seg.element2,CONVERT(VARCHAR,seg.contract_id))
				,'|',stage,'|',scenario_id,'|',value_type)	as curve_code,
		ISNULL(seg.element2,CONVERT(VARCHAR,seg.contract_id)
			+case
				when seg.element3='Marginal' then '-M'
				when seg.element3='Marginal' then '-C'
			else '' end)	as curve_name,
		case
			when ISNULL(seg.element3,'Marginal')='Marginal' then src.lgd
		else 0 end	as value_perc1,
		case
			when seg.element3='Cumulative' then src.lgd
		else 0 end	as value_amount1,
		eomonth(dateadd(month, case when stage=3 then 0 else 1 end,@cob_date))	as term_start_date1
	from t_nonretail_lgd_output_tbank src
		join t_ecl_segmentation seg
		on src.deal_id=seg.deal_tfi_id
		and seg.lot_id=@max_lot_id
		join (
			select se.scenario_id, se.scenario_name, stage, se.value_type, t.TABLE_NAME, se.src_field_name, se.model_name
			from INFORMATION_SCHEMA.TABLES t
				join ( -- create configuration table with inline sql, may move to physical table in the future
					select s.scenario_id, s.scenario_name
						,case
							when CHARINDEX('stage 1',lower(s.scenario_name)) > 0	then	'1'
							when CHARINDEX('stage 2',lower(s.scenario_name)) > 0	then	'2'
							when CHARINDEX('stage 3',lower(s.scenario_name)) > 0	then	'3'
							else 'unknown' end as stage
						,N'LGD' as value_type
						,N'lgd' as src_field_name
						,N'NonRetail' as model_name
					from t_scenario s
					where s.scenario_id in (3,4,5,51,54,55,56,57,110)
				) se on 1=1 
			where t.TABLE_NAME in (@src_table_name1,@src_table_name2,@src_table_name3) -- change product table here, may change to dynamic sql for easy config and mantinance in the future
		) conf on 1=1
	where src.date_tx=@cob_date and src.path='Liquidation'
) s 
group by scenario_id, scenario_name
;

select crv.char_cust_element3 as model_name,crv.curve_value_type, crv.char_cust_element2 as scenario_id
	, count(distinct crv.curve_code) cnt_crv_code, count(distinct crv.curve_name) cnt_crv_name, count(point.term_start_date) cnt_term_start_date
from t_crv_curve crv join t_crv_curve_point_value point
	on crv.curve_id=point.curve_id
--where crv.char_cust_element3=@param_model_name
where crv.source_system = @src_table_name1
group by crv.char_cust_element3,crv.curve_value_type, crv.char_cust_element2
order by 1,2,3
;

select *
from t_nonretail_lgd_output_tbank src
	join t_ecl_segmentation seg
	on src.deal_id=seg.deal_tfi_id
	and seg.lot_id=@max_lot_id
where src.date_tx=@cob_date and src.path='Liquidation'
	and not exists(
		select curve_name
		from t_crv_curve crv
		where crv.source_system = @src_table_name1
			and ISNULL(seg.element2,CONVERT(VARCHAR,seg.contract_id)
				+case
					when seg.element3='Marginal' then '-M'
					when seg.element3='Marginal' then '-C'
				else '' end)=crv.curve_name
	)


