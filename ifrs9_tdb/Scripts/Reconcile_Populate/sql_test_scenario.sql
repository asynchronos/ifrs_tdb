﻿
--:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Scripts\Utility\sql_ale_monitor.sql"

/* Source table list from t_pop_data_config_tbank

model_name                     value_type                     src_table_name
------------------------------ ------------------------------ --------------------------------------------------
CC/FP                          EAD                            t_retail_cc_fp_tbank
CC/FP                          LGD                            t_retail_cc_fp_tbank
CC/FP                          PD                             t_retail_cc_fp_tbank
Constant                       PD/LGD                         t_retail_forward_pd_lgd_tbank
HL                             LGD                            t_stg_hl_pit_lgd_stage1_tbank
HL                             LGD                            t_stg_hl_pit_lgd_stage2_tbank
HL                             PD                             t_retail_fla_hl_pd_stage1_tbank
HL                             PD                             t_retail_fla_hl_pd_stage2_tbank
HP                             EAD                            t_retail_hp_ead_stage1_tbank
HP                             EAD                            t_retail_hp_ead_stage2_tbank
HP                             EAD                            t_retail_hp_ead_stage3_tbank
HP                             LGD                            t_retail_hp_lgd_fla_stage1_tbank
HP                             LGD                            t_retail_hp_lgd_fla_stage2_tbank
HP                             LGD                            t_retail_hp_lgd_tbank
HP                             PD                             t_retail_hp_pd_fla_stage1_tbank
HP                             PD                             t_retail_hp_pd_fla_stage2_tbank
MRTA                           LGD                            t_retail_fla_mrta_lgd_stage1_tbank
MRTA                           LGD                            t_retail_fla_mrta_lgd_stage2_tbank
Master Scale                   PD                             t_nonretail_portfolio_marginal_pit_pd_tbank
NonRetail                      LGD                            t_nonretail_lgd_output_tbank
NonRetail&SMEs                 EAD                            t_nonretail_ead_tbank
NonRetail&SMEs                 PD                             t_nonretail_fla_cumulative_pd_tbank
SMEs                           LGD                            t_nonretail_smes_lgd_output_tbank
SPL                            LGD                            t_retail_fla_spl_lgd_stage1_tbank
SPL                            LGD                            t_retail_fla_spl_lgd_stage2_tbank
Staff-HL                       LGD                            t_retail_fla_staff_lgd_stage1_tbank
Staff-HL                       LGD                            t_retail_fla_staff_lgd_stage2_tbank

*/

-- must change source table at declare section and in from section
declare @adj_table_name						d_name					=	N't_etl_adj_data_crv_curve';
declare @src_table_name1					d_name					=	N't_nonretail_lgd_output_tbank';
declare @src_table_name2					d_name					=	N''
declare @src_table_name3					d_name					=	N''
declare @cob_date							datetime				= '2018-03-31'
declare @lot_fn_code						d_std_record_function	=	N'ECLSEG'
declare @max_lot_id							d_id					= (select max(lot.lot_id) from t_lot_definition lot where lot.std_record_function_code = @lot_fn_code);

-- input table
SELECT scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name
	, COUNT(DISTINCT src.deal_id) AS cnt_dist_deal
	, COUNT(src.deal_id) AS cnt_rows
	, SUM (CASE WHEN seg.element3 in ('Marginal','Maginal') 
			THEN src.lgd
			ELSE 0 END
	  ) AS sum_marginal_Value
	, SUM (CASE WHEN seg.element3 in ('Cumulative') 
			THEN src.lgd
			ELSE 0 END
	  ) AS sum_cumulative_Value
	, COUNT(EOMONTH(DATEADD(MONTH, CASE WHEN stage=3 THEN 0 ELSE 1 END,@cob_date)))	AS cnt_term_start_date
FROM t_nonretail_lgd_output_tbank src -- change table name here, and change at @src_table_name1,@src_table_name2,@src_table_name3
	JOIN t_ecl_segmentation seg
	ON src.deal_id=seg.deal_tfi_id
		AND seg.lot_id=@max_lot_id
	JOIN t_pop_data_config_tbank conf
	ON 1=1 -- in case source table do not have value seperate by stage, scenario
		and conf.src_table_name in (@src_table_name1,@src_table_name2,@src_table_name3)
WHERE src.date_tx=@cob_date and src.path='Liquidation' -- Get only one row for each deal for LGD NonRetail&SMEs
GROUP BY scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name
ORDER BY 1
;
-- staging table
SELECT curve.start_validity_date
		, curve.char_cust_element2 AS scenario_id
		, curve.num_cust_element4 AS stage
		, curve.curve_value_type
		, curve.char_cust_element3 AS segment_name
		, curve.source_system AS src_table_name
		, sum(curve.value_perc1) AS sum_marginal_value
		, sum(curve.value_amount1) AS sum_cumulative_value
		, count(curve.term_start_date1) AS cnt_term_start_date
FROM t_etl_adj_data_crv_curve curve
where @cob_date between curve.start_validity_date and curve.end_validity_date
	and source_system in (@src_table_name1,@src_table_name2,@src_table_name3)
group by curve.start_validity_date
		, curve.char_cust_element2 
		, curve.num_cust_element4 
		, curve.curve_value_type
		, curve.char_cust_element3 
		, curve.source_system 
ORDER BY 4,3,1
;
--select *
--from t_adj_instance
;

-- live table

/*
select scenario_id, scenario_name, count(distinct s.curve_code) cnt_crv_code
	, count(distinct s.curve_name) cnt_crv_name, count(s.term_start_date1) cnt_term_start_date
from (
	select scenario_id, scenario_name, stage, value_type, TABLE_NAME, src_field_name, model_name,
		CONCAT(ISNULL(seg.element2,CONVERT(VARCHAR,seg.contract_id))
				,'|',stage,'|',scenario_id,'|',value_type)	as curve_code,
		ISNULL(seg.element2,CONVERT(VARCHAR,seg.contract_id)
			+case
				when seg.element3='Marginal' then '-M'
				when seg.element3='Marginal' then '-C'
			else '' end)	as curve_name,
		case
			when ISNULL(seg.element3,'Marginal')='Marginal' then src.lgd
		else 0 end	as value_perc1,
		case
			when seg.element3='Cumulative' then src.lgd
		else 0 end	as value_amount1,
		eomonth(dateadd(month, case when stage=3 then 0 else 1 end,@cob_date))	as term_start_date1
	from t_nonretail_lgd_output_tbank src
		join t_ecl_segmentation seg
		on src.deal_id=seg.deal_tfi_id
		and seg.lot_id=@max_lot_id
		join (
			select se.scenario_id, se.scenario_name, stage, se.value_type, t.TABLE_NAME, se.src_field_name, se.model_name
			from INFORMATION_SCHEMA.TABLES t
				join ( -- create configuration table with inline sql, may move to physical table in the future
					select s.scenario_id, s.scenario_name
						,case
							when CHARINDEX('stage 1',lower(s.scenario_name)) > 0	then	'1'
							when CHARINDEX('stage 2',lower(s.scenario_name)) > 0	then	'2'
							when CHARINDEX('stage 3',lower(s.scenario_name)) > 0	then	'3'
							else 'unknown' end as stage
						,N'LGD' as value_type
						,N'lgd' as src_field_name
						,N'NonRetail' as model_name
					from t_scenario s
					where s.scenario_id in (3,4,5,51,54,55,56,57,110)
				) se on 1=1 
			where t.TABLE_NAME in (@src_table_name1,@src_table_name2,@src_table_name3) -- change product table here, may change to dynamic sql for easy config and mantinance in the future
		) conf on 1=1
	where src.date_tx=@cob_date and src.path='Liquidation'
) s 
group by scenario_id, scenario_name
;

select crv.char_cust_element3 as model_name,crv.curve_value_type, crv.char_cust_element2 as scenario_id
	, count(distinct crv.curve_code) cnt_crv_code, count(distinct crv.curve_name) cnt_crv_name, count(point.term_start_date) cnt_term_start_date
from t_crv_curve crv join t_crv_curve_point_value point
	on crv.curve_id=point.curve_id
--where crv.char_cust_element3=@param_model_name
where crv.source_system = @src_table_name1
group by crv.char_cust_element3,crv.curve_value_type, crv.char_cust_element2
order by 1,2,3
;

select *
from t_nonretail_lgd_output_tbank src
	join t_ecl_segmentation seg
	on src.deal_id=seg.deal_tfi_id
	and seg.lot_id=@max_lot_id
where src.date_tx=@cob_date and src.path='Liquidation'
	and not exists(
		select curve_name
		from t_crv_curve crv
		where crv.source_system = @src_table_name1
			and ISNULL(seg.element2,CONVERT(VARCHAR,seg.contract_id)
				+case
					when seg.element3='Marginal' then '-M'
					when seg.element3='Marginal' then '-C'
				else '' end)=crv.curve_name
	)

*/
