
--set transaction isolation level read uncommitted

select 
	l.event_log_guid
	,l.event_log_id
	,l.timestamp
	,c.event_class_id
	,l.event_id
	,e.event_desc
	,c.description as event_class_desc
	,l.spid
	,l.event_type
	,dbo.fn_ale_format_message(
		case when len(ltrim(e.message_template)) < 1 then '$message' 
			else e.message_template +':'+ SUBSTRING(l.context.value('.','varchar(max)'),1,10000) end
		,l.context) as event_msg
	,l.context
	,d.[message]
	,u.username
	,h.hostname
	,a.application
from t_ale_event_log l 
	left join t_ale_event_log_detail d on l.event_log_guid = d.event_log_guid
	inner join t_ale_event e on l.event_id=e.event_id
	inner join t_ale_event_class c on e.event_class_id=c.event_class_id
	left join t_ale_user u on l.ale_user_id=u.ale_user_id
	left join t_ale_host h on l.ale_host_id=h.ale_host_id
	left join t_ale_application a on l.ale_app_id=a.ale_app_id
where l.timestamp between '2018-10-05 13:30' and '2018-10-10'
	--and l.event_id = '60001' -- 'OV_STEP_START' 
	--and e.event_class_id = '30000' 
	--and d.message like 'Start: p_stg_pop_ead_nonretail_smes%'
order by l.timestamp





