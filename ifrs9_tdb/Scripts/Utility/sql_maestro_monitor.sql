
declare @mex_type d_id = '50050';
declare @mex_id d_id;


select m.mex_type,m.version_id,m.mex_type_name
	,l.*
from t_mex_type m 
	join v_mex_location_table l on m.mex_id=l.mex_id
where m.mex_type=@mex_type
;

select s.*
from t_mex_type m 
	join v_mex_step s on m.mex_id=s.mex_id
where m.mex_type=@mex_type
;

select sl.*
from t_mex_type m 
	join v_mex_step_link sl on m.mex_id=sl.mex_id
where m.mex_type=@mex_type
;

select sf.*
from t_mex_type m 
	join t_mex_step_filter sf on m.mex_id=sf.mex_id
where m.mex_type=@mex_type
;

select fm.*
from t_mex_type m 
	join t_mex_field_mapping fm on m.mex_id=fm.mex_id
where m.mex_type=@mex_type
;
