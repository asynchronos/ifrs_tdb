﻿
declare @cob_date	datetime	= '2018-03-31';
declare @max_date	datetime	= '9999-12-31';

select --top 10 
	--new_zacc.*
	new_osx_datasrc_id
	,count(new_zacc.acc_id) as cnt_zacc
	,count(src.deal_id) as cnt_map_cm_src
	,sum(src.filter_out) as cnt_map_cm_src_filter_out
	,count(l.deal_id) as cnt_map_trn_loan
	,count(le.deal_id) as cnt_map_trn_lease
	,count(m.deal_id) as cnt_map_trn_mm
	,count(rp.deal_id) as cnt_map_trn_rp
	,count(validate.deal_id) as cnt_map_validate
from (
	-- mapping deal id by source system
	select
		case 
			when new_osx_datasrc_id IN ('HP','LE','MM','RP','TF')
				then CONCAT(CONVERT(NVARCHAR,new_osx_datasrc_id),osx_acct_no_1)
			when new_osx_datasrc_id = ('CC')
				then CONCAT(N'RV',osx_acct_no_1)
			-- FP, LN, OD, LS
			else	CONCAT(CONVERT(NVARCHAR,new_osx_datasrc_id),osx_acct_no_1,osx_acct_no_2,osx_acct_no_3)
		end as deal_id
		,zacc.*
	from (
		-- mapping osx_datasrc_id='RT' to ifrs source system
		select
			case 
				when z.osx_datasrc_id='RT' and z.prod1_id IN ('D4','BE') then 'BR'
				when z.osx_datasrc_id='RT' and z.prod1_id IN ('SD') then 'PP'
				when z.osx_datasrc_id='RT' and z.prod1_id IN ('FD') then 'FD'
				when z.osx_datasrc_id='RT' and z.prod1_id IN ('MD','DF','ML','DP','MD') then 'MM'
				when z.osx_datasrc_id='RT' and z.prod1_id IN ('CD') then 'NC'
				when z.osx_datasrc_id='RT' and z.prod1_id IN ('PL') then 'RP'
				else z.osx_datasrc_id
			end	as new_osx_datasrc_id
			,z.date_tx, z.acc_id
			, RIGHT(CONCAT('0000000000',z.cus_id),10) as cus_id
			,z.osx_acct_no_1, z.osx_acct_no_2, z.osx_acct_no_3, z.osx_acct_no_4
			,date_close
			, z.datasrc_id, z.curr_id, z.segment_id, z.active_fg, z.major_fg
			, z.prod1_id, z.prod2_id, z.hrdsft_fg, z.osx_datasrc_id
		from t_store_zacc_tbank z
		where date_tx			=	@cob_date
			and z.active_fg		=	'Y'
			and (z.major_fg	is null or z.major_fg = 'Y') -- if CC get only main card
	) as zacc
) as new_zacc
	left join t_trn_loan l
	on new_zacc.date_tx between l.start_validity_date and l.end_validity_date
	and new_zacc.deal_id = l.deal_id
	left join t_trn_leasing le
	on new_zacc.date_tx between le.start_validity_date and le.end_validity_date
	and new_zacc.deal_id = le.deal_id
	left join t_trn_money_market m
	on new_zacc.date_tx between m.start_validity_date and m.end_validity_date
	and new_zacc.deal_id = m.deal_id
	left join t_trn_repo_style rp
	on new_zacc.date_tx between rp.start_validity_date and rp.end_validity_date
	and new_zacc.deal_id = rp.deal_id
	left join ( -- validation report
		select distinct 
			rpt.cob_date, rpt.source_code, rpt.table_name, rpt.deal_id
			,list.xmlDoc.value('count(/*)','int') as validation_cnt
			,SUBSTRING(
				list.xmlDoc.value('.','varchar(max)')
				,3, 10000
			) as [column_list]
		from t_rpt_etl_validation_tbank rpt
		cross apply (
			select ',' + x.validation_desc
						+ '('
						+ x.column_list
						+ ')' as list_item
			from t_rpt_etl_validation_tbank x
			where rpt.cob_date			=	x.cob_date
				and rpt.source_code		=	x.source_code
				and rpt.deal_id			=	x.deal_id
				and rpt.table_name		=	x.table_name
			FOR XML PATH(''), TYPE
		) as list(xmlDoc)
		where rpt.source_code is not null
			and rpt.table_name in ('t_etl_adj_data_trn_loan','t_etl_adj_data_trn_leasing','t_etl_adj_data_trn_money_market')
			--and rpt.table_name not like '%_valuation'
	) validate on new_zacc.date_tx		=	validate.cob_date
		and new_zacc.new_osx_datasrc_id	=	validate.source_code
		and new_zacc.deal_id			=	validate.deal_id
	left join ( 
		-- fp source
		select CONCAT(CONVERT(NVARCHAR,system_code),acct_nr_segment1,acct_nr_segment2,acct_nr_segment3) as deal_id
			,s.system_code, s.asof_date, null as acc_id
			,s.customer_nr, acct_nr_segment1,acct_nr_segment2,acct_nr_segment3,acct_nr_segment4
			,s.close_date
			,case when isnull(s.close_date,@max_date)>=@cob_date 
				then 0 else 1 end as [filter_out]
		from ifrs_source.dbo.t_etl_src_trn_loan_dw_ifrs9_fp_port s
		where 1=1
			and asof_date = @cob_date
		union
		-- ln source
		select CONCAT(CONVERT(NVARCHAR,system_code),acct_nr_segment1,acct_nr_segment2,acct_nr_segment3) as deal_id
			,s.system_code, s.asof_date, null as acc_id
			,s.customer_nr, acct_nr_segment1,acct_nr_segment2,acct_nr_segment3,acct_nr_segment4
			,s.close_date
			,case when isnull(s.close_date,@max_date)>=@cob_date 
						and s.product_code is null
						and (s.amt_principal_outst > 0 or s.status_deal in ('2','9'))
				then 0 else 1 end as [filter_out]
		from ifrs_source.dbo.t_etl_src_trn_loan_dw_ifrs9_ln_port s
			left join ifrs_source.dbo.t_etl_src_product_code_filter f on s.product_code=f.product_code
		where 1=1
			and asof_date = @cob_date
		union
		-- od source
		select CONCAT(CONVERT(NVARCHAR,system_code),acct_nr_segment1,acct_nr_segment2,acct_nr_segment3) as deal_id
			,s.system_code, s.asof_date, null as acc_id
			,s.customer_nr, acct_nr_segment1,acct_nr_segment2,acct_nr_segment3,acct_nr_segment4
			,s.close_date
			,case when isnull(s.close_date,@max_date)>=@cob_date 
						and s.amt_principal_outst > s.amt_deal_principal 
				then 0 else 1 end as [filter_out]
		from ifrs_source.dbo.t_etl_src_trn_loan_dw_ifrs9_od_port s
		where 1=1
			and asof_date = @cob_date
	) src on CONCAT(CONVERT(NVARCHAR,src.system_code),src.acct_nr_segment1,src.acct_nr_segment2,src.acct_nr_segment3)=new_zacc.deal_id
where new_zacc.new_osx_datasrc_id NOT IN ('FD','NC')
	--and l.deal_id is not null
group by new_osx_datasrc_id
;




