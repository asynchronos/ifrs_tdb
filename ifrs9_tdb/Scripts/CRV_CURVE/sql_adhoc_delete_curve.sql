/*

-- delete NonRetail&SMEs EAD
delete t_flow
where exists (
	select flow_set_id
	from t_flow_set
	where (num_cust_element1 in (60,61,62,63,64,65)
			or char_cust_element3 = N'NRT'
		)
		and t_flow_set.flow_set_id=t_flow.flow_set_id
)

delete t_flow_set
where (num_cust_element1 in (60,61,62,63,64,65)
		or char_cust_element3 = N'NRT'
	)

-- ead from custom table
EXEC p_stg_pop_ead_nonretail_smes @param_reporting_date   = '2018-03-31',
                                  @param_is_test          = 0 

-- move NonRetail from adjustment_id 11 to live table
----declare @cob_date datetime	= '2018-03-31';
----declare @adjustment_id d_id = 11; -- NRT
EXEC p_etl_adj_data_flow_tbank	@execution_date	= '2018-03-31',
								@cleanup_type = 2,
								@cleanup_staging = 0,
								@only_cleanup_changed = 1,
								@use_delta_load = 1, 
								@adjustment_id	= 11,--@adjustment_id,
								@entity			= N'TBANK',  
								@log_output     = 0
;

*/

/*

delete t_crv_curve_point_value
where exists (
	select curve_id
	from t_crv_curve 
	where curve_type=9
		and source_system in ('t_nonretail_lgd_output_tbank','t_nonretail_smes_lgd_output_tbank')
		and t_crv_curve.curve_id=t_crv_curve_point_value.curve_id
)


delete t_crv_curve 
where curve_type=9
	and source_system in ('t_nonretail_lgd_output_tbank','t_nonretail_smes_lgd_output_tbank')
	
	
EXEC p_stg_pit_lgd_nonretail @param_reporting_date   = '2018-03-31',
                             @param_is_test          = 0 
							 
EXEC p_stg_pit_lgd_smes		 @param_reporting_date   = '2018-03-31',
                             @param_is_test          = 0 

*/



EXEC p_stg_pit_lgd_nonretail @param_reporting_date   = '2018-03-31',
                             @param_is_test          = 1 