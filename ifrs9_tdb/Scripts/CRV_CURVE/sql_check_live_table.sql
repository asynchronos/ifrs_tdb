﻿--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

/*

declare @adj d_id = 2507;

select adj.adjustment_id, adj.type_name, adj.comment, adj.adjustment_date, adj.reporting_date, count(c.line_nr) as rows_in_etl
from t_etl_adj_data_crv_curve c
	join (
		select i.adjustment_id, t.type_name, i.comment, i.adjustment_date, i.reporting_date
		from t_adj_instance i join t_adj_type t
			on i.type_id = t.type_id
			and t.type_name='t_etl_adj_data_crv_curve'
		where i.adjustment_id >= @adj
	) adj on c.adjustment_id = adj.adjustment_id
group by adj.adjustment_id, adj.type_name, adj.comment, adj.adjustment_date, adj.reporting_date
order by adj.adjustment_id
;

select FORMATMESSAGE('Inserted %s curve to t_crv_curve and %s curve point to t_crv_curve_point_value' ,FORMAT(count(distinct crv.curve_code),'N0'), FORMAT(count(p.curve_point_value_id),'N0')) as result
		--crv.curve_id, crv.curve_code, crv.curve_name, p.term_start_date, p.value_perc, p.value_amount
from t_crv_curve crv join
	t_crv_curve_point_value p on crv.curve_id=p.curve_id
where exists (
	select etl.curve_code
	from t_etl_adj_data_crv_curve etl
	where etl.adjustment_id>=@adj
		and etl.curve_code=crv.curve_code
)
;

select curve_value_type, segment
	, case  when total_points<=12 then '1. <= 1 years.'
			when total_points<=24 then '2. 1 - 2 years.'
			when total_points<=36 then '3. 2 - 3 years.'
			when total_points<=48 then '4. 3 - 4 years.'
			when total_points<=60 then '5. 4 - 5 years.'
			else '6. >  5 years.' end as max_point
	, FORMAT(count(curve_code),'N0') as cnt_curve
	, FORMAT(sum(total_points),'N0') as tot_curve_point
from (
	select crv.curve_value_type, crv.curve_code, count(p.curve_point_value_id) as total_points
		,crv.char_cust_element3 as segment
			--crv.curve_id, crv.curve_code, crv.curve_name, p.term_start_date, p.value_perc, p.value_amount
	from t_crv_curve crv join
		t_crv_curve_point_value p on crv.curve_id=p.curve_id
	where exists (
		select etl.curve_code
		from t_etl_adj_data_crv_curve etl
		where etl.adjustment_id>=@adj
			and etl.curve_code=crv.curve_code
	)
	group by crv.curve_value_type,crv.char_cust_element3, crv.curve_code
) cnt
group by curve_value_type,segment
	, case  when total_points<=12 then '1. <= 1 years.'
			when total_points<=24 then '2. 1 - 2 years.'
			when total_points<=36 then '3. 2 - 3 years.'
			when total_points<=48 then '4. 3 - 4 years.'
			when total_points<=60 then '5. 4 - 5 years.'
			else '6. >  5 years.' end
order by curve_value_type,segment
;

*/

--EXEC [dbo].[p_etl_adj_xtr_crv_curve]
--                            @portfolio_date = '2018-03-31',
--                            @del_data_after_loading = 0,
--                            @div_rate_by_100 = 0,
--                            @adjustment_id = 186,
--                            @debug = 1

--select top 10 *
--from t_etl_adj_data_crv_curve c
--where c.adjustment_id = 158

/*
--check dup curve_name
select c.curve_name,c.curve_value_type,c.char_cust_element2,c.char_cust_element3, count(*), min(c.curve_id), max(c.curve_id),min(last_modified),max(last_modified)
from t_crv_curve c
group by c.curve_name,c.curve_value_type,c.char_cust_element2,c.char_cust_element3
having count(*) > 1
order by 4
;
*/

/*
-- wait confirm for source table
select *
from t_crv_curve
where curve_name like 'RV101809%'
order by 1
*/


/*
select crv.char_cust_element3, crv.curve_value_type, crv.char_cust_element2, crv.adjustment_id, crv.source_system
	, count(distinct crv.curve_name) cnt_curve_name, count(distinct crv.curve_code) cnt_curve_code
	, count(crv.term_start_date1) cnt_term_start_date, min(crv.term_start_date1) min_term_start_date, max(crv.term_start_date1) max_term_start_date
	, sum(crv.value_perc1) sum_val_perc, sum(crv.value_amount1) sum_val_amt
	, min(crv.line_date) min_line_date, max(crv.line_date) max_line_date
from t_etl_adj_data_crv_curve crv
where 1=1
group by crv.char_cust_element3, crv.curve_value_type, crv.char_cust_element2, crv.adjustment_id, crv.source_system
order by crv.char_cust_element3, crv.curve_value_type, crv.char_cust_element2, crv.adjustment_id
;


select char_cust_element3, curve_value_type, char_cust_element2, source_system
	, count(distinct c.curve_name) cnt_curve_name, count(distinct curve_code) cnt_curve_code
	, count(term_start_date) cnt_term_start_date, min(term_start_date) min_term_start_date, max(term_start_date) max_term_start_date
	, sum(value_perc) sum_val_perc, sum(value_amount) sum_val_amt
from t_crv_curve c join t_crv_curve_point_value v
	on c.curve_id = v.curve_id
where 1=1
group by char_cust_element3, curve_value_type, char_cust_element2, source_system
;




select crv.curve_name,crv.curve_type, crv.char_cust_element2, count(crv.curve_name) cnt_rows, count(distinct crv.curve_name) cnt_dist
from t_crv_curve crv
group by crv.curve_name,crv.curve_type, crv.char_cust_element2
having count(crv.curve_name) > 1

*/

