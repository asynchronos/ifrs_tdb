
 --test etl pit lgd
 /*
EXEC p_stg_pit_pd @param_reporting_date   = '2018-03-31',
                   @param_src_table_name   = N't_stg_hl_pit_pd_stage2_tbank',
                   @param_scenario_id      = N'110',
                   @param_scenario_name    = N'Stage 3 Specific Treatment Group',
                   @param_stage			   = N'stage2',
                   @param_is_test          = 1 
*/

/*

select adj.adjustment_id, adj.type_name, adj.comment, adj.adjustment_date, adj.reporting_date, count(c.line_nr) as rows_in_etl
from t_etl_adj_data_crv_curve c
	join (
		select i.adjustment_id, t.type_name, i.comment, i.adjustment_date, i.reporting_date
		from t_adj_instance i join t_adj_type t
			on i.type_id = t.type_id
			and t.type_name='t_etl_adj_data_crv_curve'
		where i.adjustment_date > '2018-08-24'
			and i.comment <> 'TBANK'
	) adj on c.adjustment_id = adj.adjustment_id
group by adj.adjustment_id, adj.type_name, adj.comment, adj.adjustment_date, adj.reporting_date

select convert(nvarchar(10),crv.last_modified,121) as last_modified, count(*) as rows_in_live
from t_crv_curve crv
where crv.last_modified > '2018-08-24'
group by convert(nvarchar(10),crv.last_modified,121)

*/
/*

select char_cust_element3, curve_value_type, count(distinct c.curve_name) cnt_deal, count(distinct c.curve_id) cnt_curve_id, count(v.curve_point_value_id) cnt_curve_point
from t_crv_curve c join t_crv_curve_point_value v
	on c.curve_id = v.curve_id
group by char_cust_element3, curve_value_type
;

*/
--select distinct c.char_cust_element3,curve_type
--from t_crv_curve c


-- delete curve
--EXEC p_stg_pit_del_crv @param_reporting_date   = '2018-03-31',
--                       @param_crv_type         = 8,
--                       @param_is_test          = 0
--;
		
			
-- TM
--EXEC p_stg_pit_pd_cc			@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_pd_fp			@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_pd_spl			@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_pd_upl			@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_pd_hpju			@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

-- END TM


-- PD by deal_id
EXEC p_stg_pit_pd_cc_fp			@param_reporting_date   = '2018-03-31',
								@param_is_test          = 1 
;

--EXEC p_stg_pit_pd_constant	@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

EXEC p_stg_pit_pd_hl			@param_reporting_date   = '2018-03-31',
								@param_is_test          = 1 
;

EXEC p_stg_pit_pd_hp			@param_reporting_date   = '2018-03-31',
								@param_is_test          = 1 
;

EXEC p_stg_pit_pd_master_scale @param_reporting_date   = '2018-03-31',
                               @param_is_test          = 1
;

--EXEC p_stg_pit_pd_hp_cyc		@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;
	   
--EXEC p_stg_pit_pd_hp_juristics	@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_pd_hp_new		@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_pd_hp_use		@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_pd_smes			@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

