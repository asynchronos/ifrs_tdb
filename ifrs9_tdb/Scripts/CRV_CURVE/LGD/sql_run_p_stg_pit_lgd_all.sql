
 --test etl pit lgd
 /*
EXEC p_stg_pit_lgd @param_reporting_date   = '2018-03-31',
                   @param_src_table_name   = N't_stg_hl_pit_lgd_stage2_tbank',
                   @param_scenario_id      = N'110',
                   @param_scenario_name    = N'Stage 3 Specific Treatment Group',
                   @param_stage			   = N'stage2',
                   @param_is_test          = 1 
*/

/*

select adj.adjustment_id, adj.type_name, adj.comment, adj.adjustment_date, adj.reporting_date, count(c.line_nr) as rows_in_etl
from t_etl_adj_data_crv_curve c
	join (
		select i.adjustment_id, t.type_name, i.comment, i.adjustment_date, i.reporting_date
		from t_adj_instance i join t_adj_type t
			on i.type_id = t.type_id
			and t.type_name='t_etl_adj_data_crv_curve'
		where i.adjustment_date > '2018-08-24'
			and i.comment <> 'TBANK'
	) adj on c.adjustment_id = adj.adjustment_id
group by adj.adjustment_id, adj.type_name, adj.comment, adj.adjustment_date, adj.reporting_date

select convert(nvarchar(10),crv.last_modified,121) as last_modified, count(*) as rows_in_live
from t_crv_curve crv
where crv.last_modified > '2018-08-24'
group by convert(nvarchar(10),crv.last_modified,121)

*/

-- delete lgd by product
/*
declare @cob_date		datetime		= '2018-03-31';
declare @max_date		datetime		= '9999-12-31';
declare @crv_type		d_curve_type	= 9; -- 8=PD, 9=LGD
-- 1.1 delete t_crv_curve_point_value by curve_id
DELETE t_crv_curve_point_value
--select count(*) from t_crv_curve_point_value
WHERE exists(
	select c.curve_id -- select curve_id for remove t_crv_curve_point value
	from t_crv_curve c
	where c.source_system NOT IN ('FS','PiT PD : Transition Matrix Assignment') -- 'FS'=OSX Stardard,'PiT PD : Transition Matrix Assignment'=WK's custom rule t_mex_type 11650006
	and c.curve_type		= @crv_type
	and c.curve_begin_date	= @cob_date
	and c.char_cust_element3 in ('CC','FP') -- ****** product
    -- exists 
	and c.curve_id=t_crv_curve_point_value.curve_id
)

-- 2.1 delete t_crv_curve by curve_id
DELETE t_crv_curve
--select count(*) from t_crv_curve
WHERE source_system NOT IN ('FS','PiT PD : Transition Matrix Assignment') -- 'FS'=OSX Stardard,'PiT PD : Transition Matrix Assignment'=WK's custom rule t_mex_type 11650006
	and curve_type			= @crv_type
	and curve_begin_date	= @cob_date
	and char_cust_element3 in ('CC','FP') -- ****** product
*/

---- delete curve
--EXEC p_stg_pit_del_crv @param_reporting_date   = '2018-03-31',
--                       @param_crv_type         = 9,
--                       @param_is_test          = 0
--;



--EXEC p_stg_pit_lgd_cc			@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

EXEC p_stg_pit_lgd_cc_fp		@param_reporting_date   = '2018-03-31',
								@param_is_test          = 1 
;

--EXEC p_stg_pit_lgd_constant		@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_lgd_fp			@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;
	   
EXEC p_stg_pit_lgd_hl			@param_reporting_date   = '2018-03-31',
								@param_is_test          = 1 
;
	   
EXEC p_stg_pit_lgd_hp			@param_reporting_date   = '2018-03-31',
								@param_is_test          = 1 
;

EXEC p_stg_pit_lgd_nonretail	@param_reporting_date   = '2018-03-31',
								@param_is_test          = 1 
;

--EXEC p_stg_pit_lgd_hp_cyc		@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_lgd_hp_new		@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_lgd_hp_use		@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_lgd_mrta			@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_lgd_spl			@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

--EXEC p_stg_pit_lgd_staff		@param_reporting_date   = '2018-03-31',
--								@param_is_test          = 0 
--;

