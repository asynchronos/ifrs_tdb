


declare @adj d_id --= 4897;
select @adj = min(i.adjustment_id)
from t_adj_instance i join t_adj_type t
			on i.type_id = t.type_id
			and t.type_name='t_etl_adj_data_flow_set_flow'
where i.reporting_date = '2018-04-30';


select adj.adjustment_id, adj.type_name, adj.comment, adj.adjustment_date, adj.reporting_date, count(fsf.line_nr) as rows_in_etl
from t_etl_adj_data_flow_set_flow fsf
	join (
		select i.adjustment_id, t.type_name, i.comment, i.adjustment_date, i.reporting_date
		from t_adj_instance i join t_adj_type t
			on i.type_id = t.type_id
			and t.type_name='t_etl_adj_data_flow_set_flow'
		where i.adjustment_id >= @adj
	) adj on fsf.adjustment_id = adj.adjustment_id
group by adj.adjustment_id, adj.type_name, adj.comment, adj.adjustment_date, adj.reporting_date
order by adj.adjustment_id
;

select FORMATMESSAGE('Inserted %s curve to t_flow_set and %s curve point to t_flow' ,FORMAT(count(distinct fs.object_key_value),'N0'), FORMAT(count(f.flow_date),'N0')) as result
		--fs.flow_set_id, fs.object_key_value, fs.curve_name, f.term_start_date, f.value_perc, f.value_amount
from t_flow_set fs join
	t_flow f on fs.flow_set_id=f.flow_set_id
where exists (
	select etl.object_key_value
	from t_etl_adj_data_flow_set_flow etl
	where etl.adjustment_id>=@adj
		and etl.object_key_value=fs.object_key_value
)
;

select flow_type, segment
	, case  when total_points<=12 then '1. <= 1 years.'
			when total_points<=24 then '2. 1 - 2 years.'
			when total_points<=36 then '3. 2 - 3 years.'
			when total_points<=48 then '4. 3 - 4 years.'
			when total_points<=60 then '5. 4 - 5 years.'
			else '6. >  5 years.' end as max_point
	, FORMAT(count(object_key_value),'N0') as cnt_curve
	, FORMAT(sum(total_points),'N0') as tot_curve_point
from (
	select fs.flow_type, fs.object_key_value, count(f.flow_date) as total_points
		,fs.char_cust_element3 as segment
			--fs.flow_set_id, fs.object_key_value, fs.curve_name, f.term_start_date, f.value_perc, f.value_amount
	from t_flow_set fs join
		t_flow f on fs.flow_set_id=f.flow_set_id
	where exists (
		select etl.object_key_value
		from t_etl_adj_data_flow_set_flow etl
		where etl.adjustment_id>=@adj
			and etl.object_key_value=fs.object_key_value
	)
	group by fs.flow_type,fs.char_cust_element3, fs.object_key_value
) cnt
group by flow_type,segment
	, case  when total_points<=12 then '1. <= 1 years.'
			when total_points<=24 then '2. 1 - 2 years.'
			when total_points<=36 then '3. 2 - 3 years.'
			when total_points<=48 then '4. 3 - 4 years.'
			when total_points<=60 then '5. 4 - 5 years.'
			else '6. >  5 years.' end
order by flow_type,segment
;



--EXEC [dbo].[p_etl_adj_xtr_fs_curve]
--                            @portfolio_date = '2018-03-31',
--                            @del_data_after_loading = 0,
--                            @div_rate_by_100 = 0,
--                            @adjustment_id = 186,
--                            @debug = 1

--select top 10 *
--from t_etl_adj_data_flow_set_flow c
--where fsf.adjustment_id = 158
/*

select flow_set_char_cust_element3,adjustment_id, count(*) cnt_rows
from t_etl_adj_data_flow_set_flow fsf
group by flow_set_char_cust_element3,adjustment_id

*/



/*


select fsf.flow_set_char_cust_element3, fsf.flow_type, fsf.adjustment_id
	, count(distinct fsf.object_key_value) cnt_obj_key_value
	, count(fsf.flow_date) cnt_flow_date, count(distinct fsf.flow_date) cnt_dist_flow_date
	, min(fsf.flow_date) min_flow_date, max(fsf.flow_date) max_flow_date, sum(fsf.amount) sum_flow_amt
	, min(fsf.line_date) min_line_date, max(fsf.line_date) max_line_date
from t_etl_adj_data_flow_set_flow fsf (nolock)
where 1=1
group by fsf.flow_set_char_cust_element3, fsf.flow_type, fsf.adjustment_id
order by fsf.flow_set_char_cust_element3, fsf.flow_type, fsf.adjustment_id
;

select fs.char_cust_element3, fs.flow_type
	, count(distinct fs.object_key_value) cnt_obj_key_value
	, count(f.flow_date) cnt_flow_date, count(distinct f.flow_date) cnt_dist_flow_date
	, min(f.flow_date) min_flow_date, max(f.flow_date) max_flow_date, sum(f.flow_amount) sum_flow_amt
from t_flow_set fs (nolock) join t_flow f (nolock)
	on fs.flow_set_id = f.flow_set_id
where 1=1
group by fs.char_cust_element3, fs.flow_type
;


select fs.object_key_value, fs.flow_type, count(fs.object_key_value) cnt_rows, count(distinct fs.object_key_value) cnt_dist
from t_flow_set fs
group by fs.object_key_value, fs.flow_type
having count(fs.object_key_value) > 1

*/