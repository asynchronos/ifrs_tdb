

select *
from t_pool
;

select count(*)
from t_flow_set fs
where exists(
	select distinct seg.item_object_key_value
	from t_ecl_segmentation seg
	where seg.segment_pool_id in (8,9) -- delete by pool
		and seg.item_object_key_value=fs.object_key_value
)

/*
-- delete t_flow of product xx,yy,zz
delete t_flow
where exists(
	select fs.flow_set_id
	from t_flow_set fs
	where exists(
		select distinct seg.item_object_key_value
		from t_ecl_segmentation seg
		where seg.segment_pool_id in (8,9) -- delete by pool
			and seg.item_object_key_value=fs.object_key_value
	) 
	and fs.flow_set_id=t_flow.flow_set_id
)
;
-- delete t_flow of product xx,yy,zz
delete t_flow_set
where exists(
	select distinct seg.item_object_key_value
	from t_ecl_segmentation seg
	where seg.segment_pool_id in (8,9) -- delete by pool
		and seg.item_object_key_value=t_flow_set.object_key_value
)
*/

--EXEC p_stg_pop_ead_cc_fp @param_reporting_date   = '2018-03-31',
--                         @param_is_test          = 0 


/*

delete t_crv_curve_point_value
where exists(
	select c.curve_id
	from t_crv_curve c
	where source_system in ('t_result_forward_looking_transition_matr','t_result_pd_weighting_transition_matrix_','t_retail_forward_pd_lgd_tbank')
		and c.curve_id=t_crv_curve_point_value.curve_id
) 

delete t_crv_curve 
where source_system in ('t_result_forward_looking_transition_matr','t_result_pd_weighting_transition_matrix_','t_retail_forward_pd_lgd_tbank')

*/

EXEC p_stg_pit_pdlgd_constant @param_reporting_date   = '2018-03-31',
                              @param_is_test          = 0 
