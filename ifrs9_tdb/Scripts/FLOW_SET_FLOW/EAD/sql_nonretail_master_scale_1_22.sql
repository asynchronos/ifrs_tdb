
-- curve_code = MS1|NRT|stage|scenario|PD

--select top 10 *
--from t_nonretail_portfolio_marginal_pit_pd_tbank pd
--where as_of_date = '2018-04-30'

--select * from t_scenario where scenario_context_id = '301003';

-- MS1 9 scenario looping to MS22
select --top 100 
	CONCAT(N'NRT','-','MS',1,'|',se.stage,'|',se.scenario_id,'|','PD') as curve_code
	,case
		when se.scenario_id in (51,57)			then	ms1_ttc_pd
		when se.scenario_id in (3,4,5,54,55,56)	then	ms1_pit_pd
		when se.scenario_id in (110)			then	1.0
		else 0.0 end as value_perc1
	,eomonth(dateadd(month,forecast_month,as_of_date)) as term_start_date1
	, se.*
	, src.*
from t_nonretail_portfolio_marginal_pit_pd_tbank src
	join (
		select s.scenario_id, s.scenario_name
			,case
				when s.scenario_id in (3,4,5,51)	then	'1'
				when s.scenario_id in (54,55,56,57)	then	'2'
				when s.scenario_id in (110)			then	'3'
				else 'unknown' end as stage
			,'PD' as value_type
			,case
				when s.scenario_id in (51,57)			then	'ms1_ttc_pd'
				when s.scenario_id in (3,4,5,54,55,56)	then	'ms1_pit_pd'
				when s.scenario_id in (110)				then	'default=1'
				else 'unknown' end as src_field_name
			,case
				when s.scenario_id in (5,56)		then	'SCN001'
				when s.scenario_id in (3,54)		then	'SCN002'
				when s.scenario_id in (4,55)		then	'SCN003'
				else 'NON FL' end as scenario_id_tbank
		from t_scenario s
		where s.scenario_id in (3,4,5,51,54,55,56,57,110)
	) se on 1=1
		and se.scenario_id_tbank = src.scenario_id
		or (src.scenario_id='SCN001' and se.scenario_id_tbank = 'NON FL') -- get non FL
where as_of_date = '2018-03-31'
	and (
			(stage=1 and forecast_month between 1 and 12)	-- stage 1 use T1 - T12
			or (stage=2 and forecast_month > 0)				-- stage 2 use T1 - Txx(180)
			or (stage=3 and forecast_month = 0)				-- stage 3 use T0
		)
order by 1,3
;


--select top 10 *
--from t_ecl_segmentation
--where element1 is not null

--t_pool