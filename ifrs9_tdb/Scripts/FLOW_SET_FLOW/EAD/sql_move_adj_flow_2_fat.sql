

--/*

--515	Stage 1 BAU
--516	Stage 1 DOWNTURN
--517	Stage 1 UPTURN
--518	Stage 1 NON FL
--519	Stage 2 Specific Treatment Group BAU
--520	Stage 2 Specific Treatment Group DOWNTURN
--521	Stage 2 Specific Treatment Group UPTURN
--522	Stage 2 Specific Treatment Group NON FL
--523	Stage 3 Specific Treatment Group

--*/



--DELETE ifrs_fat_fsdb.dbo.t_etl_adj_data_flow_set_flow
--WHERE adjustment_id = 1
--;


--INSERT INTO ifrs_fat_fsdb.dbo.t_etl_adj_data_flow_set_flow
--       (adjustment_id, line_nr, flag, line_status, retry_count, line_date, entity, object_origin, object_key_type, object_key_value, flow_type, flow_set_id, scenario_id, amount_type, start_validity_date, end_validity_date, calculation_date, flow_date, currency, amount, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5, last_modified, modified_by)
--SELECT adjustment_id, line_nr, flag, line_status, retry_count, line_date, entity, object_origin, object_key_type, object_key_value, flow_type, flow_set_id
--	, case scenario_id -- change scenario to stage 
--		when 518 then 1
--		when 522 then 2
--		when 523 then 3
--		else scenario_id end as scenario_id
--	, amount_type, start_validity_date, end_validity_date, calculation_date, flow_date, currency, amount, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5, last_modified, modified_by
--from ifrs_fsdb.dbo.t_etl_adj_data_flow_set_flow
--where adjustment_id = 1
--	and scenario_id in (518,522,523) -- NON FL 1, 2, 3
--;


-----
--select distinct adjustment_id
--from t_etl_adj_data_flow_set_flow fsf
--where adjustment_id = 1

---- use only exists in curve (HL)
--select count(*)
--from t_etl_adj_data_flow_set_flow fsf 
--where adjustment_id = 1
--	and exists (
--		select distinct c.curve_name
--		from t_crv_curve c
--		where c.char_cust_element3 = 'HL'
--			-- exists
--			and replace(fsf.object_key_value,'|TBANK','') = c.curve_name
--	)
--;

---- update char_cust_element3 to HL
---- best way is link to t_store_zacc for get the real segment
--update t_etl_adj_data_flow_set_flow
--set flow_set_char_cust_element3 = ''
--where adjustment_id = 1
--	and not exists (
--		select distinct c.curve_name
--		from t_crv_curve c
--		where c.char_cust_element3 = 'HL'
--			-- exists
--			and replace(t_etl_adj_data_flow_set_flow.object_key_value,'|TBANK','') = c.curve_name
--	)
--;

---- move non HL to adjustment_id 11
--update t_etl_adj_data_flow_set_flow
--set adjustment_id = 11
--where adjustment_id = 1
--	and flow_set_char_cust_element3 <> 'HL'

---- move HL calculation_date 2018-04-30 to adjustment_id 14
--delete t_etl_adj_data_flow_set_flow
--where adjustment_id = 14
--;
--update t_etl_adj_data_flow_set_flow
--set adjustment_id = 14
--where adjustment_id = 1
--	and calculation_date = '2018-04-30'
--	and flow_set_char_cust_element3 = 'HL'
--;

---- check total point of 2018-03-31
--select count(*)
--from t_etl_adj_data_flow_set_flow fsf
--where adjustment_id = 1
--;

---- check stage 3 must have 1 plot

--select object_key_value, flow_type, scenario_id, min(flow_date) min_flow_date, max(flow_date) max_flow_date, count(flow_date) cnt_flow
--	--top 10
--	--case flow_type 
--	--	when N'EAD_1' then 1
--	--	when N'EAD_2' then 2
--	--	when N'EAD_3' then 3
--	--	else 0 end as stage -- replace scenario_id
--	--, src.*
--from t_etl_adj_data_flow_set_flow src
--where adjustment_id = 1 
--	--and (flow_date <> calculation_date or flow_type = N'EAD_3')
--	and flow_type = N'EAD_3'
--	and calculation_date = '2018-03-31'
--group by object_key_value, flow_type, scenario_id
--having count(flow_date) > 1

---- check dup by deal
--select *
--from t_etl_adj_data_flow_set_flow fs
--where adjustment_id = 1
--	and flow_type = 'EAD_3'
--	and object_key_value = 'LN0179012000402000000000000000000000000000000000000000000000|TBANK'


---- check dup flow_date if re-run more than 1 time
--select count(*)
--from (
--	select --src.object_key_value, src.line_nr,
--		row_number() over(partition by adjustment_id, object_key_value, flow_type, flow_date order by line_date desc) seq
--		,src.*
--	from t_etl_adj_data_flow_set_flow src
--	where adjustment_id = 1
--		--and flow_type <> N'EAD_3'
--		--and object_key_value = 'LN0048017000275000000000000000000000000000000000000000000000|TBANK'
--		--and object_key_value = 'FP0013484000510016101840MRHFC6620JT100478|TBANK'
--) check_dup
--where seq <> 1

---- delete dup flow_date if re-run more than 1 time
--delete t_etl_adj_data_flow_set_flow
----select count(*) from t_etl_adj_data_flow_set_flow
--where exists (	
--	select object_key_value
--	from ( 
--		select src.object_key_value, src.adjustment_id, src.line_nr
--			,row_number() over(partition by adjustment_id, object_key_value, flow_type, flow_date order by line_date desc) seq
--			--,src.*
--		from t_etl_adj_data_flow_set_flow src
--		where adjustment_id = 1
--	) dup
--	where dup.seq <> 1
--		-- exists
--		and dup.adjustment_id = t_etl_adj_data_flow_set_flow.adjustment_id
--		and dup.line_nr = t_etl_adj_data_flow_set_flow.line_nr
--)
--;

---- check T0, Tn on each stage
---- 1. stage 3 must have T0 only
--select *
--from (	
--	select object_key_value, flow_type, start_validity_date, calculation_date
--		,count(flow_date) as cnt_flow_date, min(flow_date) as min_flow_date, max(flow_date) as max_flow_date
--	from t_etl_adj_data_flow_set_flow fsf
--	where adjustment_id = 1
--	group by object_key_value, flow_type, start_validity_date, calculation_date
--) as tn_chk
--where flow_type = 'EAD_3'
--	and (	calculation_date <> min_flow_date
--			or calculation_date <> max_flow_date
--			or cnt_flow_date <> 1	
--		)
--;
---- 2. stage 1,2 must start at T1 not T0
--select *
--from (	
--	select object_key_value, flow_type, start_validity_date, calculation_date
--		,count(flow_date) as cnt_flow_date, min(flow_date) as min_flow_date, max(flow_date) as max_flow_date
--	from t_etl_adj_data_flow_set_flow fsf
--	where adjustment_id = 1
--	group by object_key_value, flow_type, start_validity_date, calculation_date
--) as tn_chk
--where flow_type <> 'EAD_3'
--	and min_flow_date <= calculation_date 

----	2.1 remove TO of stage 1,2
--delete t_etl_adj_data_flow_set_flow
--where flow_type <> 'EAD_3'
--	and flow_date <= calculation_date 

---- 3. stage 1 must have value T1 to T12 (dup T12 from stage 2)
---- check only cnt_flow_date = 11, less than this is maturity before 12 month
--select * -- count(*)
--from (	
--	select object_key_value, flow_type, start_validity_date, calculation_date
--		,count(flow_date) as cnt_flow_date, min(flow_date) as min_flow_date, max(flow_date) as max_flow_date
--	from t_etl_adj_data_flow_set_flow fsf
--	where adjustment_id = 1
--	group by object_key_value, flow_type, start_validity_date, calculation_date
--) as tn_chk
--where flow_type = 'EAD_1'
--	and cnt_flow_date = 11
--;

----	3.1 select stage 2 prepare for insert to stage 1
----	*** note stage 1 has 33110 deal but stage 2 has 32938
--select *
--from t_etl_adj_data_flow_set_flow
--where adjustment_id = 1
--	and flow_type = 'EAD_2'
--	and flow_date = eomonth(dateadd(month,12,calculation_date))
--	and exists (
--		select object_key_value
--		from (	
--			select object_key_value, flow_type, start_validity_date, calculation_date
--				,count(flow_date) as cnt_flow_date, min(flow_date) as min_flow_date, max(flow_date) as max_flow_date
--			from t_etl_adj_data_flow_set_flow fsf
--			where adjustment_id = 1
--				and flow_type = 'EAD_1'
--			group by object_key_value, flow_type, start_validity_date, calculation_date
--		) as tn_chk
--		where cnt_flow_date = 11
--		-- exists
--			and tn_chk.object_key_value = t_etl_adj_data_flow_set_flow.object_key_value
--	)
--;

---- insert by create new line_nr, change flow_type, scenario_id
--INSERT INTO t_etl_adj_data_flow_set_flow
--       (adjustment_id, line_nr, flag, line_status, retry_count, line_date, entity, object_origin, object_key_type, object_key_value, flow_type, flow_set_id, scenario_id, amount_type, start_validity_date, end_validity_date, calculation_date, flow_date, currency, amount, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5, last_modified, modified_by)
--SELECT adjustment_id--, line_nr
--	, max_line_nr + row_number() over(order by line_nr) as line_nr
--	, flag, line_status, retry_count, line_date, entity, object_origin, object_key_type, object_key_value
--	, 'EAD_1' as flow_type, flow_set_id
--	, '1' as scenario_id
--	, amount_type, start_validity_date, end_validity_date, calculation_date, flow_date, currency, amount, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5, last_modified, modified_by
--from t_etl_adj_data_flow_set_flow 
--	join (
--		select max(line_nr) as max_line_nr
--		from t_etl_adj_data_flow_set_flow m
--		where adjustment_id in (1,11,14)
--	) mline on 1=1
--where adjustment_id = 1
--	and flow_type = 'EAD_2'
--	and flow_date = eomonth(dateadd(month,12,calculation_date))
--	and exists (
--		select object_key_value
--		from (	
--			select object_key_value, flow_type, start_validity_date, calculation_date
--				,count(flow_date) as cnt_flow_date, min(flow_date) as min_flow_date, max(flow_date) as max_flow_date
--			from t_etl_adj_data_flow_set_flow fsf
--			where adjustment_id = 1
--				and flow_type = 'EAD_1'
--			group by object_key_value, flow_type, start_validity_date, calculation_date
--		) as tn_chk
--		where cnt_flow_date = 11
--		-- exists
--			and tn_chk.object_key_value = t_etl_adj_data_flow_set_flow.object_key_value
--	)
--;


---- move to live table
---- run hms using auto cleanup
--declare @cob_date datetime	= '2018-03-31';
--declare @adjustment_id d_id = 1; -- HL
--exec p_etl_adj_data_flow_ccfp	@execution_date	= @cob_date, 
--								@cleanup_type = 2,
--								@cleanup_staging = 0,
--								@only_cleanup_changed = 1,
--								@use_delta_load = 1, 
--								@adjustment_id	= @adjustment_id,
--								@entity			= N'TBANK',  
--								@log_output     = 0
--;

---- run hms after clear flow, flow_set by manual script
--declare @cob_date datetime	= '2018-03-31';
--declare @adjustment_id d_id = 1; -- HL
--exec p_etl_adj_data_flow_ccfp	@execution_date	= @cob_date, 
--								@cleanup_type = 0,
--								@cleanup_staging = 0,
--								@only_cleanup_changed = 0,
--								@use_delta_load = 1, 
--								@adjustment_id	= @adjustment_id,
--								@entity			= N'TBANK',  
--								@log_output     = 0
--;


---- clear old flow
--select count(*)
--from t_flow f
--where exists (
--	select *
--	from t_flow_set fs
--	where fs.flow_set_id = f.flow_set_id
--		and exists (
--			select *
--			from t_etl_adj_data_flow_set_flow fsf
--			where fsf.adjustment_id = 1
--				and fsf.object_key_value = fs.object_key_value
--		)
--)
--;

--delete t_flow
--where exists (
--	select t_flow_set.flow_set_id
--	from t_flow_set
--	where t_flow_set.flow_set_id = t_flow.flow_set_id
--		and exists (
--			select *
--			from t_etl_adj_data_flow_set_flow fsf
--			where fsf.adjustment_id = 1
--				and fsf.object_key_value = t_flow_set.object_key_value
--		)
--)
--;
--delete t_flow_set
--where exists(
--	select *
--	from t_etl_adj_data_flow_set_flow fsf
--	where fsf.adjustment_id = 1
--		and fsf.object_key_value = t_flow_set.object_key_value
--)
--;

---- check the deal that not start at T1
--select *
--from (	
--	select object_key_value, flow_type, start_validity_date, calculation_date
--		,count(flow_date) as cnt_flow_date, min(flow_date) as min_flow_date, max(flow_date) as max_flow_date
--	from t_etl_adj_data_flow_set_flow fsf
--	where adjustment_id = 1
--	group by object_key_value, flow_type, start_validity_date, calculation_date
--) as tn_chk
--where flow_type = 'EAD_2'
--	and min_flow_date <> eomonth(dateadd(month,1,calculation_date))
--;

--select *
--from (	
--	select object_key_value, flow_type, start_validity_date, calculation_date
--		,count(flow_date) as cnt_flow_date, min(flow_date) as min_flow_date, max(flow_date) as max_flow_date
--	from t_flow_set fs join t_flow f
--	on fs.flow_set_id = f.flow_set_id
--	where fs.char_cust_element3 = 'HL'
--	group by object_key_value, flow_type, start_validity_date, calculation_date
--) as tn_chk
--where flow_type = 'EAD_2'
--	and min_flow_date <> eomonth(dateadd(month,1,calculation_date))
--;

--select *
--from t_flow_set fs join t_flow f
--	on fs.flow_set_id = f.flow_set_id
--where fs.flow_set_id = 8243452
--;

select char_cust_element3, count(distinct fs.object_key_value) cnt_deal, count(distinct fs.flow_set_id) cnt_flow_set, count(f.flow_date) cnt_flow_date
from t_flow_set fs join t_flow f
	on fs.flow_set_id = f.flow_set_id
group by fs.char_cust_element3
--;

--select top 10 *
--from t_store_zacc_tbank
--where deal_id = 'LN0411015000516000000000000000000000000000000000000000000000'

----8243452

--delete t_flow 
--where exists(
--	select flow_set_id
--	from t_flow_set
--	where t_flow_set.char_cust_element3 = ''
--		and t_flow_set.flow_set_id = t_flow.flow_set_id
--)
--;
--delete t_flow_set
--where t_flow_set.char_cust_element3 = ''
--;
