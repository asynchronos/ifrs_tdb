
--select *
--from t_etl_adj_data_flow_set_flow
--where adjustment_id in (3,4)

/*
-- delete flow of 
-- adjustment_id 3: cc & fp
-- adjustment_id 4: nonretail & smes

delete t_etl_adj_data_flow_set_flow
where adjustment_id in (3)
	and flow_type in ('EAD_2','EAD_3');
*/

select adjustment_id ,count(*)
from  t_etl_adj_data_flow_set_flow
--where adjustment_id in (3)
	--and flow_type in ('EAD_2','EAD_3')	
group by adjustment_id
;

select count(*)
from t_flow_set
;


EXEC p_stg_pop_ead_cc_fp		@param_reporting_date   = '2018-03-31',
								@param_is_test          = 0 
;


EXEC p_stg_pop_ead_hp			@param_reporting_date   = '2018-03-31',
								@param_is_test          = 0 
;


EXEC p_stg_pop_ead_nonretail_smes	@param_reporting_date   = '2018-03-31',
									@param_is_test          = 0 
;


select char_cust_element3, count(distinct fs.object_key_value) cnt_deal, count(distinct fs.flow_set_id) cnt_flow_set, count(f.flow_date) cnt_flow_date
from t_flow_set fs join t_flow f
	on fs.flow_set_id = f.flow_set_id
group by fs.char_cust_element3


select count(*)
from t_etl_adj_data_flow_set_flow
where adjustment_id = 3

--delete t_etl_adj_data_flow_set_flow
--where adjustment_id = 3

select count(*)
from t_flow f
where exists (
	select s.flow_set_id
	from t_flow_set s
	where s.char_cust_element3 not in ('CC','FP')
)


/*
select count(*)
from t_flow_set;
select count(*)
from t_flow;
*/

-- gen t_flow_set, t_flow 
/*

truncate table t_flow;
delete t_flow_set;

exec p_etl_adj_data_flow	@execution_date	= '2018-03-31',  
								@adjustment_id	= 3,
                                @entity			= N'TBANK',  
                                @log_output     = 0

-------- P'Aum tuning
exec p_etl_adj_data_flow_ccfp	@execution_date	= '2018-03-31',  
								@use_delta_load = 1, 
								@adjustment_id	= 3,
                                @entity			= N'TBANK',  
                                @log_output     = 0

								
exec p_etl_adj_data_flow_ccfp	@execution_date	= '2018-03-31', 
								@use_delta_load = 1,  
								@adjustment_id	= 4,
                                @entity			= N'TBANK',  
                                @log_output     = 0


exec p_etl_adj_data_flow_ccfp	@execution_date	= '2018-03-31', 
								@use_delta_load = 1, 
								@adjustment_id	= 1,
                                @entity			= N'TBANK',  
                                @log_output     = 0

PRINT 

-- check not found flow
--exec p_run_assump_ead_tbank @entity	= @entity

*/

/*

t_retail_hp_ead_stage1_tbank

-- pop to stg and run hms
EXEC p_stg_pop_ead_hp @param_reporting_date   = '2018-03-31',
                      @param_is_test          = 0 

*/

declare @adjustment_id int = 1;

declare @cnt_etl_grp int;
declare @cnt_liv_grp int;

declare @cnt_etl int;
declare @cnt_liv int;

declare @src_result nvarchar(255);
declare @hms_result nvarchar(255);

declare @compare_result nvarchar(255);
declare @compare_flag bit;

select @cnt_etl_grp = count(*)
from (
	select distinct etl.object_key_value, etl.flow_type
	from t_etl_adj_data_flow_set_flow etl
	where etl.adjustment_id = @adjustment_id
) m
;

select @cnt_liv_grp = count(flow_set_id)
from t_flow_set liv
where exists (
	select object_key_value, flow_type
	from t_etl_adj_data_flow_set_flow etl
	where etl.adjustment_id			= @adjustment_id
		and etl.object_key_value	= liv.object_key_value
		and etl.flow_type			= liv.flow_type
)
;

--select @src_result = FORMATMESSAGE('Not inserted %i flow set to t_flow_set with %i flow date.',@cnt_etl_grp ,count(line_nr))
select @cnt_etl = count(*)
from t_etl_adj_data_flow_set_flow etl
where etl.adjustment_id=@adjustment_id
;

--select @hms_result = FORMATMESSAGE('Inserted %i flow set to t_flow_set with %i flow date in t_flow.',@cnt_liv_grp ,count(d.flow_date))
select @cnt_liv = count(*)
from t_flow_set m join
	t_flow d on m.flow_set_id=d.flow_set_id
where exists (
	select etl.object_key_value, flow_type
	from t_etl_adj_data_flow_set_flow etl
	where etl.adjustment_id			= @adjustment_id
		and etl.object_key_value	= m.object_key_value
		and etl.flow_type			= m.flow_type
);

select	 @src_result = FORMATMESSAGE('ETL records(adj_id:%i), %s flow set with %s flow.',@adjustment_id, FORMAT(@cnt_etl_grp,'N0'), FORMAT(@cnt_etl,'N0'))
		,@hms_result = FORMATMESSAGE('OSX records(adj_id:%i), %s flow set with %s flow' ,@adjustment_id, FORMAT(@cnt_liv_grp,'N0'), FORMAT(@cnt_liv,'N0'))

PRINT @src_result;
PRINT @hms_result;

/*

-- move data from backup
delete t_etl_adj_data_flow_set_flow
where adjustment_id = 1
; 

INSERT INTO t_etl_adj_data_flow_set_flow
       (adjustment_id, line_nr, flag, line_status, retry_count, line_date, entity, object_origin, object_key_type, object_key_value, flow_type, flow_set_id, scenario_id, amount_type, start_validity_date, end_validity_date, calculation_date, flow_date, currency, amount, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5, last_modified, modified_by)
SELECT adjustment_id                
       ,line_nr                      
       ,flag                         
       ,line_status                  
       ,retry_count                  
       ,line_date                    
       ,entity                       
       ,object_origin                
       ,object_key_type              
       ,object_key_value             
       ,flow_type                    
       ,flow_set_id
	   ,case flow_type 
			when N'EAD_1' then 1
			when N'EAD_2' then 2
			when N'EAD_3' then 3
		else 0 end as scenario_id-- stage replace scenario_id                                    
       ,amount_type                  
       ,start_validity_date          
       ,end_validity_date            
       ,calculation_date             
       ,flow_date                    
       ,currency                     
       ,amount                       
       ,flow_set_char_cust_element1  
       ,flow_set_char_cust_element2  
       ,flow_set_char_cust_element3  
       ,flow_set_char_cust_element4  
       ,flow_set_char_cust_element5  
       ,flow_set_num_cust_element1   
       ,flow_set_num_cust_element2   
       ,flow_set_num_cust_element3   
       ,flow_set_num_cust_element4   
       ,flow_set_num_cust_element5   
       ,CURRENT_TIMESTAMP
       ,dbo.fn_user() 
from t_etl_adj_data_flow_set_flow_bk_20180911 src
where adjustment_id = 1 
	and (flow_date <> calculation_date or flow_type = N'EAD_3')
	and calculation_date = '2018-03-31'
;

*/

-- check stage 3 must have 1 plot
/*

select object_key_value, flow_type, scenario_id, min(flow_date) min_flow_date, max(flow_date) max_flow_date, count(flow_date) cnt_flow
	--top 10
	--case flow_type 
	--	when N'EAD_1' then 1
	--	when N'EAD_2' then 2
	--	when N'EAD_3' then 3
	--	else 0 end as stage -- replace scenario_id
	--, src.*
from t_etl_adj_data_flow_set_flow src
where adjustment_id = 1 
	--and (flow_date <> calculation_date or flow_type = N'EAD_3')
	and flow_type = N'EAD_3'
	and calculation_date = '2018-03-31'
group by object_key_value, flow_type, scenario_id
having count(flow_date) > 1

*/

/*

-- check dup flow_date if re-run more than 1 time
select count(*)
from (
	select --src.object_key_value, src.line_nr,
		row_number() over(partition by adjustment_id, object_key_value, flow_date order by line_date desc) seq
		,src.*
	from t_etl_adj_data_flow_set_flow src
	where adjustment_id = 1
		--and flow_type <> N'EAD_3'
		--and object_key_value = 'LN0048017000275000000000000000000000000000000000000000000000|TBANK'
		--and object_key_value = 'FP0013484000510016101840MRHFC6620JT100478|TBANK'
) check_dup
where seq <> 1

-- delete dup flow_date if re-run more than 1 time
delete t_etl_adj_data_flow_set_flow
--select count(*) from t_etl_adj_data_flow_set_flow
where exists(	
	select *
	from ( 
		select src.object_key_value, src.adjustment_id, src.line_nr
			,row_number() over(partition by adjustment_id, object_key_value, flow_date order by line_date desc) seq
			--,src.*
		from t_etl_adj_data_flow_set_flow src
		where adjustment_id = 1
	) dup
	where dup.seq <> 1
		-- exists
		and dup.adjustment_id = t_etl_adj_data_flow_set_flow.adjustment_id
		and dup.line_nr = t_etl_adj_data_flow_set_flow.line_nr
)

*/
