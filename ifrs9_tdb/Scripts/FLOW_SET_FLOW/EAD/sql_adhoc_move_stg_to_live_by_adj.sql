
-- check not exist after move
select flow_set_char_cust_element3,adjustment_id, count(*) cnt_rows
from t_etl_adj_data_flow_set_flow fsf
where adjustment_id not in (14,15 /* HL,'' */
							,1767,2115,2116,2117,2155,2206,2154,2157 /* HP-Juristic */
							)
and not exists(
	select distinct object_key_value
	from t_flow_set fs
	where fs.object_key_value=fsf.object_key_value
)
group by flow_set_char_cust_element3,adjustment_id
order by 1

select top 10 *
from t_etl_adj_data_flow_set_flow
where flow_set_char_cust_element3='Commercial'

/*

declare	@cob_date		datetime	= '2018-03-31'
declare	@adjustment_id	d_id		= 3	-- HL:1, CC/FP:3, HP-Stage1:2157, HP-Stage1:2206, HP-Stage1:2308
declare	@log_output		d_id		= 1
-- run hms
exec p_etl_adj_data_flow_tbank	@execution_date	= @cob_date, 
								@cleanup_type = 2,
								@cleanup_staging = 0,
								@only_cleanup_changed = 1,
								@use_delta_load = 1, 
								@adjustment_id	= 2157,
								@entity			= N'TBANK',  
								@log_output     = @log_output
-- run hms
exec p_etl_adj_data_flow_tbank	@execution_date	= @cob_date, 
								@cleanup_type = 2,
								@cleanup_staging = 0,
								@only_cleanup_changed = 1,
								@use_delta_load = 1, 
								@adjustment_id	= 2206,
								@entity			= N'TBANK',  
								@log_output     = @log_output
-- run hms
exec p_etl_adj_data_flow_tbank	@execution_date	= @cob_date, 
								@cleanup_type = 2,
								@cleanup_staging = 0,
								@only_cleanup_changed = 1,
								@use_delta_load = 1, 
								@adjustment_id	= 2308,
								@entity			= N'TBANK',  
								@log_output     = @log_output

-- check not exist after move
select flow_set_char_cust_element3,adjustment_id, count(*) cnt_rows
from t_etl_adj_data_flow_set_flow fsf
where adjustment_id not in (14,15 /* HL,'' */
							,1767,2115,2116,2117,2155,2206,2154,2157 /* HP-Juristic */
							)
and not exists(
	select distinct object_key_value
	from t_flow_set fs
	where fs.object_key_value=fsf.object_key_value
)
group by flow_set_char_cust_element3,adjustment_id
order by 1

*/
