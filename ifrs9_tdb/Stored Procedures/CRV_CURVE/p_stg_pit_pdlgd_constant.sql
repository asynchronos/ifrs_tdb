﻿
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_pdlgd_constant' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pit_pdlgd_constant] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pit_pdlgd_constant] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pit_pdlgd_constant]...'
GO
ALTER PROCEDURE [dbo].[p_stg_pit_pdlgd_constant] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime	= null,
	@param_is_test			bit			= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	Stored procedure for population t_etl_adj_data_flow_set_flow  *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

	-- variables for ale
	declare @ale_message						d_ale_message;
	declare @ale_context						xml;
	declare @ale_correlation_guid				UNIQUEIDENTIFIER;
	declare @ale_event_log_guid					UNIQUEIDENTIFIER;

-- Declare local variables
	declare @cob_date							datetime;
	declare @max_date							datetime				=	'9999-12-31';
	declare @entity								d_entity				=	N'TBANK';
	declare @lot_fn_code						d_std_record_function	=	N'ECLSEG';
	declare @max_lot_id							d_id;
	declare @scenario_context_id				d_id					=	301003;

	declare @adjustment_id						d_id;
	declare @adj_type_id						d_id;
	declare @src_table_name1					d_name					=	N't_retail_forward_pd_lgd_tbank'
	declare @src_table_name2					d_name					=	N''
	declare @src_table_name3					d_name					=	N''
	declare @table_type_name					d_name					=	N't_etl_adj_data_crv_curve';
	
	-- variables for t_ecl_process_log_tbank
	declare @log_pid							int;
	declare @log_process_type					varchar(20)				=	N'Retail';
	declare @log_model_name						varchar(100)			=	N'Pop Retail Constant PD LGD';
	declare @log_result_table					varchar(50)				=	@table_type_name;

	-- variables for cursor
	declare @cursor_table_name					nvarchar(256);
	declare @cursor_stage						d_name;
	declare @cursor_scenario_id					d_id;
	declare @cursor_scenario_id_str				d_name;
	declare @cursor_scenario_name				d_name;
	declare @cursor_src_field_name				d_name;
	declare @cursor_src_flow_type				d_name;
	declare @cursor_model_name					d_name;

	-- variable for dynamic sql
	declare @nsql								nvarchar(max);
	declare @nsql_select						nvarchar(50);
	declare @nsql_column						nvarchar(max);
	declare @nsql_table							nvarchar(max);
	declare @nsql_filter						nvarchar(max);
	declare @nsql_insert						nvarchar(max);
	declare @paramDef							nvarchar(max);

	-- variable for insert t_etl_adj_data_xxx
	declare @field_adjustment_id   				nvarchar(max);
	declare @field_line_date	   				nvarchar(max);
	--declare @field_line_nr         				nvarchar(max);
	declare @field_curve_code         			nvarchar(max);
	declare @field_curve_name         			nvarchar(max);
	declare @field_curve_desc         			nvarchar(max);
	declare @field_curve_type            		nvarchar(max);
	declare @field_source_system     			nvarchar(max);
	declare @field_curve_value_type     		nvarchar(max);
	declare @field_usage_type       			nvarchar(max);
	declare @field_curve_reference          	nvarchar(max);
	declare @field_fwd_nbr_of_time_units   		nvarchar(max);
	declare @field_fwd_time_unit 				nvarchar(max);
	declare @field_currency						nvarchar(max);
	declare @field_day_count_convention       	nvarchar(max);
	declare @field_busi_day_convention			nvarchar(max);
	declare @field_calendar						nvarchar(max);
	declare @field_retry_count					nvarchar(max);
	declare @field_start_validity_date 			nvarchar(max);
	declare @field_end_validity_date   			nvarchar(max);
	declare @field_value_perc1 					nvarchar(max);
	declare @field_value_amount1				nvarchar(max);
	declare @field_term_start_date1				nvarchar(max);
	declare @field_char_cust_element1			nvarchar(max);
	declare @field_char_cust_element2			nvarchar(max);
	declare @field_char_cust_element3			nvarchar(max);
	declare @field_char_cust_element4			nvarchar(max);
	declare @field_char_cust_element5			nvarchar(max);
	declare @field_num_cust_element1			nvarchar(max);
	declare @field_num_cust_element2			nvarchar(max);
	declare @field_num_cust_element3			nvarchar(max);
	declare @field_num_cust_element4			nvarchar(max);
	declare @field_num_cust_element5			nvarchar(max);
	declare @field_last_modified				nvarchar(max);
	declare @field_modified_by					nvarchar(max);
		
	-- assign cob date
	select @cob_date = isnull(@param_reporting_date,cob_date) from t_entity where entity = @entity;

	-- assign max lot id
	select @max_lot_id = max(lot.lot_id) from t_lot_definition lot where lot.std_record_function_code = @lot_fn_code;

	--add logging here
	set @ale_message = N'Start: ' + @proc_name
						+ N'(@param_reporting_date=' + CONVERT(varchar,@param_reporting_date)
						+ char(13)+char(10)+char(9)+N',@param_is_test=' + CONVERT(varchar,@param_is_test)
						+ char(13)+char(10)+char(9)+N',@cob_date=' + CONVERT(varchar,@cob_date)
						+ N')';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_START', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

	--/*Start Process Log*/ -- change to proc in the future
	--SET @log_pid = (select isnull(max(process_id),0)+1 
	--				from t_ecl_process_log_tbank
	--				where cob_date = @cob_date);

	--insert into t_ecl_process_log_tbank (cob_date,process_id,process_type,process_name,model_name,result_table,start_time,process_status)
	--select  @cob_date, @log_pid, @log_process_type, @proc_name, @log_model_name, @log_result_table, getdate(),'Processing';
	--/*End Process Log*/

-- Start actual procedure flow	
	BEGIN TRANSACTION	-- Begin tran here because of this process has less data
	BEGIN TRY
		-- assign field for select & insert
		-- src = source table, seg = t_ecl_segmentation
		-- text must have single-quote/char(39) between text
		-- function, field, numeric do not need single-quote
		select	 @field_adjustment_id   			= N'@adjustment_id'									-- if run multiple time, must update with p_adj_save before run HMS
				--,@field_line_nr         			= N'row_number() over(order by src.deal_id)'		-- must update before run if insert multiple time
				,@field_line_date         			= N'CURRENT_TIMESTAMP'		
				,@field_curve_code         			= N'CONCAT(src.curve_name'
														+',''|'',src.stage,''|'',se.scenario_id,''|'',src.model)'
				,@field_curve_name         			= N'CONCAT('+char(39)+case @param_is_test when 0 then '' else 'TEST-' end+char(39)
														+N',src.curve_name)'							-- if deal: check revolving from element3
				,@field_curve_desc         			= N'src.segment_id'
				,@field_curve_type           		= N'src.model+'+char(39)+N' Curve'+char(39)			-- t_crv_curve.curve_type_name {change between PD/LGD}
				,@field_source_system     			= N'se.TABLE_NAME'
				,@field_curve_value_type     		= N'src.model'										-- curve model, change between PD/LGD
				,@field_usage_type       			= char(39)+N'Actual'+char(39)						-- t_usage_type.usage_type_name
				,@field_curve_reference      		= char(39)+N'Internal curve'+char(39)				-- t_crv_curve_reference.curve_reference_name
				,@field_fwd_nbr_of_time_units		= N'1'
				,@field_fwd_time_unit 				= char(39)+N'M'+char(39)							-- t_time_unit.time_unit
				,@field_currency			 		= char(39)+N'THB'+char(39)							-- if deal: t_ecl_segmentation.currency
				,@field_day_count_convention		= char(39)+N'ACT/ACT'+char(39)						-- t_day_count_convention.day_count_convention_code
				,@field_busi_day_convention		    = char(39)+N'C'+char(39)							-- t_business_day_convention.business_day_convention_code
				,@field_calendar			        = char(39)+N'standard'+char(39)						-- t_calendar.calendar
				,@field_retry_count				    = N'0'
				,@field_start_validity_date 		= N'@cob_date'
				,@field_end_validity_date   		= N'@max_date'									
				,@field_value_perc1 				= N'case'+char(13)+char(10)+
														N'when src.value_type=''Marginal'' and se.scenario_id=3		then src.pit_pdlgd_flat'	+char(13)+char(10)+
														N'when src.value_type=''Marginal'' and se.scenario_id=4		then src.pit_pdlgd_worst'	+char(13)+char(10)+
														N'when src.value_type=''Marginal'' and se.scenario_id=5		then src.pit_pdlgd_best'	+char(13)+char(10)+
														N'when src.value_type=''Marginal'' and se.scenario_id=51	then src.pdlgd_nfl'			+char(13)+char(10)+
														N'when src.value_type=''Marginal'' and se.scenario_id=54	then src.pit_pdlgd_flat'	+char(13)+char(10)+
														N'when src.value_type=''Marginal'' and se.scenario_id=55	then src.pit_pdlgd_worst'	+char(13)+char(10)+
														N'when src.value_type=''Marginal'' and se.scenario_id=56	then src.pit_pdlgd_best'	+char(13)+char(10)+
														N'when src.value_type=''Marginal'' and se.scenario_id=57	then src.pdlgd_nfl'			+char(13)+char(10)+
														N'when src.value_type=''Marginal'' and se.scenario_id=110	then src.pdlgd_nfl'			+char(13)+char(10)+
														N'else 0 end'
				,@field_value_amount1				= N'case'+char(13)+char(10)+
														N'when src.value_type=''Cumulative'' and se.scenario_id=3	then src.pit_pdlgd_flat'	+char(13)+char(10)+
														N'when src.value_type=''Cumulative'' and se.scenario_id=4	then src.pit_pdlgd_worst'	+char(13)+char(10)+
														N'when src.value_type=''Cumulative'' and se.scenario_id=5	then src.pit_pdlgd_best'	+char(13)+char(10)+
														N'when src.value_type=''Cumulative'' and se.scenario_id=51	then src.pdlgd_nfl'			+char(13)+char(10)+
														N'when src.value_type=''Cumulative'' and se.scenario_id=54	then src.pit_pdlgd_flat'	+char(13)+char(10)+
														N'when src.value_type=''Cumulative'' and se.scenario_id=55	then src.pit_pdlgd_worst'	+char(13)+char(10)+
														N'when src.value_type=''Cumulative'' and se.scenario_id=56	then src.pit_pdlgd_best'	+char(13)+char(10)+
														N'when src.value_type=''Cumulative'' and se.scenario_id=57	then src.pdlgd_nfl'			+char(13)+char(10)+
														N'when src.value_type=''Cumulative'' and se.scenario_id=110	then src.pdlgd_nfl'			+char(13)+char(10)+
														N'else 0 end'
				,@field_term_start_date1			= N'eomonth(dateadd(month, case when src.stage=3 then 0 else src.forecast_month end,@cob_date))'
				,@field_char_cust_element1			= char(39)+''+char(39)								-- if deal: date_matur
				,@field_char_cust_element2			= N'CONVERT(nvarchar,se.scenario_id)'				-- scenario_id
				,@field_char_cust_element3			= N'src.segment_name'								-- segment_name
				,@field_char_cust_element4			= N'@cob_date'										-- reporting_date
				,@field_char_cust_element5			= char(39)+N''+char(39)								-- free slot, not use
				,@field_num_cust_element1			= N'src.segment_id'									-- segment_id
				,@field_num_cust_element2			= N'0.0'											-- if deal: remaining_maturity
				,@field_num_cust_element3			= N'0.0'											-- if deal: remaining_term
				,@field_num_cust_element4			= N'src.stage'											-- if deal: scenario stage
				,@field_num_cust_element5			= N'0.0'											-- free slot, not use
				,@field_last_modified				= N'CURRENT_TIMESTAMP'
				,@field_modified_by					= N'@proc_name'
		;
						
		-- assign nsql
		select @nsql_insert = 'INSERT INTO t_etl_adj_data_crv_curve
			(adjustment_id, line_date, curve_code, curve_name, curve_desc, curve_type, source_system, curve_value_type, usage_type, curve_reference, forward_nbr_of_time_units, forward_time_unit, currency, day_count_convention, business_day_convention, calendar, retry_count, start_validity_date, end_validity_date, value_perc1, value_amount1, term_start_date1, char_cust_element1, char_cust_element2, char_cust_element3, char_cust_element4, char_cust_element5, num_cust_element1, num_cust_element2, num_cust_element3, num_cust_element4, num_cust_element5, last_modified, modified_by)'
				
		select @nsql_table = N''					+char(13)+char(10)+
				N'from '+@src_table_name1+' src'	+char(13)+char(10)+
					N'join #t_config se on src.stage=se.stage'
		;

		select @nsql_filter = N''															+char(13)+char(10)+
							N'where src.as_of_date=@cob_date'
		;

		select @nsql_select = N''+char(13)+char(10)+ case @param_is_test when 0 then ' select ' else ' select top 300 ' end;
		
		select @nsql_column = N''															+char(13)+char(10)+
				@field_adjustment_id   					+'	as adjustment_id,'				+char(13)+char(10)+
				@field_line_date       					+'	as line_date,'					+char(13)+char(10)+
				@field_curve_code      					+'	as curve_code,'					+char(13)+char(10)+
				@field_curve_name     					+'	as curve_name,'					+char(13)+char(10)+
				@field_curve_desc     					+'	as curve_desc,'					+char(13)+char(10)+
				@field_curve_type      					+'	as curve_type,'					+char(13)+char(10)+
				@field_source_system   					+'	as source_system,'				+char(13)+char(10)+
				@field_curve_value_type					+'	as curve_value_type,'			+char(13)+char(10)+
				@field_usage_type	 					+'	as usage_type,'					+char(13)+char(10)+
				@field_curve_reference					+'	as curve_reference,'			+char(13)+char(10)+
				@field_fwd_nbr_of_time_units 			+'	as forward_nbr_of_time_units,'	+char(13)+char(10)+
				@field_fwd_time_unit					+'	as forward_time_unit,'			+char(13)+char(10)+
				@field_currency							+'	as currency,'					+char(13)+char(10)+
				@field_day_count_convention				+'	as day_count_convention,'		+char(13)+char(10)+
				@field_busi_day_convention				+'	as business_day_convention,'	+char(13)+char(10)+
				@field_calendar							+'	as calendar,'					+char(13)+char(10)+
				@field_retry_count						+'	as retry_count,'				+char(13)+char(10)+
				@field_start_validity_date				+'	as start_validity_date,'		+char(13)+char(10)+
				@field_end_validity_date				+'	as end_validity_date,'			+char(13)+char(10)+
				@field_value_perc1						+'	as value_perc1,'				+char(13)+char(10)+
				@field_value_amount1					+'	as value_amount1,'				+char(13)+char(10)+
				@field_term_start_date1					+'	as term_start_date1,'			+char(13)+char(10)+
				@field_char_cust_element1				+'	as char_cust_element1,'			+char(13)+char(10)+
				@field_char_cust_element2				+'	as char_cust_element2,'			+char(13)+char(10)+
				@field_char_cust_element3				+'	as char_cust_element3,'			+char(13)+char(10)+
				@field_char_cust_element4				+'	as char_cust_element4,'			+char(13)+char(10)+
				@field_char_cust_element5				+'	as char_cust_element5,'			+char(13)+char(10)+
				@field_num_cust_element1				+'	as num_cust_element1,'			+char(13)+char(10)+
				@field_num_cust_element2				+'	as num_cust_element2,'			+char(13)+char(10)+
				@field_num_cust_element3				+'	as num_cust_element3,'			+char(13)+char(10)+
				@field_num_cust_element4				+'	as num_cust_element4,'			+char(13)+char(10)+
				@field_num_cust_element5				+'	as num_cust_element5,'			+char(13)+char(10)+
				@field_last_modified					+'	as last_modified,'				+char(13)+char(10)+
				@field_modified_by						+'	as modified_by';

		-- param definition
		set @paramDef = N'@proc_name			sysname
						,@cob_date				datetime
						,@max_date				datetime
						,@adjustment_id			d_id
						,@scenario_context_id	d_id
						,@src_table_name1		d_name
						,@src_table_name2		d_name
						,@src_table_name3		d_name'

		-- final t-sql
		set @nsql = @nsql_insert + @nsql_select + @nsql_column + @nsql_table + @nsql_filter;

		-- create config temp table
		select t.TABLE_NAME, stage, sec.scenario_id, sec.scenario_name, sec.value_type, sec.src_field_name
		into #t_config
		from INFORMATION_SCHEMA.TABLES t
			join ( -- create configuration table with inline sql, may move to physical table in the future
				select s.scenario_id, s.scenario_name
					,case
						when CHARINDEX('stage 1',lower(s.scenario_name)) > 0	then	'1'
						when CHARINDEX('stage 2',lower(s.scenario_name)) > 0	then	'2'
						when CHARINDEX('stage 3',lower(s.scenario_name)) > 0	then	'3'
						else 'unknown' end as stage
					,N'LGD' as value_type
					,case s.scenario_id
						when 3		then	'pit_pdlgd_flat'
						when 4		then	'pit_pdlgd_worst'
						when 5		then	'pit_pdlgd_best'
						when 51		then	'pdlgd_nfl'
						when 54		then	'pit_pdlgd_flat'
						when 55		then	'pit_pdlgd_worst'
						when 56		then	'pit_pdlgd_best'
						when 57		then	'pdlgd_nfl'
						when 110	then	'pdlgd_nfl'
						else 'unknown' end as src_field_name
				from t_scenario s
				where s.scenario_context_id = @scenario_context_id
			) sec on 1=1 
		where t.TABLE_NAME in (@src_table_name1,@src_table_name2,@src_table_name3)

		-- check final t-sql length
		IF len(@nsql) > 4000 or @nsql is null
		BEGIN
			set @ale_message = N'SQL string is over than 4000 character or null.';
			set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, len(@nsql) as len_nsql
				, @nsql_insert as nsql_insert, @nsql_select as nsql_select
				, @nsql_column as nsql_column, @nsql_table as nsql_table
				, @nsql_filter as nsql_filter FOR XML PATH);

			EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
						@event_type                = N'E',
						@context                   = @ale_context,
						@message                   = @ale_message,
						@event_type_detail         = N'E',
						--@correlation_guid          = @ale_correlation_guid OUTPUT,
						--@timestamp_utc             = N'',
						--@event_log_guid            = @ale_event_log_guid OUTPUT,
						@use_synchronous_logging   = 0,
						@print_ind                 = @param_is_test,
						@print_level               = 0,
						@calling_proc_name         = @proc_name;

			PRINT '@nsql_insert:'	+ISNULL(@nsql_insert,'NULL')
			PRINT '@nsql_select:'	+ISNULL(@nsql_select,'NULL')
			PRINT '@nsql_column:'	+ISNULL(@nsql_column,'NULL')
			PRINT '@nsql_table:'	+ISNULL(@nsql_table,'NULL')
			PRINT '@nsql_filter:'	+ISNULL(@nsql_filter,'NULL')

			set @error_msg = @ale_message
			RAISERROR (@error_msg, 16, 1, @proc_name)
		END

		-- clear adjustment_id 0 before run insert
		--delete t_etl_adj_data_crv_curve where adjustment_id=0;

		set @adjustment_id = null;
		-- get table type id
		SELECT @adj_type_id   = (SELECT typ.type_id FROM t_adj_type typ JOIN t_adj_category cat ON cat.category_id = typ.category_id WHERE typ.type_name = @table_type_name);
		-- prepare adjustment before load to stg
		EXEC @status = [dbo].[p_adj_save]
								@adjustment_id = @adjustment_id OUTPUT,
								@type_id = @adj_type_id,
								@reporting_date = @cob_date,
								@comment = @src_table_name1
		;

		--add logging here
		set @ale_message = N'Inserting '+ @src_table_name1+','+@src_table_name2+','+@src_table_name3 + ' to ' + @table_type_name+' (adjustment_id:'+CONVERT(NVARCHAR,@adjustment_id)+')';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @cob_date as cob_date, @nsql as nsql FOR XML PATH);

		EXEC p_ale_log @event_name            = N'CREATE_ADJUSTMENT', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT 
					@event_type                = N'I',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'I',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = @param_is_test,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;

		RAISERROR(@ale_message,10,1,@proc_name);

		-- insert data with sp_executesql
		exec sp_executesql @nsql ,@paramDef ,@proc_name				=@proc_name			
											,@cob_date				=@cob_date				
											,@max_date				=@max_date	
											,@adjustment_id			=@adjustment_id			
											,@scenario_context_id	=@scenario_context_id	
											,@src_table_name1		=@src_table_name1		
											,@src_table_name2		=@src_table_name2		
											,@src_table_name3		=@src_table_name3		
		;

		--add logging here
		set @ale_message = N'Running HMS.';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @adjustment_id as adjustment_id FOR XML PATH);

		EXEC p_ale_log @event_name            = N'START_ADJUSTMENT', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
					@event_type                = N'I',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'I',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = @param_is_test,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;

		RAISERROR(@ale_message,10,1,@proc_name);

		-- run hms
		EXEC @status = [dbo].[p_etl_adj_xtr_crv_curve]
                            @portfolio_date = @cob_date,
                            @del_data_after_loading = 0,
                            @div_rate_by_100 = 0,
                            @adjustment_id = @adjustment_id,
                            @debug = @param_is_test
		;

		declare @hms_result nvarchar(max);
		select @hms_result = FORMATMESSAGE('Inserted %i curve to t_crv_curve and %i curve point to t_crv_curve_point_value' ,count(distinct crv.curve_code), count(p.curve_point_value_id))
		from t_crv_curve crv join
			t_crv_curve_point_value p on crv.curve_id=p.curve_id
		where exists (
			select etl.curve_code
			from t_etl_adj_data_crv_curve etl
			where etl.adjustment_id=@adjustment_id
				and etl.curve_code=crv.curve_code
		);
		RAISERROR(@hms_result,10,1,@proc_name);
		
		COMMIT;
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK;
		
		--/******************************************************************/
		--/*End Process Log*/	
		--/******************************************************************/
		--update t_ecl_process_log_tbank 
		--set end_time		=	getdate(),
		--	execution_time	=	cast((getdate() - start_time) as time(0)),
		--	process_status	=	'Failed'
		--where process_id = @log_pid
		--	and cob_date = @cob_date;

		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'
							+ char(13)+char(10)+char(9)+'##################################################';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = 1,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
	END CATCH

	--/******************************************************************/
	--/*End Process Log*/	
	--/******************************************************************/
	--update t_ecl_process_log_tbank 
	--set end_time		=	getdate(),
	--	execution_time	=	cast((getdate() - start_time) as time(0)),
	--	process_status	=	'Success'
	--where process_id = @log_pid
	--	and cob_date = @cob_date;
	--Print '#################################################################################';

	--add logging here
	set @ale_message = N'End: ' + @proc_name;
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_FINISH', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				--@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				--@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;
-- Return succes
RETURN (0) 


GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_pdlgd_constant' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_pdlgd_constant] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_pdlgd_constant] has NOT been altered due to errors!'
END
GO
