﻿
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_lgd_main' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pit_lgd_main] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pit_lgd_main] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pit_lgd_main]...'
GO
ALTER PROCEDURE [dbo].[p_stg_pit_lgd_main] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime,
	@param_src_table_name	nvarchar(256),
	@param_scenario_id		d_name,
	@param_scenario_name	d_name,
	@param_stage			d_name,
	@param_src_field_name	d_name,
	@param_is_test			bit				= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	This stored procedure will populate data for				  *
-- *				t_etl_adj_data_crv_curve and run HMS process to live table	  *
-- *				t_crv_curve and t_crv_curve_point_value.					  *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_src_table_name: point in time source table name      *
-- *				- @param_scenario_id: scenario_id/list of scenario_id		  *
-- *				- @param_scenario_name: scenario_name						  *
-- *				- @param_stage: stage name for mapping table/column			  *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

	-- variables for ale
	declare @ale_message						d_ale_message;
	declare @ale_context						xml;
	declare @ale_correlation_guid				UNIQUEIDENTIFIER;
	declare @ale_event_log_guid					UNIQUEIDENTIFIER;

-- Declare local variables
	declare @cob_date							datetime;
	declare @max_date							datetime				=	'9999-12-31';
	declare @entity								d_entity				=	N'TBANK';
	declare @lot_fn_code						d_std_record_function	=	N'ECLSEG';
	declare @max_lot_id							d_id;
	declare @scenario_context_id				d_id					=	301003;

	declare @adjustment_id						d_id;
	declare @adj_type_id						d_id;
	declare @src_table_name1					d_name					=	N't_retail_forward_pd_lgd_tbank'
	declare @src_table_name2					d_name					=	N''
	declare @src_table_name3					d_name					=	N''
	declare @table_type_name					d_name					=	N't_etl_adj_data_crv_curve';
	declare @pool_id_list						nvarchar(100)			=	'(1,2,3,5,71,72,73)';
	
	-- variables for t_ecl_process_log_tbank
	declare @log_pid							int;
	declare @log_process_type					varchar(20)				=	N'Retail';
	declare @log_model_name						varchar(100)			=	N'Pop SPL,MRTA,UPL,T-Loan,Staff LGD';
	declare @log_result_table					varchar(50)				=	@table_type_name;

	-- control variable (default)
	declare @data_level							d_name					=	N'deal'	-- deal,segment,subsegment(segment+rating)
	declare @is_revolving						d_name					=	N''		-- (Y,N,Blank) for change between Marginal/Cumulative/Blank
	declare @is_nonretail						d_name					=	N'N'	-- for change between date_tx/as_of_date

	-- variables for cursor
	declare @cursor_table_name					nvarchar(256);
	declare @cursor_stage						d_name;
	declare @cursor_scenario_id					d_id;
	declare @cursor_scenario_id_str				d_name;
	declare @cursor_scenario_name				d_name;
	declare @cursor_src_field_name				d_name;
	declare @cursor_src_flow_type				d_name;
	declare @cursor_model_name					d_name;

	-- variable for dynamic sql
	declare @nsql								nvarchar(max);
	declare @nsql_select						nvarchar(50);
	declare @nsql_column						nvarchar(max);
	declare @nsql_table							nvarchar(max);
	declare @nsql_filter						nvarchar(max);
	declare @nsql_insert						nvarchar(max);
	declare @paramDef							nvarchar(max);

	-- variable for insert t_etl_adj_data_xxx
	declare @field_adjustment_id   				nvarchar(255);
	declare @field_line_date	   				nvarchar(255);
	--declare @field_line_nr         				nvarchar(255);
	declare @field_curve_code         			nvarchar(255);
	declare @field_curve_name         			nvarchar(255);
	declare @field_curve_desc         			nvarchar(255);
	declare @field_curve_type            		nvarchar(255);
	declare @field_source_system     			nvarchar(255);
	declare @field_curve_value_type     		nvarchar(255);
	declare @field_usage_type       			nvarchar(255);
	declare @field_curve_reference          	nvarchar(255);
	declare @field_fwd_nbr_of_time_units   		nvarchar(255);
	declare @field_fwd_time_unit 				nvarchar(255);
	declare @field_currency						nvarchar(255);
	declare @field_day_count_convention       	nvarchar(255);
	declare @field_busi_day_convention			nvarchar(255);
	declare @field_calendar						nvarchar(255);
	declare @field_retry_count					nvarchar(255);
	declare @field_start_validity_date 			nvarchar(255);
	declare @field_end_validity_date   			nvarchar(255);
	declare @field_value_perc1 					nvarchar(255);
	declare @field_value_amount1				nvarchar(255);
	declare @field_term_start_date1				nvarchar(255);
	declare @field_char_cust_element1			nvarchar(255);
	declare @field_char_cust_element2			nvarchar(255);
	declare @field_char_cust_element3			nvarchar(255);
	declare @field_char_cust_element4			nvarchar(255);
	declare @field_char_cust_element5			nvarchar(255);
	declare @field_num_cust_element1			nvarchar(255);
	declare @field_num_cust_element2			nvarchar(255);
	declare @field_num_cust_element3			nvarchar(255);
	declare @field_num_cust_element4			nvarchar(255);
	declare @field_num_cust_element5			nvarchar(255);
	declare @field_last_modified				nvarchar(255);
	declare @field_modified_by					nvarchar(255);
		
	-- assign cob date
	select @cob_date = isnull(@param_reporting_date,cob_date) from t_entity where entity = @entity;

	-- assign max lot id
	select @max_lot_id = max(lot.lot_id) from t_lot_definition lot where lot.std_record_function_code = @lot_fn_code;

	--add logging here
	set @ale_message = N'Start: ' + @proc_name
						+ N'(@param_reporting_date=' + CONVERT(varchar,@param_reporting_date)
						+ char(13)+char(10)+char(9)+N',@param_is_test=' + CONVERT(varchar,@param_is_test)
						+ char(13)+char(10)+char(9)+N',@cob_date=' + CONVERT(varchar,@cob_date)
						+ N')';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_START', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = @param_is_test,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

-- Start actual procedure flow
	--IF @param_reporting_date is null 
	--	or @param_src_table_name is null
	--	or @param_scenario_id is null
	--	or @param_scenario_name is null
	--	or @param_stage is null
	--	or @param_src_field_name is null
	--BEGIN
	--	select @error_msg = isnull(@param_reporting_date,',@param_reporting_date')
	--						+ isnull(@param_src_table_name,',@param_src_table_name')
	--						+ isnull(@param_scenario_id,',@param_scenario_id')
	--						+ isnull(@param_scenario_name,',@param_scenario_name')
	--						+ isnull(@param_stage,',@param_stage')
	--						+ isnull(@param_src_field_name,',@param_src_field_name');
	--	set @error_msg = N'This procedure must send parameter: ' + substring(@error_msg,2,len(@error_msg))
	--	RAISERROR (@error_msg, 16, 1, @proc_name)
	--	RETURN 1
	--END
	BEGIN TRY
		-- clear adjustment_id 0 before run insert
		delete t_etl_adj_data_flow_set_flow where adjustment_id=0;

		---- setting control variable
		--select	@data_level							= case when then else end
		--		,@is_revolving						= case when then else end
		--		,@is_nonretail						= case when then else end
		--;

		-- assign field for select & insert
		-- src = source table, seg = t_ecl_segmentation
		-- text must have single-quote/char(39) between text
		-- function, field, numeric do not need single-quote
		select	 @field_adjustment_id   			= N'0'												-- must update with p_adj_save before run HMS
				--,@field_line_nr         			= N'row_number() over(order by src.deal_id)'		-- must update before run if insert multiple time
				,@field_line_date         			= N'CURRENT_TIMESTAMP'		
				,@field_curve_code         			= N'CONCAT('
													 +case when @data_level in ('deal') then N'seg.contract_id'
														   when @data_level in ('subsegment') then N'src.curve_name'
													  else N'src.segment_id' end
													 +case when @is_revolving in ('Y') then N','+char(39)+'-R'+char(39)
														   when @is_revolving in ('N') then N','+char(39)+'-M'+char(39)
													  else N'' end
													 +',''|'',@cursor_stage,''|'',@cursor_scenario_id,''|'',src.model)'
				,@field_curve_name         			= N'CONCAT('+char(39)+case @param_is_test when 0 then '' else 'TEST-' end+char(39)+N','
													 +case when @data_level in ('deal') then N'src.deal_id'
														   when @data_level in ('subsegment') then N'src.curve_name'
													  else N'src.segment_id' end
													 +case when @is_revolving in ('Y') then N','+char(39)+'-R'+char(39)
														   when @is_revolving in ('N') then N','+char(39)+'-M'+char(39)
													  else N'' end -- if deal: check revolving from element3
													 +N')'
				,@field_curve_desc         			= N'CONCAT('
													 +case when @data_level in ('deal') then N'src.acc_id'
														   when @data_level in ('subsegment') then N'src.curve_name'
													  else N'src.segment_id' end
													 +N')'
				,@field_curve_type           		= N'src.model+'+char(39)+N' CURVE'+char(39)			-- t_crv_curve.curve_type_name {change between PD/LGD}
				,@field_source_system     			= N'@cursor_table_name'
				,@field_curve_value_type     		= N'src.model'										-- curve model, change between PD/LGD
				,@field_usage_type       			= char(39)+N'Actual'+char(39)						-- t_usage_type.usage_type_name
				,@field_curve_reference      		= char(39)+N'INT'+char(39)							-- t_crv_curve_reference.curve_reference_name
				,@field_fwd_nbr_of_time_units		= N'1'
				,@field_fwd_time_unit 				= char(39)+N'M'+char(39)							-- t_time_unit.time_unit
				,@field_currency			 		= char(39)
														+case when @data_level in ('deal') then N'seg.currency'
														else N'THB' end
													  +char(39)											-- if deal: t_ecl_segmentation.currency
				,@field_day_count_convention		= char(39)+N'ACT/ACT'+char(39)						-- t_day_count_convention.day_count_convention_code
				,@field_busi_day_convention		    = char(39)+N'C'+char(39)							-- t_business_day_convention.business_day_convention_code
				,@field_calendar			        = char(39)+N'standard'+char(39)						-- t_calendar.calendar
				,@field_retry_count				    = N'0'
				,@field_start_validity_date 		= N'@cob_date'
				,@field_end_validity_date   		= N'@max_date'									
				,@field_value_perc1 				= N'case '+case when @data_level in ('deal') then N'seg.element3'
																else N'src.value_type' end+char(13)+char(10)			-- check marginal/cummulative
														+'when ''Marginal''	then '+@cursor_src_field_name+char(13)+char(10)
														+'else 0 end'
				,@field_value_amount1				= N'case '+case when @data_level in ('deal') then N'seg.element3'
																else N'src.value_type' end+char(13)+char(10)			-- check marginal/cummulative
														+'when ''Cumulative'' then '+@cursor_src_field_name+char(13)+char(10)
														+'else 0 end'
				,@field_term_start_date1			= N'eomonth(dateadd(month, case when @cursor_stage=3 then 0 else src.forecast_month end,@cob_date))'
				,@field_char_cust_element1			= char(39)
														+case when @data_level in ('deal') then N'src.date_matur'
														else '' end
													  +char(39)											-- if deal: date_matur
				,@field_char_cust_element2			= N'CONVERT(nvarchar,@cursor_scenario_id)'			-- scenario_id
				,@field_char_cust_element3			= N'src.segment_name'								-- segment_name
				,@field_char_cust_element4			= N'@cob_date'										-- reporting_date
				,@field_char_cust_element5			= char(39)+N''+char(39)								-- free slot, not use
				,@field_num_cust_element1			= N'src.segment_id'									-- segment_id
				,@field_num_cust_element2			= case when @data_level in ('deal') then N'src.remaining_maturity'
														else '0.0' end									-- if deal: remaining_maturity
				,@field_num_cust_element3			= case when @data_level in ('deal') then N'src.remaining_maturity'
														else '0.0' end									-- if deal: remaining_term
				,@field_num_cust_element4			= case when @data_level in ('deal') then N'src.stage'
														else '0.0' end									-- if deal: scenario stage
				,@field_num_cust_element5			= N'0.0'											-- free slot, not use
				,@field_last_modified				= N'CURRENT_TIMESTAMP'
				,@field_modified_by					= N'@proc_name'
		;
						
		-- assign nsql
		select @nsql_insert = 'INSERT INTO t_etl_adj_data_crv_curve
			(adjustment_id, line_date, curve_code, curve_name, curve_desc, curve_type, source_system, curve_value_type, usage_type, curve_reference, forward_nbr_of_time_units, forward_time_unit, currency, day_count_convention, business_day_convention, calendar, retry_count, start_validity_date, end_validity_date, value_perc1, value_amount1, term_start_date1, char_cust_element1, char_cust_element2, char_cust_element3, char_cust_element4, char_cust_element5, num_cust_element1, num_cust_element2, num_cust_element3, num_cust_element4, num_cust_element5, last_modified, modified_by)'
		
		select @nsql_select = N''+char(13)+char(10)+ case @param_is_test when 0 then ' select ' else ' select top 300 ' end;
		
		select @nsql_column = N''																	+char(13)+char(10)+
				@field_adjustment_id   					+'	as adjustment_id,'						+char(13)+char(10)+
				@field_line_date       					+'	as line_date,'							+char(13)+char(10)+
				@field_curve_code      					+'	as curve_code,'							+char(13)+char(10)+
				@field_curve_name     					+'	as curve_name,'							+char(13)+char(10)+
				@field_curve_desc     					+'	as curve_desc,'							+char(13)+char(10)+
				@field_curve_type      					+'	as curve_type,'							+char(13)+char(10)+
				@field_source_system   					+'	as source_system,'						+char(13)+char(10)+
				@field_curve_value_type					+'	as curve_value_type,'					+char(13)+char(10)+
				@field_usage_type	 					+'	as usage_type,'							+char(13)+char(10)+
				@field_curve_reference					+'	as curve_reference,'					+char(13)+char(10)+
				@field_fwd_nbr_of_time_units 		+'	as forward_nbr_of_time_units,'			+char(13)+char(10)+
				@field_fwd_time_unit				+'	as forward_time_unit,'					+char(13)+char(10)+
				@field_currency							+'	as currency,'							+char(13)+char(10)+
				@field_day_count_convention				+'	as day_count_convention,'				+char(13)+char(10)+
				@field_busi_day_convention			+'	as business_day_convention,'			+char(13)+char(10)+
				@field_calendar							+'	as calendar,'							+char(13)+char(10)+
				@field_retry_count						+'	as retry_count,'						+char(13)+char(10)+
				@field_start_validity_date				+'	as start_validity_date,'				+char(13)+char(10)+
				@field_end_validity_date				+'	as end_validity_date,'					+char(13)+char(10)+
				@field_value_perc1						+'	as value_perc1,'						+char(13)+char(10)+
				@field_value_amount1					+'	as value_amount1,'						+char(13)+char(10)+
				@field_term_start_date1					+'	as term_start_date1,'					+char(13)+char(10)+
				@field_char_cust_element1				+'	as char_cust_element1,'					+char(13)+char(10)+
				@field_char_cust_element2				+'	as char_cust_element2,'					+char(13)+char(10)+
				@field_char_cust_element3				+'	as char_cust_element3,'					+char(13)+char(10)+
				@field_char_cust_element4				+'	as char_cust_element4,'					+char(13)+char(10)+
				@field_char_cust_element5				+'	as char_cust_element5,'					+char(13)+char(10)+
				@field_num_cust_element1				+'	as num_cust_element1,'					+char(13)+char(10)+
				@field_num_cust_element2				+'	as num_cust_element2,'					+char(13)+char(10)+
				@field_num_cust_element3				+'	as num_cust_element3,'					+char(13)+char(10)+
				@field_num_cust_element4				+'	as num_cust_element4,'					+char(13)+char(10)+
				@field_num_cust_element5				+'	as num_cust_element5,'					+char(13)+char(10)+
				@field_last_modified					+'	as last_modified,'						+char(13)+char(10)+
				@field_modified_by						+'	as modified_by';
				
		select @nsql_table = N''																	+char(13)+char(10)+
				N'from '+@cursor_table_name+' src'													+char(13)+char(10)+
				case when @data_level in ('deal') 
						then N'join t_ecl_segmentation seg on src.deal_id=seg.deal_tfi_id and seg.lot_id = @max_lot_id'
				else '' end
		;

		select @nsql_filter = N''																	+char(13)+char(10)+
				case when @is_nonretail='Y' 
						then N' where src.date_tx=@reporting_date and src.path=''Liquidation'''		+char(13)+char(10)+
							+N' and deal_id not in (''FP001356500179'',''FP001359501124'',''FP001359501221'',''FP001359502065'',''FP001359501166'',''FP001359501182'',''FP001359501205'')'-- temporay filter until FP not duplicate
					 when @is_nonretail='N' and @cursor_stage=3
						then N' where src.as_of_date=@reporting_date and src.forecast_month=1'
				else N' where src.as_of_date=@reporting_date' end									+char(13)+char(10)+
				case when @data_level in ('deal') 
						then N' and src.deal_id is not null'
				else '' end
		;

		-- param definition
		set @paramDef = N'@proc_name			sysname
						,@cursor_scenario_id	d_id
						,@cursor_stage			d_name
						,@cursor_src_flow_type	d_name
						,@entity				d_entity
						,@cob_date				datetime
						,@max_date				datetime
						,@max_lot_id			d_id'

		-- final t-sql
		set @nsql = @nsql_insert + @nsql_select + @nsql_column + @nsql_table + @nsql_filter;
		-- check final t-sql length
		IF len(@nsql) > 4000
		BEGIN
			set @ale_message = N'SQL string is over than 4000 character.';
			set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql FOR XML PATH);

			EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
						@event_type                = N'E',
						@context                   = @ale_context,
						@message                   = @ale_message,
						@event_type_detail         = N'E',
						--@correlation_guid          = @ale_correlation_guid OUTPUT,
						--@timestamp_utc             = N'',
						--@event_log_guid            = @ale_event_log_guid OUTPUT,
						@use_synchronous_logging   = 0,
						@print_ind                 = @param_is_test,
						@print_level               = 0,
						@calling_proc_name         = @proc_name;

			set @error_msg = @ale_message
			RAISERROR (@error_msg, 16, 1, @proc_name)
			RETURN 1
		END

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Curve|SourceTable|Scenario: ' + @comment
		END

		--RAISERROR(@nsql, 16, 1, @proc_name) -- force stop program for view and testing
	END TRY
	BEGIN CATCH
		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'
							+ char(13)+char(10)+char(9)+'##################################################';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = @param_is_test,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
	END CATCH

	BEGIN TRANSACTION;
	BEGIN TRY -- start execute main process		

		-- get table type id
		SELECT @adj_type_id_crv_curve   = (SELECT typ.type_id FROM t_adj_type typ JOIN t_adj_category cat ON cat.category_id = typ.category_id WHERE typ.type_name = @table_type_name);
		-- prepare adjustment before load to stg
		EXEC @status = [dbo].[p_adj_save]
								@adjustment_id = @adjustment_id OUTPUT,
								@type_id = @adj_type_id_crv_curve,
								@reporting_date = @param_reporting_date,
								@comment = @comment
		;

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'@adjustment_id: ' + convert(varchar,@adjustment_id)
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Inserting ' + @param_src_table_name + ' to ' + @table_type_name
		END

		-- insert data with sp_executesql
		exec sp_executesql @nsql ,@paramDef ,@reporting_date			=@param_reporting_date
											,@adjustment_id				=@adjustment_id
											,@scenario_id				=@param_scenario_id
											,@modified_by				=@modified_by
											,@lot_fn_code				=@lot_fn_code
											,@crv_type					=@crv_type
											,@crv_type_name				=@crv_type_name
											,@crv_val_type				=@crv_val_type
											,@src_sys					=@src_sys
											,@crv_begin_date			=@crv_begin_date
											,@crv_end_date				=@crv_end_date
											,@u_type_name				=@u_type_name
											,@crv_ref_name				=@crv_ref_name
											,@fwd_nbr					=@fwd_nbr
											,@fwd_time_unit				=@fwd_time_unit
											,@ccy						=@ccy
											,@day_count_con				=@day_count_con
											,@busi_day_con				=@busi_day_con
											,@cal						=@cal
											,@value_amount1				=@value_amount1
		;

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Running HMS p_etl_adj_xtr_crv_curve'
		END

		-- run hms
		EXEC @status = [dbo].[p_etl_adj_xtr_crv_curve]
                            @portfolio_date = @param_reporting_date,
                            @del_data_after_loading = 0,
                            @div_rate_by_100 = 0,
                            @adjustment_id = @adjustment_id,
                            @debug = @param_is_test

		IF @param_is_test = 1
		BEGIN
			declare @hms_result nvarchar(max);

			select @hms_result = FORMATMESSAGE('Inserted %i curve to t_crv_curve and %i curve point to t_crv_curve_point_value' ,count(distinct crv.curve_code), count(p.curve_point_value_id))
			from t_crv_curve crv join
				t_crv_curve_point_value p on crv.curve_id=p.curve_id
			where exists (
				select etl.curve_code
				from t_etl_adj_data_crv_curve etl
				where etl.adjustment_id=@adjustment_id
					and etl.curve_code=crv.curve_code
			);

			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + @hms_result;
		END

		SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
		IF @error <> 0 OR @status <> 0
		  BEGIN
			RAISERROR('HMS encountered an error.', 16, 1, @proc_name, @error)
			RETURN 
		  END	

		COMMIT;
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK;
		
		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'
							+ char(13)+char(10)+char(9)+'##################################################';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = @param_is_test,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
	END CATCH

	--add logging here
	set @ale_message = N'End: ' + @proc_name
						+ char(13)+char(10)+char(9)+'##################################################';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_FINISH', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				--@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				--@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = @param_is_test,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

-- Return succes
RETURN (0) 


GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_lgd_main' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_lgd_main] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_lgd_main] has NOT been altered due to errors!'
END
GO
