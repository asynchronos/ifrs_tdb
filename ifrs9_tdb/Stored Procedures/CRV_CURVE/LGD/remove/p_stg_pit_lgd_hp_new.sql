
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_lgd_hp_new' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pit_lgd_hp_new] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pit_lgd_hp_new] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pit_lgd_hp_new]...'
GO

ALTER PROCEDURE [dbo].[p_stg_pit_lgd_hp_new] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime,
	@param_is_test			bit			= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	Stored procedure for population t_etl_adj_data_crv_curve	  *
-- *				by looping each source table and each scenario.			      *
-- *				This procedure will chain to child procedure 			      *
-- *				for populate adj_data and run HMS process.	  			      *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

	-- variables for t_ecl_process_log_tbank
	declare @log_pid				int;
	declare @log_process_type		varchar(20)			=	'HP-NEW';
	declare @log_model_name			varchar(100)		=	'Pop HP-NEW LGD';
	declare @log_result_table		varchar(50)			=	't_etl_adj_data_crv_curve';

	-- variables for ale
	declare @ale_message			d_ale_message;
	declare @ale_context			xml;
	declare @ale_correlation_guid	UNIQUEIDENTIFIER;
	declare @ale_event_log_guid		UNIQUEIDENTIFIER;

-- Declare local variables	
	declare @table_name nvarchar(256);
	declare @scenario_id d_id;
	declare @scenario_name d_name;
	declare @scenario_id_str d_name;
	declare @stage d_name;
	declare @src_field_name d_name;

-- Start actual procedure flow
	IF @param_reporting_date is null 
	BEGIN 
		select @error_msg = isnull(@param_reporting_date,',@param_reporting_date');
		set @error_msg = N'This procedure must send parameter: ' + substring(@error_msg,2,len(@error_msg))
		RAISERROR (@error_msg, 16, 1, @proc_name)
		RETURN 1
	END

	--add logging here
	set @ale_message = N'Start: ' + @proc_name
						+ N'(@param_reporting_date=' + CONVERT(varchar,@param_reporting_date)
						+ char(13)+char(10)+char(9)+N',@param_is_test=' + CONVERT(varchar,@param_is_test)
						+ N')';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_START', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = @param_is_test,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

	BEGIN TRY

		declare pit_lgd_hp_new_cursor cursor local static for
		-- select target source table name and scenario mapping
		select t.TABLE_NAME, stage, convert(varchar,se.scenario_id) as scenario_id_str, se.scenario_name, se.src_field_name
		from INFORMATION_SCHEMA.TABLES t
			join ( -- create configuration table with inline sql, may move to physical table in the future
				select s.scenario_id, s.scenario_name
					,case
						when s.scenario_id in (3,4,5,51)	then	'1'
						when s.scenario_id in (54,55,56,57)	then	'2'
						when s.scenario_id in (110)			then	'3'
						else 'unknown' end as stage
					,N'LGD' as flow_type
					,case s.scenario_id
						when 3		then	'lgd_flat_stage1'
						when 4		then	'lgd_worst_stage1'
						when 5		then	'lgd_best_stage1'
						when 51		then	'lgd_longrun'
						when 54		then	'lgd_flat_stage2'
						when 55		then	'lgd_worst_stage2'
						when 56		then	'lgd_best_stage2'
						when 57		then	'lgd_longrun'
						when 110	then	'lgd_longrun'
						else 'unknown' end as src_field_name
				from t_scenario s
				where s.scenario_id in (3,4,5,51,54,55,56,57,110)
			) se on 1=1 -- use 1=1 because not have word stageX in table name
				and substring(t.TABLE_NAME,charindex(N'stage',t.TABLE_NAME,1)+5,1) = se.stage -- must have word 'stageX' in table name for join with configuration table
				or (t.TABLE_NAME = 't_stg_hp_new_pit_lgd_stage2_tbank' and se.stage = 3)
		where t.TABLE_NAME in ('t_stg_hp_new_pit_lgd_stage1_tbank','t_stg_hp_new_pit_lgd_stage2_tbank') -- change product table here, may change to dynamic sql for easy config and mantinance in the future
		
		open pit_lgd_hp_new_cursor
		fetch next from pit_lgd_hp_new_cursor into @table_name, @stage, @scenario_id_str, @scenario_name, @src_field_name

		IF @@FETCH_STATUS <> 0 -- check status before loop, for check table exists or not.
		BEGIN
			IF @param_is_test = 1
			BEGIN
				PRINT CHAR(13)+CHAR(10)+CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + FORMATMESSAGE('Source table %s do not exists','t_stg_hp_new_pit_lgd_stage1_tbank,t_stg_hp_new_pit_lgd_stage2_tbank')
			END
			-- add logging here
			--EXEC p_ale_log_event @event_name    = 'new event name',
  --                   @message       = @message,
  --                   @context_value = @proc_name,
  --                   @event_log_id  = @event_log_id OUTPUT;
		END

		While @@FETCH_STATUS = 0
		BEGIN
			
			BEGIN TRY -- handle error for continue loop to next source
				-- execute stored procedure for populate data and load hms of each table with each scenario
				EXEC p_stg_pit_lgd_main @param_reporting_date   = @param_reporting_date,
					   @param_src_table_name   = @table_name,
					   @param_scenario_id      = @scenario_id_str,
					   @param_scenario_name    = @scenario_name,
					   @param_stage			   = @stage,
					   @param_src_field_name   = @src_field_name,
					   @param_is_test          = @param_is_test
			END TRY
			BEGIN CATCH -- print error and continue, user severity 10 or lower
				--add logging here
				set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').';
				set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

				EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
							@event_type                = N'E',
							@context                   = @ale_context,
							@message                   = @ale_message,
							@event_type_detail         = N'E',
							--@correlation_guid          = @ale_correlation_guid OUTPUT,
							--@timestamp_utc             = N'',
							--@event_log_guid            = @ale_event_log_guid OUTPUT,
							@use_synchronous_logging   = 0,
							@print_ind                 = @param_is_test,
							@print_level               = 0,
							@calling_proc_name         = @proc_name;
		
				SELECT @error_msg =  @ale_message
						,@error_severity = 10
						,@error_state	 = ERROR_STATE();
				RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
			END CATCH
		
			fetch next from pit_lgd_hp_new_cursor into @table_name, @stage, @scenario_id_str, @scenario_name, @src_field_name
		END
		
		close pit_lgd_hp_new_cursor
		deallocate pit_lgd_hp_new_cursor
	
	END TRY
	BEGIN CATCH -- for catch random error
		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = @param_is_test,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)

	END CATCH

	--add logging here
	set @ale_message = N'End: ' + @proc_name;
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_FINISH', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				--@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				--@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = @param_is_test,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

-- Return succes
RETURN (0) 

GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_lgd_hp_new' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_lgd_hp_new] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_lgd_hp_new] has NOT been altered due to errors!'
END
GO
