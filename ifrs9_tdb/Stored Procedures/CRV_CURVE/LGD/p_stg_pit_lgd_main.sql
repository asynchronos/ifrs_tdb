
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_lgd_main' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pit_lgd_main] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pit_lgd_main] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pit_lgd_main]...'
GO

ALTER PROCEDURE [dbo].[p_stg_pit_lgd_main] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime,
	@param_src_table_name	nvarchar(256),
	@param_scenario_id		d_name,
	@param_scenario_name	d_name,
	@param_stage			d_name,
	@param_src_field_name	d_name,
	@param_is_test			bit				= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	This stored procedure will populate data for				  *
-- *				t_etl_adj_data_crv_curve and run HMS process to live table	  *
-- *				t_crv_curve and t_crv_curve_point_value.					  *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_src_table_name: point in time source table name      *
-- *				- @param_scenario_id: scenario_id/list of scenario_id		  *
-- *				- @param_scenario_name: scenario_name						  *
-- *				- @param_stage: stage name for mapping table/column			  *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

	-- variables for ale
	declare @ale_message			d_ale_message;
	declare @ale_context			xml;
	declare @ale_correlation_guid	UNIQUEIDENTIFIER;
	declare @ale_event_log_guid		UNIQUEIDENTIFIER;

-- Declare local variables
	--declare @reporting_date			datetime					= '2018-03-31';
	--declare @src_table_name			d_name						= N't_stg_hl_pit_lgd_stage2_tbank';
	--declare @scenario_id			d_id						= 54;

	declare @adjustment_id			d_id;
	declare @adj_type_id_crv_curve	d_id;

	-- list of constant variable
	declare @comment				d_long_description			= N'LGD|'+@param_src_table_name+N'|'+@param_scenario_name --N'TBANK';
	declare @modified_by			d_user						= @proc_name; -- change to proc name later, OBJECT_NAME( @@PROCID )|dbo.fn_user()
	declare @lot_fn_code			d_std_record_function		= N'ECLSEG';
	declare @table_type_name		d_name						= N't_etl_adj_data_crv_curve';

	declare @crv_type				d_curve_type				= 9; -- pd=8, lgd=9
	declare @crv_type_name			d_name						= N'LGD Curve'; -- pd=8, lgd=9
	declare @crv_val_type			d_value_type				= N'LGD'; -- pd=8, lgd=9
	declare @src_sys				d_source_system				= @param_src_table_name+N'|'+@param_scenario_id --N'TBANK';
	declare @crv_begin_date			datetime					= @param_reporting_date;
	declare @crv_end_date			datetime					= '9999-12-31';
	declare @u_type					d_usage_type				= 1; -- 1=Actual, 2=Stress
	declare @u_type_name			d_name						= N'Actual'; -- 1=Actual, 2=Stress
	declare @crv_ref				d_reference_code			= N'INT'; -- ADM1=Primary Administered Rate, ADM2=Secondary Administered Rate, CB=Central Bank curve, GOV=Government curve, INT=Internal curve, MKT=Market curve, REG=Defined by Regulator
	declare @crv_ref_name			d_name						= N'Internal curve'; -- ADM1=Primary Administered Rate, ADM2=Secondary Administered Rate, CB=Central Bank curve, GOV=Government curve, INT=Internal curve, MKT=Market curve, REG=Defined by Regulator
	declare @fwd_nbr				d_nbr_of_units				= 1;
	declare @fwd_time_unit			d_time_unit					= N'M';      
	declare @ccy					d_currency					= N'THB'; -- can use from t_ecl_segmentation      
	declare @day_count_con			d_day_count_convention		= N'ACT/ACT';      
	declare @busi_day_con			d_business_day_convention	= N'C';      
	declare @cal					d_calendar					= N'standard';
	declare @value_amount1			d_amount					= 0; -- cummulative value   
	declare @field_char_cust_element1		d_name				= N''; 
	declare @field_char_cust_element2		d_name				= N''; 
	declare @field_char_cust_element3		d_name				= N''; 
	declare @field_char_cust_element4		d_name				= N''; 
	declare @field_char_cust_element5		d_name				= N''; 
	declare @field_num_cust_element1		d_name				= N''; 
	declare @field_num_cust_element2		d_name				= N''; 
	declare @field_num_cust_element3		d_name				= N''; 
	declare @field_num_cust_element4		d_name				= N''; 
	declare @field_num_cust_element5		d_name				= N''; 
	declare @field_term_start_date1			d_name				= N''; 

	-- control variable
	declare @data_level				d_name						= N'deal'	-- deal,segment,subsegment
	declare @is_nonretail			d_name						= N'N'

-- Start actual procedure flow
	IF @param_reporting_date is null 
		or @param_src_table_name is null
		or @param_scenario_id is null
		or @param_scenario_name is null
		or @param_stage is null
		or @param_src_field_name is null
	BEGIN
		select @error_msg = isnull(@param_reporting_date,',@param_reporting_date')
							+ isnull(@param_src_table_name,',@param_src_table_name')
							+ isnull(@param_scenario_id,',@param_scenario_id')
							+ isnull(@param_scenario_name,',@param_scenario_name')
							+ isnull(@param_stage,',@param_stage')
							+ isnull(@param_src_field_name,',@param_src_field_name');
		set @error_msg = N'This procedure must send parameter: ' + substring(@error_msg,2,len(@error_msg))
		RAISERROR (@error_msg, 16, 1, @proc_name)
		RETURN 1
	END

	--add logging here
	set @ale_message = N'Start: ' + @proc_name
						+ N'(@param_reporting_date=' + CONVERT(varchar,@param_reporting_date)
						+ char(13)+char(10)+char(9)+N',@param_src_table_name=' + CONVERT(varchar,@param_src_table_name)
						+ char(13)+char(10)+char(9)+N',@param_scenario_id=' + CONVERT(varchar,@param_scenario_id)
						+ char(13)+char(10)+char(9)+N',@param_scenario_name=' + CONVERT(varchar,@param_scenario_name)
						+ char(13)+char(10)+char(9)+N',@param_stage=' + CONVERT(varchar,@param_stage)
						+ char(13)+char(10)+char(9)+N',@param_src_field_name=' + CONVERT(varchar,@param_src_field_name)
						+ char(13)+char(10)+char(9)+N',@param_is_test=' + CONVERT(varchar,@param_is_test)
						+ N')';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_START', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

	declare @nsql nvarchar(max);
	declare @nsql_top100 nvarchar(10);
	declare @nsql_filter nvarchar(255);

	-- ad_hoc for support sub segment level
	select @data_level		= case when @param_src_table_name in ('t_retail_hp_lgd_fla_stage1_tbank','t_retail_hp_lgd_fla_stage2_tbank') then 'subsegment'
								else 'deal' end;
	select @is_nonretail	= case when @param_src_table_name in ('t_nonretail_lgd_output_tbank','t_nonretail_smes_lgd_output_tbank') then 'Y'
								else 'N' end;

	declare @nsql_fields	nvarchar(255);
	declare @nsql_join_segment		nvarchar(255);

	BEGIN TRY -- prepare dynamic t-sql
		select @field_char_cust_element1		= case when @data_level in ('subsegment') or @is_nonretail='Y' then ''''''
												  else 'src.date_matur' end
				,@field_char_cust_element2		= '@scenario_id'
				,@field_char_cust_element3		= case when @data_level in ('subsegment') then 'src.segment_name'
												  else 'p.pool_name' end
				,@field_char_cust_element4		= '@reporting_date'
				,@field_char_cust_element5		= ''''''
				,@field_num_cust_element1		= case when @data_level in ('subsegment') then 'src.segment_id'		
												  else 'p.pool_id' end
				,@field_num_cust_element2		= case when @data_level in ('subsegment') or @is_nonretail='Y' then '0.0'
												  else 'src.remaining_maturity' end
				,@field_num_cust_element3		= case when @data_level in ('subsegment') or @is_nonretail='Y' then '0.0'
												  else 'src.remaining_term' end
				,@field_num_cust_element4		= case when @data_level in ('subsegment') or @is_nonretail='Y' then '0.0'
												  else 'src.stage' end
				,@field_num_cust_element5		= '0.0'
				,@field_term_start_date1		= case when @is_nonretail='Y' then ' eomonth(dateadd(month, case when '+@param_stage+'=3 then 0 else 1 end,@reporting_date))'
												  else ' eomonth(dateadd(month, case when '+@param_stage+'=3 then 0 else src.forecast_month end,@reporting_date))' end
		;

		select @nsql_top100 = case @param_is_test when 0 then '' else 'top 100 ' end
				,@nsql_fields = case when @data_level in ('subsegment')
											then ' ,CONCAT(src.curve_name,''|'','+@param_stage+',''|'',@scenario_id,''|'',@crv_val_type) as curve_code '
												+' ,src.curve_name as curve_name'
												+' ,src.curve_name as curve_desc'
									 else ' ,CONCAT(seg.contract_id,''|'','+@param_stage+',''|'',@scenario_id,''|'',@crv_val_type) as curve_code '
										+' ,src.deal_id as curve_name'
										+' ,src.acc_id as curve_desc' end
				,@nsql_join_segment   = case when @data_level in ('subsegment')
											then ' '
										else N' join t_ecl_segmentation seg on src.deal_id=seg.deal_tfi_id'
											+' and seg.lot_id = (select max(lot.lot_id) from t_lot_definition lot where lot.std_record_function_code = @lot_fn_code)' 
											+' join t_pool p on p.pool_id=seg.segment_pool_id' end
				,@nsql_filter = case when @param_stage='3' and @is_nonretail='N' then N' where src.as_of_date = @reporting_date and src.forecast_month=1'
									 when @is_nonretail='Y' then N' where src.date_tx = @reporting_date and src.path=''Liquidation'' and deal_id not in (''FP001356500179'',''FP001359501124'',''FP001359501221'',''FP001359502065'',''FP001359501166'',''FP001359501182'',''FP001359501205'')'-- temporay filter until FP not duplicate
									 else N' where src.as_of_date = @reporting_date' end
		;
		
		select @nsql = 'INSERT INTO t_etl_adj_data_crv_curve
			(adjustment_id, line_date, curve_code, curve_name, curve_desc, curve_type, source_system, curve_value_type, usage_type, curve_reference, forward_nbr_of_time_units, forward_time_unit, currency, day_count_convention, business_day_convention, calendar, retry_count, start_validity_date, end_validity_date, value_perc1, value_amount1, term_start_date1, char_cust_element1, char_cust_element2, char_cust_element3, char_cust_element4, char_cust_element5, num_cust_element1, num_cust_element2, num_cust_element3, num_cust_element4, num_cust_element5, last_modified, modified_by)'
		
		select @nsql = @nsql + 
		N'
		select '+@nsql_top100+' @adjustment_id as adjustment_id
			,CURRENT_TIMESTAMP		as line_date'
			+@nsql_fields+'
			,@crv_type_name			as curve_type
			,@src_sys				as source_system
			,@crv_val_type			as curve_value_type
			,@u_type_name			as usage_type
			,@crv_ref_name			as curve_reference
			,@fwd_nbr				as forward_nbr_of_time_units
			,@fwd_time_unit			as forward_time_unit'
			+case when @data_level in ('segment','subsegment')
				then ',''THB''		as currency'
			 else ',seg.currency	as currency' end +
			',@day_count_con			as day_count_convention
			,@busi_day_con			as business_day_convention
			,@cal					as calendar
			,0						as retry_count
			,@crv_begin_date		as start_validity_date
			,@crv_end_date			as end_validity_date
			,src.'+@param_src_field_name+'		as value_perc1
			,@value_amount1						as value_amount1
			,'+@field_term_start_date1+'		as term_start_date1
			,'+@field_char_cust_element1+'		as char_cust_element1
			,'+@field_char_cust_element2+'		as char_cust_element2
			,'+@field_char_cust_element3+'		as char_cust_element3
			,'+@field_char_cust_element4+'		as char_cust_element4
			,'+@field_char_cust_element5+'		as char_cust_element5
			,'+@field_num_cust_element1+'		as num_cust_element1
			,'+@field_num_cust_element2+'		as num_cust_element2
			,'+@field_num_cust_element3+'		as num_cust_element3
			,'+@field_num_cust_element4+'		as num_cust_element4
			,'+@field_num_cust_element5+'		as num_cust_element5
			,CURRENT_TIMESTAMP					as last_modified
			,@modified_by						as modified_by
		from '+@param_src_table_name+' as src'
			+@nsql_join_segment+
			+@nsql_filter
		;

		IF len(@nsql) > 4000
		BEGIN
			set @ale_message = N'SQL string is over than 4000 character.';
			set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql FOR XML PATH);

			EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
						@event_type                = N'E',
						@context                   = @ale_context,
						@message                   = @ale_message,
						@event_type_detail         = N'E',
						--@correlation_guid          = @ale_correlation_guid OUTPUT,
						--@timestamp_utc             = N'',
						--@event_log_guid            = @ale_event_log_guid OUTPUT,
						@use_synchronous_logging   = 0,
						@print_ind                 = @param_is_test,
						@print_level               = 0,
						@calling_proc_name         = @proc_name;

			set @error_msg = @ale_message
			RAISERROR (@error_msg, 16, 1, @proc_name)
			RETURN 1
		END

		declare @paramDef nvarchar(max);
		set @paramDef = N'@reporting_date datetime
						,@adjustment_id			d_id
						,@scenario_id			d_name
						,@modified_by			d_user
						,@lot_fn_code			d_std_record_function
						,@crv_type				d_curve_type
						,@crv_type_name			d_name
						,@crv_val_type			d_value_type
						,@src_sys				d_source_system
						,@crv_begin_date		datetime
						,@crv_end_date			datetime
						,@u_type_name			d_name
						,@crv_ref_name			d_name
						,@fwd_nbr				d_nbr_of_units
						,@fwd_time_unit			d_time_unit
						,@ccy					d_currency
						,@day_count_con			d_day_count_convention
						,@busi_day_con			d_business_day_convention
						,@cal					d_calendar
						,@value_amount1			d_amount
		';-- end prepare dynamic t-sql

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + FORMATMESSAGE(N'Curve|SourceTable|Scenario: %s|%s|%s',@crv_type_name,@param_src_table_name,@param_scenario_id)
		END

		--RAISERROR(@nsql, 16, 1, @proc_name) -- force stop program for view and testing
	END TRY
	BEGIN CATCH
		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'
							+ char(13)+char(10)+char(9)+'##################################################';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = 1,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
	END CATCH

	BEGIN TRANSACTION;
	BEGIN TRY -- start execute main process		

		-- get table type id
		SELECT @adj_type_id_crv_curve   = (SELECT typ.type_id FROM t_adj_type typ JOIN t_adj_category cat ON cat.category_id = typ.category_id WHERE typ.type_name = @table_type_name);
		-- prepare adjustment before load to stg
		EXEC @status = [dbo].[p_adj_save]
								@adjustment_id = @adjustment_id OUTPUT,
								@type_id = @adj_type_id_crv_curve,
								@reporting_date = @param_reporting_date,
								@comment = @comment
		;

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'@adjustment_id: ' + convert(varchar,@adjustment_id)
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Inserting ' + @param_src_table_name + ' to ' + @table_type_name
		END

		-- insert data with sp_executesql
		exec sp_executesql @nsql ,@paramDef ,@reporting_date			=@param_reporting_date
											,@adjustment_id				=@adjustment_id
											,@scenario_id				=@param_scenario_id
											,@modified_by				=@modified_by
											,@lot_fn_code				=@lot_fn_code
											,@crv_type					=@crv_type
											,@crv_type_name				=@crv_type_name
											,@crv_val_type				=@crv_val_type
											,@src_sys					=@src_sys
											,@crv_begin_date			=@crv_begin_date
											,@crv_end_date				=@crv_end_date
											,@u_type_name				=@u_type_name
											,@crv_ref_name				=@crv_ref_name
											,@fwd_nbr					=@fwd_nbr
											,@fwd_time_unit				=@fwd_time_unit
											,@ccy						=@ccy
											,@day_count_con				=@day_count_con
											,@busi_day_con				=@busi_day_con
											,@cal						=@cal
											,@value_amount1				=@value_amount1
		;

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Running HMS p_etl_adj_xtr_crv_curve'
		END

		-- run hms
		EXEC @status = [dbo].[p_etl_adj_xtr_crv_curve]
                            @portfolio_date = @param_reporting_date,
                            @del_data_after_loading = 0,
                            @div_rate_by_100 = 0,
                            @adjustment_id = @adjustment_id,
                            @debug = @param_is_test

		IF @param_is_test = 1
		BEGIN
			declare @hms_result nvarchar(max);

			select @hms_result = FORMATMESSAGE('Inserted %i curve to t_crv_curve and %i curve point to t_crv_curve_point_value' ,count(distinct crv.curve_code), count(p.curve_point_value_id))
			from t_crv_curve crv join
				t_crv_curve_point_value p on crv.curve_id=p.curve_id
			where exists (
				select etl.curve_code
				from t_etl_adj_data_crv_curve etl
				where etl.adjustment_id=@adjustment_id
					and etl.curve_code=crv.curve_code
			);

			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + @hms_result;
		END

		SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
		IF @error <> 0 OR @status <> 0
		  BEGIN
			RAISERROR('HMS encountered an error.', 16, 1, @proc_name, @error)
			RETURN 
		  END	

		COMMIT;
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK;
		
		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'
							+ char(13)+char(10)+char(9)+'##################################################';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = 1,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
	END CATCH

	--add logging here
	set @ale_message = N'End: ' + @proc_name
						+ char(13)+char(10)+char(9)+'##################################################';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_FINISH', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				--@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				--@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

-- Return succes
RETURN (0) 

GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_lgd_main' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_lgd_main] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_lgd_main] has NOT been altered due to errors!'
END
GO
