
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_lgd_nonretail' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pit_lgd_nonretail] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pit_lgd_nonretail] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pit_lgd_nonretail]...'
GO

ALTER PROCEDURE [dbo].[p_stg_pit_lgd_nonretail] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime,
	@param_is_test			bit			= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	Stored procedure for population t_etl_adj_data_crv_curve	  *
-- *				by looping each source table and each scenario.			      *
-- *				This procedure will chain to child procedure 			      *
-- *				for populate adj_data and run HMS process.	  			      *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

	-- variables for ale
	declare @ale_message						d_ale_message;
	declare @ale_context						xml;
	declare @ale_correlation_guid				UNIQUEIDENTIFIER;
	declare @ale_event_log_guid					UNIQUEIDENTIFIER;

-- Declare local variables
	declare @cob_date							datetime;
	declare @max_date							datetime				=	'9999-12-31';
	declare @entity								d_entity				=	N'TBANK';
	declare @lot_fn_code						d_std_record_function	=	N'ECLSEG';
	declare @max_lot_id							d_id;
	declare @scenario_context_id				d_id					=	301003;
	
	declare @src_model_name						d_name					=	N'NonRetail';
	declare @src_table_name1					d_name					=	N't_nonretail_lgd_output_tbank';
	declare @src_table_name2					d_name					=	N''
	declare @src_table_name3					d_name					=	N''
	declare @table_type_name					d_name					=	N't_etl_adj_data_crv_curve';
	
	-- variables for t_ecl_process_log_tbank
	declare @log_pid							int;
	declare @log_process_type					varchar(20)			=	@src_model_name;
	declare @log_model_name						varchar(100)		=	N'Pop '+@src_model_name+' LGD';
	declare @log_result_table					varchar(50)			=	@table_type_name;

	-- variables for cursor
	declare @cursor_scenario_id				d_id;
	--declare @scursor_cenario_id_str		d_name;
	declare @cursor_scenario_name			d_name;
	declare @cursor_stage					d_name;
	declare @cursor_src_value_type			d_name;
	declare @cursor_src_table_name			d_name;
	declare @cursor_src_field_name			d_name;
	declare @cursor_model_name				d_name;

	-- assign cob date
	select @cob_date = isnull(@param_reporting_date,cob_date) from t_entity where entity = @entity;

	--add logging here
	set @ale_message = N'Start: ' + @proc_name
						+ N'(@param_reporting_date=' + CONVERT(varchar,@param_reporting_date)
						+ char(13)+char(10)+char(9)+N',@param_is_test=' + CONVERT(varchar,@param_is_test)
						+ char(13)+char(10)+char(9)+N',@cob_date=' + CONVERT(varchar,@cob_date)
						+ N')';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_START', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;
	
	--/*Start Process Log*/ -- change to proc in the future
	--SET @log_pid = (select isnull(max(process_id),0)+1 
	--				from t_ecl_process_log_tbank
	--				where cob_date = @cob_date);

	--insert into t_ecl_process_log_tbank (cob_date,process_id,process_type,process_name,model_name,result_table,start_time,process_status)
	--select  @cob_date, @log_pid, @log_process_type, @proc_name, @log_model_name, @log_result_table, getdate(),'Processing';
	--/*End Process Log*/

-- Start actual procedure flow
	BEGIN TRY

		declare pit_lgd_nonretail_cursor cursor local static for 
		-- select target source table name and scenario mapping
		select se.scenario_id, se.scenario_name, stage, se.value_type, t.TABLE_NAME, se.src_field_name, se.model_name
		from INFORMATION_SCHEMA.TABLES t
			join ( -- create configuration table with inline sql, may move to physical table in the future
				select s.scenario_id, s.scenario_name
					,case
						when CHARINDEX('stage 1',lower(s.scenario_name)) > 0	then	'1'
						when CHARINDEX('stage 2',lower(s.scenario_name)) > 0	then	'2'
						when CHARINDEX('stage 3',lower(s.scenario_name)) > 0	then	'3'
						else 'unknown' end as stage
					,N'LGD' as value_type
					,N'lgd' as src_field_name
					,@src_model_name as model_name
				from t_scenario s
				where s.scenario_id in (3,4,5,51,54,55,56,57,110)
			) se on 1=1 
				--and (substring(t.TABLE_NAME,charindex(N'stage',t.TABLE_NAME,1)+5,1) = se.stage -- must have word 'stageX' in table name for join with configuration table
				--	or (t.TABLE_NAME = @src_table_name2 and se.stage = 3))
		where t.TABLE_NAME in (@src_table_name1,@src_table_name2,@src_table_name3) -- change product table here, may change to dynamic sql for easy config and mantinance in the future
		
		open pit_lgd_nonretail_cursor
		fetch next from pit_lgd_nonretail_cursor into @cursor_scenario_id, @cursor_scenario_name, @cursor_stage, @cursor_src_value_type, @cursor_src_table_name, @cursor_src_field_name, @cursor_model_name

		IF @@FETCH_STATUS <> 0 -- check status before loop, for check table exists or not.
		BEGIN
			--add logging here
			set @ale_message = FORMATMESSAGE('Source table %s,%s,%s do not exists.',@src_table_name1,@src_table_name2,@src_table_name3)
			set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

			EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
						@event_type                = N'E',
						@context                   = @ale_context,
						@message                   = @ale_message,
						@event_type_detail         = N'E',
						--@correlation_guid          = @ale_correlation_guid OUTPUT,
						--@timestamp_utc             = N'',
						--@event_log_guid            = @ale_event_log_guid OUTPUT,
						@use_synchronous_logging   = 0,
						@print_ind                 = @param_is_test,
						@print_level               = 0,
						@calling_proc_name         = @proc_name;
		
			SELECT @error_msg =  @ale_message
					,@error_severity = ERROR_SEVERITY()
					,@error_state	 = ERROR_STATE();
			RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
		END

		While @@FETCH_STATUS = 0
		BEGIN
				-- execute stored procedure for populate data and load hms of each table with each scenario
				EXEC p_stg_pit_lgd_main_nrt_smes @param_reporting_date   = @param_reporting_date,
												   @param_scenario_id      = @cursor_scenario_id,
												   @param_scenario_name    = @cursor_scenario_name,
												   @param_stage			   = @cursor_stage,
												   @param_src_value_type   = @cursor_src_value_type,
												   @param_src_table_name   = @cursor_src_table_name,
												   @param_src_field_name   = @cursor_src_field_name,
												   @param_model_name	   = @cursor_model_name,
												   @param_is_test          = @param_is_test
				;
		
			fetch next from pit_lgd_nonretail_cursor into @cursor_scenario_id, @cursor_scenario_name, @cursor_stage, @cursor_src_value_type, @cursor_src_table_name, @cursor_src_field_name, @cursor_model_name
		END
		
		close pit_lgd_nonretail_cursor
		deallocate pit_lgd_nonretail_cursor
	
	END TRY
	BEGIN CATCH -- for catch random error
		
		--/******************************************************************/
		--/*End Process Log*/	
		--/******************************************************************/
		--update t_ecl_process_log_tbank 
		--set end_time		=	getdate(),
		--	execution_time	=	cast((getdate() - start_time) as time(0)),
		--	process_status	=	'Failed'
		--where process_id = @log_pid
		--	and cob_date = @cob_date;

		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = 1,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)

	END CATCH

	--/******************************************************************/
	--/*End Process Log*/	
	--/******************************************************************/
	--update t_ecl_process_log_tbank 
	--set end_time		=	getdate(),
	--	execution_time	=	cast((getdate() - start_time) as time(0)),
	--	process_status	=	'Success'
	--where process_id = @log_pid
	--	and cob_date = @cob_date;
	--Print '#################################################################################';

	--add logging here
	set @ale_message = N'End: ' + @proc_name;
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_FINISH', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				--@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				--@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

-- Return succes
RETURN (0) 

GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_lgd_nonretail' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_lgd_nonretail] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_lgd_nonretail] has NOT been altered due to errors!'
END
GO
