

IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_del_crv' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pit_del_crv] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pit_del_crv] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pit_del_crv]...'
GO

ALTER PROCEDURE [dbo].[p_stg_pit_del_crv] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime,
	@param_crv_type			d_curve_type, -- 8=PD, 9=LGD
	@param_is_test			bit			= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	Stored procedure for delete	t_crv_curve and					  *
-- *				t_crv_curve_point_value after re-run segmentation.		      *
-- *				Note: segmentation process will generate new contract_id.     *
-- *				So, We must delete the old one for prevent dup curve with     *
-- *				the same deal,stage,scenaio in same reporting date			  *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_crv_type: populated recored                          *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

-- Declare local variables	
	declare @max_date		datetime		= '9999-12-31';

-- Start actual procedure flow
	IF @param_reporting_date is null 
		or @param_crv_type is null
	BEGIN 
		select @error_msg = isnull(@param_reporting_date,',@param_reporting_date')
							+ isnull(@param_crv_type,',@param_crv_type');
		set @error_msg = N'This procedure must send parameter: ' + substring(@error_msg,2,len(@error_msg))
		RAISERROR (@error_msg, 16, 1, @proc_name)
		RETURN 1
	END

	IF @param_is_test = 1
	BEGIN
		PRINT CHAR(13)+CHAR(10)+CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Start: ' + @proc_name
	END
		
	BEGIN TRY

		-- 1.1 delete t_crv_curve_point_value by curve_id
		DELETE t_crv_curve_point_value
		WHERE exists(
			select c.curve_id -- select curve_id for remove t_crv_curve_point value
			from t_crv_curve c
			where c.source_system NOT IN ('FS')--,'PiT PD : Transition Matrix Assignment') -- 'FS'=OSX Stardard,'PiT PD : Transition Matrix Assignment'=WK's custom rule t_mex_type 11650006
			and c.curve_type		= @param_crv_type
			and c.curve_begin_date	= @param_reporting_date
			-- exists 
			and c.curve_id=t_crv_curve_point_value.curve_id
		)

		-- 1.2 roll end_validity_date of table t_crv_curve_point_value from cob_date-1 to max date
		UPDATE t_crv_curve_point_value
		   SET end_validity_date        = @max_date,
			   last_modified            = CURRENT_TIMESTAMP,
			   modified_by              = dbo.fn_user() 
		WHERE exists(
				select c.curve_id -- select curve_id for remove t_crv_curve_point value
				from t_crv_curve c
				where c.source_system NOT IN ('FS')--,'PiT PD : Transition Matrix Assignment') -- 'FS'=OSX Stardard,'PiT PD : Transition Matrix Assignment'=WK's custom rule t_mex_type 11650006
				and c.curve_type		= @param_crv_type
				and c.curve_end_date	= DATEADD(day,-1,@param_reporting_date) 
				-- exists 
				and c.curve_id=t_crv_curve_point_value.curve_id
				and c.curve_end_date=t_crv_curve_point_value.end_validity_date
			)

		-- 2. delete & roll date t_crv_curve

		-- 2.1 delete t_crv_curve by curve_id
		DELETE t_crv_curve
		WHERE source_system NOT IN ('FS')--,'PiT PD : Transition Matrix Assignment') -- 'FS'=OSX Stardard,'PiT PD : Transition Matrix Assignment'=WK's custom rule t_mex_type 11650006
			and curve_type			= @param_crv_type
			and curve_begin_date	= @param_reporting_date

		-- 2.2 roll curve_end_date of table t_crv_curve from cob_date-1 to max date
		UPDATE t_crv_curve
		   SET curve_end_date       = @max_date,
			   last_modified        = CURRENT_TIMESTAMP,
			   modified_by          = dbo.fn_user() 
		WHERE source_system NOT IN ('FS')--,'PiT PD : Transition Matrix Assignment') -- 'FS'=OSX Stardard,'PiT PD : Transition Matrix Assignment'=WK's custom rule t_mex_type 11650006
			and curve_type		= @param_crv_type
			and curve_end_date	= DATEADD(day,-1,@param_reporting_date) 
	
	END TRY
	BEGIN CATCH -- for catch random error
		--SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
		
		SELECT @error_msg =  char(13)+char(10)+char(9)+ '@param_reporting_date:' + convert(varchar,@param_reporting_date)
							+char(13)+char(10)+char(9)+ @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'+char(13)+char(10)
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)

		-- add logging here
		--EXEC p_ale_log_event @event_name    = 'new event name',
--                   @message       = @message,
--                   @context_value = @proc_name,
--                   @event_log_id  = @event_log_id OUTPUT;
		RETURN 1
	END CATCH
-- Return succes
RETURN (0) 

GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_del_crv' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_del_crv] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_del_crv] has NOT been altered due to errors!'
END
GO
