
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pop_pdlgd_main_deal' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pop_pdlgd_main_deal] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pop_pdlgd_main_deal] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pop_pdlgd_main_deal]...'
GO

ALTER PROCEDURE [dbo].[p_stg_pop_pdlgd_main_deal] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime,
	@param_scenario_id		d_name,
	@param_scenario_name	d_name,
	@param_stage			d_name,
	@param_src_value_type	d_name,
	@param_src_table_name	d_name,
	@param_src_field_name	d_name,
	@param_model_name		d_name,
	@param_parent_proc_name	d_name			= null,
	@param_is_test			bit				= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	This stored procedure will populate data for				  *
-- *				t_etl_adj_data_crv_curve and run HMS process to live table	  *
-- *				t_crv_curve and t_crv_curve_point_value.					  *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_scenario_id: scenario_id							  *
-- *				- @param_scenario_name: scenario_name						  *
-- *				- @param_stage: stage name for mapping table/column			  *
-- *				- @param_src_value_type: PD/LGD/EAD						      *
-- *				- @param_src_table_name: source table name					  *
-- *				- @param_src_field_name: source field name				      *
-- *				- @param_model_name: model name								  *
-- *				- @param_parent_proc_name: parent stored procedure name		  *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT
DECLARE @proc_timestamp		DATETIME

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7, -- adjust error line
	   @proc_timestamp	= CURRENT_TIMESTAMP

	-- variables for ale
	declare @ale_message						d_ale_message;
	declare @ale_context						xml;
	declare @ale_correlation_guid				UNIQUEIDENTIFIER;
	declare @ale_event_log_guid					UNIQUEIDENTIFIER;

-- Declare local variables
	declare @cob_date							datetime;
	declare @max_date							datetime				=	'9999-12-31';
	declare @entity								d_entity				=	N'TBANK';
	declare @lot_fn_code						d_std_record_function	=	N'ECLSEG';
	declare @max_lot_id							d_id;
	declare @scenario_context_id				d_id					=	301003;

	declare @adjustment_id						d_id					=	0;
	declare @adj_type_id						d_id;
	declare @src_table_name1					d_name					=	@param_src_table_name;
	declare @src_table_name2					d_name					=	N''
	declare @src_table_name3					d_name					=	N''
	declare @table_type_name					d_name					=	N't_etl_adj_data_crv_curve';

	-- variable for dynamic sql
	declare @nsql								nvarchar(max);
	declare @nsql_select						nvarchar(50);
	declare @nsql_column						nvarchar(max);
	declare @nsql_table							nvarchar(max);
	declare @nsql_filter						nvarchar(max);
	declare @nsql_insert						nvarchar(max);
	declare @paramDef							nvarchar(max);
																
	-- variable for insert t_crv_curve
	declare @field_adjustment_id				nvarchar(255);
	declare @field_line_date					nvarchar(255);
	declare @field_curve_code					nvarchar(255);
	declare @field_curve_name					nvarchar(255);
	declare @field_curve_desc					nvarchar(255);
	declare @field_curve_type					nvarchar(255);
	declare @field_source_system				nvarchar(255);
	declare @field_curve_value_type				nvarchar(255);
	declare @field_usage_type					nvarchar(255);
	declare @field_curve_reference				nvarchar(255);
	declare @field_forward_nbr_of_time_units	nvarchar(255);
	declare @field_forward_time_unit			nvarchar(255);
	declare @field_currency						nvarchar(255);
	declare @field_day_count_convention			nvarchar(255);
	declare @field_business_day_convention		nvarchar(255);
	declare @field_calendar						nvarchar(255);
	declare @field_retry_count					nvarchar(255);
	declare @field_start_validity_date			nvarchar(255);
	declare @field_end_validity_date			nvarchar(255);
	declare @field_value_perc1					nvarchar(255);
	declare @field_value_amount1				nvarchar(255);
	declare @field_term_start_date1				nvarchar(255);
	declare @field_char_cust_element1			nvarchar(255);
	declare @field_char_cust_element2			nvarchar(255);
	declare @field_char_cust_element3			nvarchar(255);
	declare @field_char_cust_element4			nvarchar(255);
	declare @field_char_cust_element5			nvarchar(255);
	declare @field_num_cust_element1			nvarchar(255);
	declare @field_num_cust_element2			nvarchar(255);
	declare @field_num_cust_element3			nvarchar(255);
	declare @field_num_cust_element4			nvarchar(255);
	declare @field_num_cust_element5			nvarchar(255);
	declare @field_last_modified				nvarchar(255);
	declare @field_modified_by					nvarchar(255);
		
	SET DATEFORMAT	ymd;	-- Set default dateformat

	-- assign cob date
	select @cob_date = isnull(@param_reporting_date,cob_date) from t_entity where entity = @entity;

	-- assign max lot id
	select @max_lot_id = max(lot.lot_id) from t_lot_definition lot where lot.std_record_function_code = @lot_fn_code;

	IF @param_reporting_date is null 
		or @param_scenario_id is null
		or @param_scenario_name is null
		or @param_stage is null
		or @param_src_value_type is null
		or @param_src_table_name is null
		or @param_src_field_name is null
		or @param_model_name is null
	BEGIN
		select @error_msg = isnull(@param_reporting_date,',@param_reporting_date')
							+ isnull(@param_scenario_id,',@param_scenario_id')
							+ isnull(@param_scenario_name,',@param_scenario_name')
							+ isnull(@param_stage,',@param_stage')
							+ isnull(@param_src_value_type,',@param_src_value_type')
							+ isnull(@param_src_table_name,',@param_src_table_name')
							+ isnull(@param_src_field_name,',@param_src_field_name')
							+ isnull(@param_model_name,',@param_model_name');
		set @error_msg = N'This procedure must send parameter: ' + substring(@error_msg,2,len(@error_msg))
		RAISERROR (@error_msg, 16, 1, @proc_name)
		RETURN 1
	END

	--add logging here
	set @ale_message = N'Start: ' + @proc_name
						+ N'(@param_reporting_date=' + CONVERT(varchar,@param_reporting_date)
						+ char(13)+char(10)+char(9)+N',@param_scenario_id=' + CONVERT(varchar,@param_scenario_id)
						+ char(13)+char(10)+char(9)+N',@param_scenario_name=' + CONVERT(varchar,@param_scenario_name)
						+ char(13)+char(10)+char(9)+N',@param_stage=' + CONVERT(varchar,@param_stage)
						+ char(13)+char(10)+char(9)+N',@param_src_value_type=' + CONVERT(varchar,@param_src_value_type)
						+ char(13)+char(10)+char(9)+N',@param_src_field_name=' + CONVERT(varchar,@param_src_field_name)
						+ char(13)+char(10)+char(9)+N',@param_model_name=' + CONVERT(varchar,@param_model_name)
						+ char(13)+char(10)+char(9)+N',@param_is_test=' + CONVERT(varchar,@param_is_test)
						+ char(13)+char(10)+char(9)+N',@cob_date=' + CONVERT(varchar,@cob_date)
						+ N')';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_START', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

-- Start actual procedure flow	
	DECLARE @data_level		d_name;
	DECLARE @is_nrt_smes	int;

	-- ad_hoc for support sub segment level
	select @data_level		=	case when @param_src_table_name in ('t_retail_hp_pd_fla_stage1_tbank') then 'subsegment'
									when @param_src_table_name in ('t_retail_hp_pd_fla_stage2_tbank') then 'segment'
								else 'deal' end;

	SELECT @is_nrt_smes		=	case  
									when @param_model_name in ('NonRetail&SMEs','NRT_SMEs','NonRetail','SMEs') then 1
								else 0 end;

	DECLARE @local_trancount bit = 0;

	BEGIN TRY
		IF @@TRANCOUNT=0
		BEGIN
			BEGIN TRANSACTION;
			set @local_trancount = 1;
		END

		-- Create adjustmnet_id
		declare @comment	d_long_description		= FORMATMESSAGE(N'%s|%s|%s'
														,@param_src_value_type
														,@param_src_table_name
														,@param_scenario_name)
		-- get table type id
		SELECT @adj_type_id   = (SELECT typ.type_id FROM t_adj_type typ JOIN t_adj_category cat ON cat.category_id = typ.category_id WHERE typ.type_name = @table_type_name);
		-- prepare adjustment before load to stg
		set @adjustment_id = null;
		EXEC @status = [dbo].[p_adj_save]
								@adjustment_id = @adjustment_id OUTPUT,
								@type_id = @adj_type_id,
								@reporting_date = @cob_date,
								@comment = @comment
		;
		
		-- assign field for select & insert
		-- src = source table, seg = t_ecl_segmentation
		-- text must have single-quote/char(39) between text
		-- function, field, numeric do not need single-quote
		select	 @field_adjustment_id					= FORMATMESSAGE(N'%i',@adjustment_id)										-- must update with p_adj_save before run HMS
				,@field_line_date						= FORMATMESSAGE(N'CONVERT(DATETIME,%s)',CHAR(39)+@proc_timestamp+CHAR(39))	-- use proc timestamp for same adjustment_id
				,@field_curve_code						= FORMATMESSAGE(N'CONCAT(ISNULL(CONVERT(NVARCHAR(255),%s),CONVERT(NVARCHAR,seg.contract_id))'
																				+',''|'''
																				+',%s'
																				+',''|'''
																				+',%s'
																				+',''|'''
																				+',%s'
																				+',''|'')'
															,CHAR(39)+CASE WHEN @param_src_value_type='PD' THEN 'seg.element1'
																		   WHEN @param_src_value_type='LGD' THEN 'seg.element2' 
																		   ELSE 'NULL' END	+CHAR(39)
															,CHAR(39)+@param_stage			+CHAR(39)
															,CHAR(39)+@param_scenario_id	+CHAR(39)
															,CHAR(39)+@param_src_value_type	+CHAR(39))
				,@field_curve_name						= FORMATMESSAGE(N'CONCAT('+CHAR(39) +CASE @param_is_test WHEN 0 THEN '' ELSE 'TEST-' end	+CHAR(39)
																				+',ISNULL(CONVERT(NVARCHAR(255),%s),seg.deal_tfi_id)'
																				+',)'
															,CHAR(39)+CASE WHEN @param_src_value_type='PD' THEN 'seg.element1'
																		   WHEN @param_src_value_type='LGD' THEN 'seg.element2' 
																		   ELSE 'NULL' END	+CHAR(39))
				,@field_curve_desc						= N'src.acc_id'
				,@field_curve_type						= CHAR(39)+FORMATMESSAGE(N'%s Curve',@param_src_value_type)+CHAR(39) 
				,@field_source_system					= N'CONVERT(NVARCHAR(40),'+char(39)+@param_src_table_name+char(39)+')'
				,@field_curve_value_type				= CHAR(39)+@param_src_value_type+CHAR(39)			
				,@field_usage_type						= CHAR(39)+N'Actual'+CHAR(39)
				,@field_curve_reference					= CHAR(39)+N'Internal curve'+CHAR(39)
				,@field_forward_nbr_of_time_units		= N'1'
				,@field_forward_time_unit				= CHAR(39)+N'M'+CHAR(39)		
				,@field_currency						= N'seg.currency'									-- if deal: t_ecl_segmentation.currency
				,@field_day_count_convention			= CHAR(39)+N'ACT/ACT'+CHAR(39)						
				,@field_business_day_convention			= CHAR(39)+N'C'+CHAR(39)		
				,@field_calendar					    = CHAR(39)+N'standard'+CHAR(39)
				,@field_retry_count				        = N'0'
				,@field_start_validity_date		        = N'CONVERT(datetime,'+CHAR(39)+@cob_date+CHAR(39)+')'
				,@field_end_validity_date				= N'CONVERT(datetime,'+CHAR(39)+@max_date+CHAR(39)+')'
				,@field_value_perc1						= FORMATMESSAGE(N'CASE '																		+CHAR(13)+CHAR(10)+
																		+'WHEN ISNULL(seg.element3,''Marginal'') IN (''Marginal'',''Maginal'') THEN %s'	+CHAR(13)+CHAR(10)+
																		+'ELSE 0 END'
															,CASE WHEN @param_stage=3 and @param_src_value_type='PD' THEN '1' 
															 ELSE 'ISNULL(src.'+@param_src_field_name+',0)' END)						
				,@field_value_amount1					= FORMATMESSAGE(N'CASE '																		+CHAR(13)+CHAR(10)+
																		+'WHEN seg.element3=''Cumulative'' THEN %s'										+CHAR(13)+CHAR(10)+
																		+'ELSE 0 END'
															,CASE WHEN @param_stage=3 and @param_src_value_type='PD' THEN '1' 
															 ELSE 'ISNULL(src.'+@param_src_field_name+',0)' END)
				,@field_term_start_date1				= FORMATMESSAGE(N'EOMONTH(DATEADD(MONTH,%s,%s))'
															,CASE WHEN @param_stage=3 THEN '0'									-- T0
																  WHEN @is_nrt_smes=1 and @param_src_value_type='LGD' THEN '1'	-- T1 for NonRetail&SMEs LGD
															 ELSE 'src.forecast_month' END
															,N'CONVERT(datetime,'+CHAR(39)+@cob_date+CHAR(39)+')')
				,@field_char_cust_element1				= CHAR(39)+N''+CHAR(39)
				,@field_char_cust_element2				= CHAR(39)+@param_scenario_id+CHAR(39)									-- scenario_id
				,@field_char_cust_element3				= CASE WHEN @data_level='deal' THEN 'src.segment_name'
														  ELSE CHAR(39)+@param_model_name+CHAR(39) END							-- segment_name
				,@field_char_cust_element4				= N'CONVERT(datetime,'+CHAR(39)+@cob_date+CHAR(39)+')'					-- reporting_date
				,@field_char_cust_element5				= CHAR(39)+N''+CHAR(39)													-- free slot, not use
				,@field_num_cust_element1				= CASE WHEN @data_level in ('subsegment','segment') 
															THEN N'0.0' 
															ELSE N'CONVERT(numeric,seg.segment_pool_id)' END			 -- segment_id
				,@field_num_cust_element2				= CASE WHEN @data_level in ('subsegment','segment') 
																	or @is_nrt_smes=1 THEN N'0.0' 
															ELSE N'CONVERT(numeric,src.remaining_maturity)' END		-- if deal: remaining_maturity
				,@field_num_cust_element3				= CASE WHEN @data_level in ('subsegment','segment') 
																	or @is_nrt_smes=1 THEN N'0.0' 
															ELSE N'CONVERT(numeric,src.remaining_term)' END			-- if deal: remaining_term
				,@field_num_cust_element4				= CASE WHEN @data_level in ('subsegment','segment') 
															THEN N'0.0' 
															ELSE @param_stage END							-- scenario stage
				,@field_num_cust_element5				= N'0.0'											-- free slot, not use
				,@field_last_modified					= N'CURRENT_TIMESTAMP'			
				,@field_modified_by						= char(39)+@proc_name+char(39)
		;		

		select @nsql_insert = 'INSERT INTO t_etl_adj_data_crv_curve
			(adjustment_id, line_date, curve_code, curve_name, curve_desc, curve_type, source_system, curve_value_type, usage_type, curve_reference, forward_nbr_of_time_units, forward_time_unit, currency, day_count_convention, business_day_convention, calendar, retry_count, start_validity_date, end_validity_date, value_perc1, value_amount1, term_start_date1, char_cust_element1, char_cust_element2, char_cust_element3, char_cust_element4, char_cust_element5, num_cust_element1, num_cust_element2, num_cust_element3, num_cust_element4, num_cust_element5, last_modified, modified_by)'
		;
		select @nsql_table = N''																	+char(13)+char(10)+
			N'from '+@param_src_table_name+' src
				join t_ecl_segmentation seg on src.deal_id=seg.deal_tfi_id
				and seg.lot_id=@max_lot_id'
		;
		select @nsql_filter = N''																	+char(13)+char(10)+
			N'where src.deal_id is not null'														+char(13)+char(10)+
				N'and src.as_of_date=@cob_date'														+char(13)+char(10)+
				CASE WHEN @param_stage='3' and @is_nrt_smes<>1 THEN N'and src.forecast_month=1'
					 WHEN @param_stage='3' and @is_nrt_smes=1 THEN N'and src.stage=2'
					 WHEN @is_nrt_smes=1 THEN N'and src.stage='+@param_stage
				ELSE N'' END
		--nrt	-- temporay filter until FP not duplicate
		--		and deal_id not in (''FP001356500179'',''FP001359501124'',''FP001359501221'',''FP001359502065'',''FP001359501166'',''FP001359501182'',''FP001359501205'')';
		;
		select @nsql_select = char(13)+char(10)+case @param_is_test when 0 then 'select ' else 'select top 300 ' end
		;
		select @nsql_column = N''															+char(13)+char(10)+
				@field_adjustment_id   					+'	as adjustment_id,'				+char(13)+char(10)+
				@field_line_date       					+'	as line_date,'					+char(13)+char(10)+
				@field_curve_code      					+'	as curve_code,'					+char(13)+char(10)+
				@field_curve_name     					+'	as curve_name,'					+char(13)+char(10)+
				@field_curve_desc     					+'	as curve_desc,'					+char(13)+char(10)+
				@field_curve_type      					+'	as curve_type,'					+char(13)+char(10)+
				@field_source_system   					+'	as source_system,'				+char(13)+char(10)+
				@field_curve_value_type					+'	as curve_value_type,'			+char(13)+char(10)+
				@field_usage_type	 					+'	as usage_type,'					+char(13)+char(10)+
				@field_curve_reference					+'	as curve_reference,'			+char(13)+char(10)+
				@field_forward_nbr_of_time_units		+'	as forward_nbr_of_time_units,'	+char(13)+char(10)+
				@field_forward_time_unit				+'	as forward_time_unit,'			+char(13)+char(10)+
				@field_currency							+'	as currency,'					+char(13)+char(10)+
				@field_day_count_convention				+'	as day_count_convention,'		+char(13)+char(10)+
				@field_business_day_convention			+'	as business_day_convention,'	+char(13)+char(10)+
				@field_calendar							+'	as calendar,'					+char(13)+char(10)+
				@field_retry_count						+'	as retry_count,'				+char(13)+char(10)+
				@field_start_validity_date				+'	as start_validity_date,'		+char(13)+char(10)+
				@field_end_validity_date				+'	as end_validity_date,'			+char(13)+char(10)+
				@field_value_perc1						+'	as value_perc1,'				+char(13)+char(10)+
				@field_value_amount1					+'	as value_amount1,'				+char(13)+char(10)+
				@field_term_start_date1					+'	as term_start_date1,'			+char(13)+char(10)+
				@field_char_cust_element1				+'	as char_cust_element1,'			+char(13)+char(10)+
				@field_char_cust_element2				+'	as char_cust_element2,'			+char(13)+char(10)+
				@field_char_cust_element3				+'	as char_cust_element3,'			+char(13)+char(10)+
				@field_char_cust_element4				+'	as char_cust_element4,'			+char(13)+char(10)+
				@field_char_cust_element5				+'	as char_cust_element5,'			+char(13)+char(10)+
				@field_num_cust_element1				+'	as num_cust_element1,'			+char(13)+char(10)+
				@field_num_cust_element2				+'	as num_cust_element2,'			+char(13)+char(10)+
				@field_num_cust_element3				+'	as num_cust_element3,'			+char(13)+char(10)+
				@field_num_cust_element4				+'	as num_cust_element4,'			+char(13)+char(10)+
				@field_num_cust_element5				+'	as num_cust_element5,'			+char(13)+char(10)+
				@field_last_modified					+'	as last_modified,'				+char(13)+char(10)+
				@field_modified_by						+'	as modified_by';
		
		-- param definition
		set @paramDef = N'@adjustment_id			d_id
						,@cob_date				datetime
						,@max_date				datetime
						,@max_lot_id			d_id';
		-- end prepare dynamic t-sql
		
		-- final t-sql
		set @nsql = @nsql_insert + @nsql_select + @nsql_column + @nsql_table + @nsql_filter;

		-- check final t-sql length
		IF len(@nsql) > 4000 or @nsql is null
		BEGIN
			set @ale_message = N'SQL string is over than 4000 character or null.';
			set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, len(@nsql) as len_nsql
				, @nsql_insert as nsql_insert, @nsql_select as nsql_select
				, @nsql_column as nsql_column, @nsql_table as nsql_table
				, @nsql_filter as nsql_filter FOR XML PATH);

			EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
						@event_type                = N'E',
						@context                   = @ale_context,
						@message                   = @ale_message,
						@event_type_detail         = N'E',
						--@correlation_guid          = @ale_correlation_guid OUTPUT,
						--@timestamp_utc             = N'',
						--@event_log_guid            = @ale_event_log_guid OUTPUT,
						@use_synchronous_logging   = 0,
						@print_ind                 = @param_is_test,
						@print_level               = 0,
						@calling_proc_name         = @proc_name;

			PRINT '@nsql_insert:'	+ISNULL(@nsql_insert,'NULL')
			PRINT '@nsql_select:'	+ISNULL(@nsql_select,'NULL')
			PRINT '@nsql_column:'	+ISNULL(@nsql_column,'NULL')
			PRINT '@nsql_table:'	+ISNULL(@nsql_table,'NULL')
			PRINT '@nsql_filter:'	+ISNULL(@nsql_filter,'NULL')

			set @error_msg = @ale_message
			RAISERROR (@error_msg, 16, 1, @proc_name)
		END

		
		--add logging here
		set @ale_message = N'Inserting '+ @src_table_name1+','+@src_table_name2+','+@src_table_name3 + ' to ' + @table_type_name+' (adjustment_id:'+CONVERT(NVARCHAR,@adjustment_id)+')';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql, @paramDef as paramDef FOR XML PATH);

		EXEC p_ale_log @event_name            = N'CREATE_ADJUSTMENT', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT 
					@event_type                = N'I',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'I',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = @param_is_test,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;

		RAISERROR(@ale_message,10,1,@proc_name);

		-- insert data with sp_executesql
		exec sp_executesql @nsql ,@paramDef ,@adjustment_id			=@adjustment_id	
											,@cob_date				=@cob_date				
											,@max_date				=@max_date				
											,@max_lot_id			=@max_lot_id
		;

		--add logging here
		set @ale_message = N'Running HMS.';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @adjustment_id as adjustment_id FOR XML PATH);

		EXEC p_ale_log @event_name            = N'START_ADJUSTMENT', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
					@event_type                = N'I',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'I',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = @param_is_test,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;

		RAISERROR(@ale_message,10,1,@proc_name);

		-- run hms
		exec @status = [dbo].[p_etl_adj_xtr_crv_curve]
                            @portfolio_date = @cob_date,
                            @del_data_after_loading = 0,
                            @div_rate_by_100 = 0,
                            @adjustment_id = @adjustment_id,
                            @debug = @param_is_test
		;

		declare @hms_result nvarchar(max);
		select @hms_result = FORMATMESSAGE('Inserted %i curve to t_crv_curve and %i curve point to t_crv_curve_point_value' ,count(distinct crv.curve_code), count(p.curve_point_value_id))
		from t_crv_curve crv join
			t_crv_curve_point_value p on crv.curve_id=p.curve_id
		where exists (
			select etl.curve_code
			from t_etl_adj_data_crv_curve etl
			where etl.adjustment_id=@adjustment_id
				and etl.curve_code=crv.curve_code
		);
		RAISERROR(@hms_result,10,1,@proc_name);

		IF @local_trancount=1
			COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		IF @local_trancount=1
			ROLLBACK TRANSACTION;

		--add logging here
		set @ale_message = FORMATMESSAGE('%s error at line %i'
								+CHAR(13)+CHAR(10)+'Inner Error: %s (error number %i).'
								,@proc_name
								,CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line)
								,ERROR_MESSAGE()
								,CONVERT(NVARCHAR, ERROR_NUMBER())
							);
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = 1,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;

		SELECT	 @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
	END CATCH

	--add logging here
	set @ale_message = N'End: ' + @proc_name;
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_FINISH', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				--@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				--@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

-- Return succes
RETURN (0) 

GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pop_pdlgd_main_deal' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pop_pdlgd_main_deal] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pop_pdlgd_main_deal] has NOT been altered due to errors!'
END
GO
