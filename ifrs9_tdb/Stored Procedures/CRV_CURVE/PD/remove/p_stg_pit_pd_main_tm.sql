
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_pd_main_tm' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pit_pd_main_tm] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pit_pd_main_tm] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pit_pd_main_tm]...'
GO
ALTER PROCEDURE [dbo].[p_stg_pit_pd_main_tm] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime,
	@param_src_table_name	nvarchar(256),
	@param_scenario_id		d_name,
	@param_scenario_name	d_name,
	@param_stage			d_name,
	@param_src_field_name	d_name,
	@param_model_name		d_name,
	@param_is_test			bit				= 0

AS
-- ********************************************************************************
-- * Author:		                                *
-- * Description:	This stored procedure will populate data for				  *
-- *				t_etl_adj_data_crv_curve and run HMS process to live table	  *
-- *				t_crv_curve and t_crv_curve_point_value.					  *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_src_table_name: point in time source table name      *
-- *				- @param_scenario_id: scenario_id/list of scenario_id		  *
-- *				- @param_scenario_name: scenario_name						  *
-- *				- @param_stage: stage name for mapping table/column			  *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

-- Declare local variables
	--declare @reporting_date		datetime					= '2018-03-31';
	--declare @src_table_name		d_name						= N't_stg_hl_pit_pd_stage2_tbank';
	--declare @scenario_id			d_id						= 54;

	declare @adjustment_id			d_id;
	declare @adj_type_id_crv_curve	d_id;

	-- list of constant variable
	declare @comment				d_long_description			= N'PD|'+@param_src_table_name+N'|'+@param_scenario_name --N'TBANK';
	declare @modified_by			d_user						= @proc_name; -- change to proc name later, OBJECT_NAME( @@PROCID )|dbo.fn_user()
	declare @lot_fn_code			d_std_record_function		= N'ECLSEG';
	declare @table_type_name		d_name						= N't_etl_adj_data_crv_curve';

	declare @crv_type				d_curve_type				= 8; -- pd=8, lgd=9
	declare @crv_type_name			d_name						= N'PD Curve'; -- pd=8, lgd=9
	declare @crv_val_type			d_value_type				= N'PD'; -- pd=8, lgd=9
	declare @src_sys				d_source_system				= @param_src_table_name+N'|'+@param_scenario_id --N'TBANK';
	declare @crv_begin_date			datetime					= @param_reporting_date;
	declare @crv_end_date			datetime					= '9999-12-31';
	declare @u_type					d_usage_type				= 1; -- 1=Actual, 2=Stress
	declare @u_type_name			d_name						= N'Actual'; -- 1=Actual, 2=Stress
	declare @crv_ref				d_reference_code			= N'INT'; -- ADM1=Primary Administered Rate, ADM2=Secondary Administered Rate, CB=Central Bank curve, GOV=Government curve, INT=Internal curve, MKT=Market curve, REG=Defined by Regulator
	declare @crv_ref_name			d_name						= N'Internal curve'; -- ADM1=Primary Administered Rate, ADM2=Secondary Administered Rate, CB=Central Bank curve, GOV=Government curve, INT=Internal curve, MKT=Market curve, REG=Defined by Regulator
	declare @fwd_nbr				d_nbr_of_units				= 1;
	declare @fwd_time_unit			d_time_unit					= N'M';      
	declare @ccy					d_currency					= N'THB';      
	declare @day_count_con			d_day_count_convention		= N'ACT/ACT';      
	declare @busi_day_con			d_business_day_convention	= N'C';
	declare @cal					d_calendar					= N'standard';
	declare @value_prec1			d_amount					= 0; -- marginal value
	declare @value_amount1			d_amount					= 0; -- cummulative value
	declare @char_cust_element5		d_char_cust_element5		= N''; --not use forecast_month because hms can not distinct curve when this field has multiple value
	declare @num_cust_element5		d_num_cust_element5			= 0;  -- process flag
	declare	@marginal				d_name;
	declare	@cumulative				d_name;
	declare	@adj_forecast			d_id						= 0;
	declare	@segment_id				d_name;
	declare	@stage					d_name						= '';
	

-- Start actual procedure flow
	IF @param_reporting_date is null 
		or @param_src_table_name is null
		or @param_scenario_id is null
		or @param_scenario_name is null
	BEGIN
		select @error_msg = isnull(@param_reporting_date,',@param_reporting_date')
							+ isnull(@param_src_table_name,',@param_src_table_name')
							+ isnull(@param_scenario_id,',@param_scenario_id')
							+ isnull(@param_scenario_name,',@param_scenario_name')
							+ isnull(@param_stage,',@param_stage');
		set @error_msg = N'This procedure must send parameter: ' + substring(@error_msg,2,len(@error_msg))
		RAISERROR (@error_msg, 16, 1, @proc_name)
		RETURN 1
	END

	IF @param_is_test = 1
	BEGIN
		PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Start: ' + @proc_name
	END
	
	declare @nsql nvarchar(max);
	declare @nsql_top100 nvarchar(10);
	declare	@nsql_bhl	 nvarchar(50) = '';
	declare	@nsql_scenario			d_name;
	
	BEGIN TRY -- prepare dynamic t-sql
		select @nsql_top100 = case @param_is_test when 0 then '' else 'top 100 ' end;

		if @param_model_name = 'CC'
			set @segment_id = '08'
		else if @param_model_name = 'FP'
			set @segment_id = '09'
		else if @param_model_name = 'SPL/MRTA'
			set @segment_id = '03,05'
		else if @param_model_name = 'UPL/T-LOAN'
			set @segment_id = '01,02'
		else if @param_model_name = 'HP_Juristics'
			set @segment_id = '94'

		if @param_scenario_id in ('5','56')
			set @nsql_scenario = ' and scenario_id = ''SCN001'''
		else if @param_scenario_id in ('3','54')
			set @nsql_scenario = ' and scenario_id = ''SCN002'''
		else if @param_scenario_id in ('4','55')
			set @nsql_scenario = ' and scenario_id = ''SCN003'''
		else 
			set @nsql_scenario = ''

		if @param_stage = '2' 
			begin 
				set	@stage = '2'
				if @param_model_name in ('CC','FP')
					begin
						select	@nsql_bhl = concat(' and seq_month = ',cast(bhl_m as nvarchar(2))) , @adj_forecast = (bhl_m - 1)
						from	t_store_bhl_transition_matrix_tbank				
						where	model_name = @param_model_name
						and		as_of_date = eomonth(dateadd(month,-1,@param_reporting_date))
						
						set @marginal	= '0'
						set @cumulative	= 'pd.cumulative_m'
					end
				else
					begin
						set @adj_forecast = 0
						set @marginal	= 'pd.marginal_m'
						set @cumulative	= '0'
					end
			end
		else if @param_stage = '1' 
			begin 
				set @stage = '1'
				if @param_model_name in ('CC','FP')
					begin
						set @nsql_bhl = ' and seq_month = 12'
						set @adj_forecast = 11
						set @marginal	= '0'
						set @cumulative	= 'pd.cumulative_a'
					end
				else
					begin
						set @adj_forecast = 0
						set @marginal	= 'pd.marginal_a'
						set @cumulative	= '0'
					end
			end
		else if @param_stage = '3' 
			begin 
				set @stage = '3'
				if @param_model_name in ('CC','FP')
					begin
						set @nsql_bhl = ' and seq_month = 12'
						set @adj_forecast = 11
						set @marginal	= '0'
						set @cumulative	= '1'
					end
				else
					begin
						set @nsql_bhl = ' and seq_month = 1'
						set @adj_forecast = 0
						set @marginal	= '1'
						set @cumulative	= '0'
					end
			end

		select @nsql = 'INSERT INTO t_etl_adj_data_crv_curve
			(adjustment_id, line_date, curve_code, curve_name, curve_desc, curve_type, source_system, curve_value_type, usage_type, curve_reference, forward_nbr_of_time_units, forward_time_unit, currency, day_count_convention, business_day_convention, calendar, retry_count, start_validity_date, end_validity_date, value_perc1, value_amount1, term_start_date1, char_cust_element1, char_cust_element2, char_cust_element3, char_cust_element4, char_cust_element5, num_cust_element1, num_cust_element2, num_cust_element3, num_cust_element4, num_cust_element5, last_modified, modified_by)'
		
		select @nsql = @nsql + 
		N' select '+@nsql_top100+' @adjustment_id as adjustment_id
			,CURRENT_TIMESTAMP					as line_date
			,concat(case when pd.model_name=''HP_Juristics'' then ''HP_JU'' else pd.model_name end,''|'',@stage,''|'',@scenario_id,''|'',@crv_val_type) as curve_code
			,pd.model_name						as curve_name
			,pd.model_name						as curve_desc
			,@crv_type_name						as curve_type
			,@src_sys							as source_system
			,@crv_val_type						as curve_value_type
			,@u_type_name						as usage_type
			,@crv_ref_name						as curve_reference
			,@fwd_nbr							as forward_nbr_of_time_units
			,@fwd_time_unit						as forward_time_unit
			,@ccy								as currency
			,@day_count_con						as day_count_convention
			,@busi_day_con						as business_day_convention
			,@cal								as calendar
			,0									as retry_count
			,@crv_begin_date					as start_validity_date
			,@crv_end_date						as end_validity_date
			,'+@marginal+'						as value_prec1
			,'+@cumulative+'					as value_amount1
			,dateadd(month,case when '+@stage+'=3 then 0 else pd.seq_month-@adj_forecast+1 end ,@reporting_date) as term_start_date1
			,''''								as char_cust_element1
			,@scenario_id						as char_cust_element2
			,pd.model_name						as char_cust_element3
			,@reporting_date					as char_cust_element4
			,@char_cust_element5				as char_cust_element5
			,0.00						as num_cust_element1
			,0.00								as num_cust_element2
			,0.00								as num_cust_element3
			,@stage								as num_cust_element4
			,@num_cust_element5					as num_cust_element5
			,CURRENT_TIMESTAMP					as last_modified
			,@modified_by						as modified_by
		from '+@param_src_table_name+' as pd
		where	pd.as_of_date = eomonth(dateadd(month,-1,@reporting_date)) and model_name = @param_model_name
		'+ @nsql_scenario + @nsql_bhl +';' 
		;

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'nsql: ' + @nsql
		END

		IF len(@nsql) > 4000
		BEGIN
			set @error_msg = N'SQL string is over than 4000 character.'
			RAISERROR (@error_msg, 16, 1, @proc_name)
			RETURN 1
		END

		declare @paramDef nvarchar(max);
		set @paramDef = N'@reporting_date datetime
		,@adjustment_id			d_id
		,@scenario_id			d_name
		,@modified_by			d_user
		,@lot_fn_code			d_std_record_function
		,@crv_type				d_curve_type
		,@crv_type_name			d_name
		,@crv_val_type			d_value_type
		,@src_sys				d_source_system
		,@crv_begin_date		datetime
		,@crv_end_date			datetime
		,@u_type_name			d_name
		,@crv_ref_name			d_name
		,@fwd_nbr				d_nbr_of_units
		,@fwd_time_unit			d_time_unit
		,@ccy					d_currency
		,@day_count_con			d_day_count_convention
		,@busi_day_con			d_business_day_convention
		,@cal					d_calendar
		,@value_amount1			d_amount
		,@char_cust_element5	d_char_cust_element5
		,@num_cust_element5		d_num_cust_element5
		,@adj_forecast			d_id
		,@segment_id			d_name
		,@stage					d_name
		,@param_model_name		d_name
		';-- end prepare dynamic t-sql

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Curve|SourceTable|Scenario: ' + @comment
		END

		--RAISERROR(@nsql, 16, 1, @proc_name) -- force stop program for view and testing
	END TRY
	BEGIN CATCH
		--SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
		--SELECT @error_msg =  char(13)+char(10)+char(9)+char(9)+ '@param_reporting_date:' + CONVERT(NVARCHAR,@param_reporting_date)
		--					+char(13)+char(10)+char(9)+char(9)+ '@param_src_table_name:' + @param_src_table_name
		--					+char(13)+char(10)+char(9)+char(9)+ '@param_scenario_id:' + CONVERT(NVARCHAR,@param_scenario_id)
		--					+char(13)+char(10)+char(9)+char(9)+ '@param_scenario_name:' + CONVERT(NVARCHAR,@param_scenario_name)
		--					+char(13)+char(10)+char(9)+char(9)+ '@param_stage:' + CONVERT(NVARCHAR,@param_stage)
		--					+char(13)+char(10)+char(9)+char(9)+ '@param_is_test:' + CONVERT(NVARCHAR,@param_is_test)
		--					+char(13)+char(10)+char(9)+char(9)+ @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'
		--		,@error_severity = ERROR_SEVERITY()
		--		,@error_state	 = ERROR_STATE();
		--RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
		--RETURN 1

		-- add logging here
		--EXEC p_ale_log_event @event_name    = 'new event name',
  --                   @message       = @message,
  --                   @context_value = @proc_name,
  --                   @event_log_id  = @event_log_id OUTPUT;

		THROW;
	END CATCH

	BEGIN TRANSACTION;
	BEGIN TRY -- start execute main process		

		-- get table type id
		SELECT @adj_type_id_crv_curve   = (SELECT typ.type_id FROM t_adj_type typ JOIN t_adj_category cat ON cat.category_id = typ.category_id WHERE typ.type_name = @table_type_name);
		-- prepare adjustment before load to stg
		EXEC @status = [dbo].[p_adj_save]
								@adjustment_id = @adjustment_id OUTPUT,
								@type_id = @adj_type_id_crv_curve,
								@reporting_date = @param_reporting_date,
								@comment = @comment
		;

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'@adjustment_id: ' + convert(varchar,@adjustment_id)
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Inserting ' + @param_src_table_name + ' to ' + @table_type_name
		END

		-- insert data with sp_executesql
		exec sp_executesql @nsql ,@paramDef ,@reporting_date			=@param_reporting_date
											,@adjustment_id				=@adjustment_id
											,@scenario_id				=@param_scenario_id
											,@modified_by				=@modified_by
											,@lot_fn_code				=@lot_fn_code
											,@crv_type					=@crv_type
											,@crv_type_name				=@crv_type_name
											,@crv_val_type				=@crv_val_type
											,@src_sys					=@src_sys
											,@crv_begin_date			=@crv_begin_date
											,@crv_end_date				=@crv_end_date
											,@u_type_name				=@u_type_name
											,@crv_ref_name				=@crv_ref_name
											,@fwd_nbr					=@fwd_nbr
											,@fwd_time_unit				=@fwd_time_unit
											,@ccy						=@ccy
											,@day_count_con				=@day_count_con
											,@busi_day_con				=@busi_day_con
											,@cal						=@cal
											,@value_amount1				=@value_amount1
											,@char_cust_element5		=@char_cust_element5
											,@num_cust_element5			=@num_cust_element5
											,@adj_forecast				=@adj_forecast
											,@segment_id				=@segment_id
											,@stage						=@stage
											,@param_model_name			=@param_model_name
		;

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Running HMS p_etl_adj_xtr_crv_curve'
		END

		-- run hms
		EXEC @status = [dbo].[p_etl_adj_xtr_crv_curve]
                            @portfolio_date = @param_reporting_date,
                            @del_data_after_loading = 0,
                            @div_rate_by_100 = 0,
                            @adjustment_id = @adjustment_id,
                            @debug = @param_is_test

		IF @param_is_test = 1
		BEGIN
			declare @hms_result nvarchar(max);
			select @hms_result = FORMATMESSAGE('Inserted %i curve to t_crv_curve and %i curve point to t_crv_curve_point_value' ,count(distinct crv.curve_code), count(p.curve_point_value_id))
			from t_crv_curve crv join
				t_crv_curve_point_value p on crv.curve_id=p.curve_id
			where exists (
				select etl.curve_code
				from t_etl_adj_data_crv_curve etl
				where etl.adjustment_id=@adjustment_id
					and etl.curve_code=crv.curve_code
			);

			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + @hms_result;
		END

		SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
		IF @error <> 0 OR @status <> 0
		  BEGIN
			RAISERROR('HMS encountered an error.', 16, 1, @proc_name, @error)
			RETURN 
		  END	

		COMMIT;
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK;

		--SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
		SELECT @error_msg =  char(13)+char(10)+char(9)+char(9)+ '@param_reporting_date:' + CONVERT(NVARCHAR,@param_reporting_date)
							+char(13)+char(10)+char(9)+char(9)+ '@param_src_table_name:' + @param_src_table_name
							+char(13)+char(10)+char(9)+char(9)+ '@param_scenario_id:' + CONVERT(NVARCHAR,@param_scenario_id)
							+char(13)+char(10)+char(9)+char(9)+ '@param_scenario_name:' + CONVERT(NVARCHAR,@param_scenario_name)
							+char(13)+char(10)+char(9)+char(9)+ '@param_stage:' + CONVERT(NVARCHAR,@param_stage)
							+char(13)+char(10)+char(9)+char(9)+ '@param_is_test:' + CONVERT(NVARCHAR,@param_is_test)
							+char(13)+char(10)+char(9)+char(9)+ @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'+char(13)+char(10)
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)

		-- add logging here
		--EXEC p_ale_log_event @event_name    = 'new event name',
  --                   @message       = @message,
  --                   @context_value = @proc_name,
  --                   @event_log_id  = @event_log_id OUTPUT;

		--THROW;
	END CATCH
-- Return succes
RETURN (0) 



GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_pd_main_tm' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_pd_main_tm] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_pd_main_tm] has NOT been altered due to errors!'
END
GO
