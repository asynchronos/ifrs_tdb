
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_pd_hpju' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pit_pd_hpju] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pit_pd_hpju] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pit_pd_hpju]...'
GO
ALTER PROCEDURE [dbo].[p_stg_pit_pd_hpju] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime,
	@param_is_test			bit			= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	Stored procedure for population t_etl_adj_data_crv_curve	  *
-- *				by looping each source table and each scenario.			      *
-- *				This procedure will chain to child procedure 			      *
-- *				for populate adj_data and run HMS process.	  			      *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

-- Declare local variables	
	declare @table_name nvarchar(256);
	declare @scenario_id d_id;
	declare @scenario_name d_name;
	declare @scenario_id_str d_name;
	declare @stage d_name;
	declare @src_field_name d_name;
	declare @model_name d_name;

-- Start actual procedure flow
	IF @param_reporting_date is null 
	BEGIN 
		select @error_msg = isnull(@param_reporting_date,',@param_reporting_date');
		set @error_msg = N'This procedure must send parameter: ' + substring(@error_msg,2,len(@error_msg))
		RAISERROR (@error_msg, 16, 1, @proc_name)
		RETURN 1
	END

	IF @param_is_test = 1
	BEGIN
		PRINT CHAR(13)+CHAR(10)+CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Start: ' + @proc_name
	END
		
	BEGIN TRY

		declare pit_pd_cc_cursor cursor local static for
		-- select target source table name and scenario mapping
		select t.TABLE_NAME, stage, convert(varchar,se.scenario_id) as scenario_id_str, se.scenario_name, src_field_name, model_name
		from INFORMATION_SCHEMA.TABLES t
			join ( -- create configuration table with inline sql, may move to physical table in the future
				select s.scenario_id, s.scenario_name
					,'HP_Juristics' as model_name
					,case
						when s.scenario_id in (3,4,5,51)	then	'1'
						when s.scenario_id in (54,55,56,57)	then	'2'
						when s.scenario_id in (110)			then	'3'
						else 'unknown' end as stage
					,N'PD' as flow_type
					,case
						when s.scenario_id in (3,4,5,51)	then	'marginal_a'
						when s.scenario_id in (54,55,56,57)	then	'marginal_m'
						when s.scenario_id in (110)			then	'marginal_m=1'
						else 'unknown' end as src_field_name
				from t_scenario s
				where s.scenario_id in (3,4,5,51,54,55,56,57,110)
			) se on 1=1 
				and (se.scenario_id in (3,4,5,54,55,56) and t.TABLE_NAME='t_result_forward_looking_transition_matrix_tbank')
				or (se.scenario_id in (51,57,110) and t.TABLE_NAME='t_result_pd_weighting_transition_matrix_tbank')
		where t.TABLE_NAME in ('t_result_forward_looking_transition_matrix_tbank','t_result_pd_weighting_transition_matrix_tbank') -- change product table here, may change to dynamic sql for easy config and mantinance in the future
		
		open pit_pd_cc_cursor
		fetch next from pit_pd_cc_cursor into @table_name, @stage, @scenario_id_str, @scenario_name, @src_field_name, @model_name

		IF @@FETCH_STATUS <> 0
		BEGIN
			IF @param_is_test = 1
			BEGIN
				PRINT CHAR(13)+CHAR(10)+CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + FORMATMESSAGE('Source table %s do not exists','t_result_forward_looking_transition_matrix_tbank,t_result_pd_weighting_transition_matrix_tbank')
			END
			-- add logging here
			--EXEC p_ale_log_event @event_name    = 'new event name',
  --                   @message       = @message,
  --                   @context_value = @proc_name,
  --                   @event_log_id  = @event_log_id OUTPUT;
		END
		
		While @@FETCH_STATUS = 0
		BEGIN

			BEGIN TRY -- handle error for continue loop to next source
				-- execute stored procedure for populate data and load hms of each table with each scenario
				EXEC p_stg_pit_pd_main_tm @param_reporting_date   = @param_reporting_date,
					   @param_src_table_name   = @table_name,
					   @param_scenario_id      = @scenario_id_str,
					   @param_scenario_name    = @scenario_name,
					   @param_stage			   = @stage,
					   @param_src_field_name   = @src_field_name,
					   @param_model_name	   = @model_name,
					   @param_is_test          = @param_is_test
			END TRY
			BEGIN CATCH -- print error and continue, user severity 10 or lower
				SELECT @error_msg =  char(13)+char(10)+char(9)+ '@param_reporting_date:' + convert(varchar,@param_reporting_date)
									+char(13)+char(10)+char(9)+ @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'+char(13)+char(10)
						,@error_severity = 10
						,@error_state	 = 1;
				RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
				--PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -E- ' + @error_msg
			END CATCH
		
			fetch next from pit_pd_cc_cursor into @table_name, @stage, @scenario_id_str, @scenario_name, @src_field_name, @model_name
		END
		
		close pit_pd_cc_cursor
		deallocate pit_pd_cc_cursor
	
	END TRY
	BEGIN CATCH -- for catch random error
		--SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
		
		SELECT @error_msg =  char(13)+char(10)+char(9)+ '@param_reporting_date:' + convert(varchar,@param_reporting_date)
							+char(13)+char(10)+char(9)+ @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
		-- add logging here
		--EXEC p_ale_log_event @event_name    = 'new event name',
--                   @message       = @message,
--                   @context_value = @proc_name,
--                   @event_log_id  = @event_log_id OUTPUT;
		RETURN 1
	END CATCH
-- Return succes
RETURN (0) 


GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_pd_hpju' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_pd_hpju] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_pd_hpju] has NOT been altered due to errors!'
END
GO
