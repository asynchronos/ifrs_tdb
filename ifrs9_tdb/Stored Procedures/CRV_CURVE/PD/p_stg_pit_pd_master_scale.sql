
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_pd_master_scale' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pit_pd_master_scale] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pit_pd_master_scale] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pit_pd_master_scale]...'
GO

ALTER PROCEDURE [dbo].[p_stg_pit_pd_master_scale] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime,
	@param_is_test			bit			= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	Stored procedure for population t_etl_adj_data_crv_curve	  *
-- *				by looping each source table and each scenario.			      *
-- *				This procedure will chain to child procedure 			      *
-- *				for populate adj_data and run HMS process.	  			      *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line 

	-- variables for ale
	declare @ale_message						d_ale_message;
	declare @ale_context						xml;
	declare @ale_correlation_guid				UNIQUEIDENTIFIER;
	declare @ale_event_log_guid					UNIQUEIDENTIFIER;

-- Declare local variables
	declare @const_max_ms						int					=	22;	-- max master scale for looping
	declare @idx_current_ms						int					=	1;	-- master scale index loop counter
	declare @table_type_name					d_name				=	N't_etl_adj_data_crv_curve';
	declare @param_src_table_name				d_name				=	N't_nonretail_portfolio_marginal_pit_pd_tbank';

	-- variables for t_ecl_process_log_tbank
	declare @log_pid							int;
	declare @log_process_type					varchar(20)			=	'Nonretail';
	declare @log_model_name						varchar(100)		=	'Pop Nonretail Master Scale PD';
	declare @log_result_table					varchar(50)			=	@table_type_name;

	-- variable for dynamic sql
	declare @nsql								nvarchar(max);
	declare @nsql_top100						nvarchar(10);
	declare @nsql_filter						nvarchar(255);
	declare @nsql_fields						nvarchar(255);
	declare @nsql_join_segment					nvarchar(255);

	-- variable for insert t_crv_curve
	declare @field_adjustment_id				nvarchar(255);
	declare @field_line_date					nvarchar(255);
	declare @field_curve_code					nvarchar(255);
	declare @field_curve_name					nvarchar(255);
	declare @field_curve_desc					nvarchar(255);
	declare @field_curve_type					nvarchar(255);
	declare @field_source_system				nvarchar(255);
	declare @field_curve_value_type				nvarchar(255);
	declare @field_usage_type					nvarchar(255);
	declare @field_curve_reference				nvarchar(255);
	declare @field_forward_nbr_of_time_units	nvarchar(255);
	declare @field_forward_time_unit			nvarchar(255);
	declare @field_currency						nvarchar(255);
	declare @field_day_count_convention			nvarchar(255);
	declare @field_business_day_convention		nvarchar(255);
	declare @field_calendar						nvarchar(255);
	declare @field_retry_count					nvarchar(255);
	declare @field_start_validity_date			nvarchar(255);
	declare @field_end_validity_date			nvarchar(255);
	declare @field_value_perc1					nvarchar(255);
	declare @field_value_amount1				nvarchar(255);
	declare @field_term_start_date1				nvarchar(255);
	declare @field_char_cust_element1			nvarchar(255);
	declare @field_char_cust_element2			nvarchar(255);
	declare @field_char_cust_element3			nvarchar(255);
	declare @field_char_cust_element4			nvarchar(255);
	declare @field_char_cust_element5			nvarchar(255);
	declare @field_num_cust_element1			nvarchar(255);
	declare @field_num_cust_element2			nvarchar(255);
	declare @field_num_cust_element3			nvarchar(255);
	declare @field_num_cust_element4			nvarchar(255);
	declare @field_num_cust_element5			nvarchar(255);
	declare @field_last_modified				nvarchar(255);
	declare @field_modified_by					nvarchar(255);

-- Start actual procedure flow
	IF @param_reporting_date is null 
	BEGIN 
		select @error_msg = isnull(@param_reporting_date,',@param_reporting_date');
		set @error_msg = N'This procedure must send parameter: ' + substring(@error_msg,2,len(@error_msg))
		RAISERROR (@error_msg, 16, 1, @proc_name)
		RETURN 1
	END

	--add logging here
	set @ale_message = N'Start: ' + @proc_name
						+ N'(@param_reporting_date=' + CONVERT(varchar,@param_reporting_date)
						+ char(13)+char(10)+char(9)+N',@param_is_test=' + CONVERT(varchar,@param_is_test)
						+ N')';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name             = N'PROC_START', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

	--/*Start Process Log*/ -- change to proc in the future
	--SET @log_pid = (select isnull(max(process_id),0)+1 
	--				from t_ecl_process_log_tbank
	--				where cob_date = @cob_date);

	--insert into t_ecl_process_log_tbank (cob_date,process_id,process_type,process_name,model_name,result_table,start_time,process_status)
	--select  @cob_date, @log_pid, @log_process_type, @proc_name, @log_model_name, @log_result_table, getdate(),'Processing';
	--/*End Process Log*/

	BEGIN TRY

		-- clear adjustment_id 0 before run
		delete t_etl_adj_data_crv_curve where adjustment_id=0;

		While @idx_current_ms <= @const_max_ms -- looping from master scale 1 to xx
		BEGIN
			-- text must have single-quote/char(39) between text
			-- function, field, numeric do not need single-quote
			select	 @field_adjustment_id				= N'0'							-- wait for update with p_adj_save before run HMS
					,@field_line_date					= N'CURRENT_TIMESTAMP'
					,@field_curve_code					= N'CONCAT(''NRT'',''-'',''MS'','+convert(nvarchar,@idx_current_ms)+',''|'',se.stage,''|'',se.scenario_id,''|'',''PD'')'
					,@field_curve_name					= N'CONCAT(''MS'','+convert(nvarchar,@idx_current_ms)+')'
					,@field_curve_desc					= N'CONCAT(''MS'','+convert(nvarchar,@idx_current_ms)+')'
					,@field_curve_type					= char(39)+N'PD Curve'+char(39)	-- use curve_type_name from t_crv_curve_type
					,@field_source_system				= N'CONVERT(NVARCHAR(40),'+char(39)+@param_src_table_name+char(39)+')'
					,@field_curve_value_type			= char(39)+N'PD'+char(39)
					,@field_usage_type					= char(39)+N'Actual'+char(39)
					,@field_curve_reference				= char(39)+N'Internal curve'+char(39)
					,@field_forward_nbr_of_time_units	= N'1'
					,@field_forward_time_unit			= char(39)+N'M'+char(39)
					,@field_currency					= char(39)+N'THB'+char(39)		-- if deal level can use currency from t_ecl_segmentation
					,@field_day_count_convention		= char(39)+N'ACT/ACT'+char(39)
					,@field_business_day_convention		= char(39)+N'C'+char(39)
					,@field_calendar					= char(39)+N'standard'+char(39)
					,@field_retry_count					= N'0'
					,@field_start_validity_date			= N'@reporting_date'
					,@field_end_validity_date			= char(39)+N'9999-12-31'+char(39)
					,@field_value_perc1					= N'case '+char(13)+char(10)+
															+'when se.scenario_id in (51,57)			then	ms'+convert(nvarchar,@idx_current_ms)+'_ttc_pd'+char(13)+char(10)+
															+'when se.scenario_id in (3,4,5,54,55,56)	then	ms'+convert(nvarchar,@idx_current_ms)+'_pit_pd'+char(13)+char(10)+
															+'when se.scenario_id in (110)			then	1.0'+char(13)+char(10)+
															+'else 0.0 end'
					,@field_value_amount1				= N'0.0'
					,@field_term_start_date1			= N'eomonth(dateadd(month,forecast_month,as_of_date))'
					,@field_char_cust_element1			= char(39)+N''+char(39)
					,@field_char_cust_element2			= N'se.scenario_id'
					,@field_char_cust_element3			= char(39)+N'Master Scale'+char(39)
					,@field_char_cust_element4			= N'@reporting_date'
					,@field_char_cust_element5			= char(39)+N''+char(39)
					,@field_num_cust_element1			= N'0.0'
					,@field_num_cust_element2			= N'0.0'
					,@field_num_cust_element3			= N'0.0'
					,@field_num_cust_element4			= N'0.0'
					,@field_num_cust_element5			= N'0.0'
					,@field_last_modified				= N'CURRENT_TIMESTAMP'
					,@field_modified_by					= N'@proc_name'
			;

			select @nsql_top100 = case @param_is_test when 0 then '' else 'top 100 ' end
			;
		
			select @nsql = 'INSERT INTO t_etl_adj_data_crv_curve
				(adjustment_id, line_date, curve_code, curve_name, curve_desc, curve_type, source_system, curve_value_type, usage_type, curve_reference, forward_nbr_of_time_units, forward_time_unit, currency, day_count_convention, business_day_convention, calendar, retry_count, start_validity_date, end_validity_date, value_perc1, value_amount1, term_start_date1, char_cust_element1, char_cust_element2, char_cust_element3, char_cust_element4, char_cust_element5, num_cust_element1, num_cust_element2, num_cust_element3, num_cust_element4, num_cust_element5, last_modified, modified_by)'
			;

			select @nsql = @nsql + char(13)+char(10)+
			N'select '+@nsql_top100+ char(13)+char(10)+
				+@field_adjustment_id				+'	as adjustment_id
				,'+@field_line_date					+'	as line_date
				,'+@field_curve_code				+'	as curve_code
				,'+@field_curve_name				+'	as curve_name
				,'+@field_curve_desc				+'	as curve_desc
				,'+@field_curve_type				+'	as curve_type
				,'+@field_source_system				+'	as source_system
				,'+@field_curve_value_type			+'	as curve_value_type
				,'+@field_usage_type				+'	as usage_type
				,'+@field_curve_reference			+'	as curve_reference
				,'+@field_forward_nbr_of_time_units	+'	as forward_nbr_of_time_units
				,'+@field_forward_time_unit			+'	as forward_time_unit
				,'+@field_currency					+'	as currency
				,'+@field_day_count_convention		+'	as day_count_convention
				,'+@field_business_day_convention	+'	as business_day_convention
				,'+@field_calendar					+'	as calendar
				,'+@field_retry_count				+'	as retry_count
				,'+@field_start_validity_date		+'	as start_validity_date
				,'+@field_end_validity_date			+'	as end_validity_date
				,'+@field_value_perc1				+'	as value_perc1
				,'+@field_value_amount1				+'	as value_amount1
				,'+@field_term_start_date1			+'	as term_start_date1
				,'+@field_char_cust_element1		+'	as char_cust_element1
				,'+@field_char_cust_element2		+'	as char_cust_element2
				,'+@field_char_cust_element3		+'	as char_cust_element3
				,'+@field_char_cust_element4		+'	as char_cust_element4
				,'+@field_char_cust_element5		+'	as char_cust_element5
				,'+@field_num_cust_element1			+'	as num_cust_element1
				,'+@field_num_cust_element2			+'	as num_cust_element2
				,'+@field_num_cust_element3			+'	as num_cust_element3
				,'+@field_num_cust_element4			+'	as num_cust_element4
				,'+@field_num_cust_element5			+'	as num_cust_element5
				,'+@field_last_modified				+'	as last_modified
				,'+@field_modified_by				+'	as modified_by
			from t_nonretail_portfolio_marginal_pit_pd_tbank src
				join (
					select s.scenario_id, s.scenario_name
						,case
							when s.scenario_id in (3,4,5,51)	then	''1''
							when s.scenario_id in (54,55,56,57)	then	''2''
							when s.scenario_id in (110)			then	''3''
							else ''unknown'' end as stage
						,''PD'' as value_type
						,case
							when s.scenario_id in (51,57)			then	''ms'+convert(nvarchar,@idx_current_ms)+'_ttc_pd''
							when s.scenario_id in (3,4,5,54,55,56)	then	''ms'+convert(nvarchar,@idx_current_ms)+'_pit_pd''
							when s.scenario_id in (110)				then	''default=1''
							else ''unknown'' end as src_field_name
						,case
							when s.scenario_id in (5,56)		then	''SCN001''
							when s.scenario_id in (3,54)		then	''SCN002''
							when s.scenario_id in (4,55)		then	''SCN003''
							else ''NON FL'' end as scenario_id_tbank
					from t_scenario s
					where s.scenario_id in (3,4,5,51,54,55,56,57,110)
				) se on 1=1
					and (se.scenario_id_tbank = src.scenario_id
						or (src.scenario_id=''SCN001'' and se.scenario_id_tbank = ''NON FL'')) -- get non FL
			where as_of_date = @reporting_date
				and ((stage=1 and forecast_month between 1 and 12)	-- stage 1 use T1 - T12
						or (stage=2 and forecast_month > 0)				-- stage 2 use T1 - Txx(180)
						or (stage=3 and forecast_month = 0))				-- stage 3 use T0'
			;

			declare @paramDef nvarchar(max);
			set @paramDef = N'@reporting_date datetime
							,@proc_name d_name';-- end prepare dynamic t-sql

			/* --original sql
			-- MS1 9 scenario looping to MS22
			select --top 100 
				CONCAT(N'NRT','-','MS',1,'|',se.stage,'|',se.scenario_id,'|','PD') as curve_code
				,case
					when se.scenario_id in (51,57)			then	ms1_ttc_pd
					when se.scenario_id in (3,4,5,54,55,56)	then	ms1_pit_pd
					when se.scenario_id in (110)			then	1.0
					else 0.0 end as value_perc1
				,eomonth(dateadd(month,forecast_month,as_of_date)) as term_start_date1
				, se.*
				, src.*
			from t_nonretail_portfolio_marginal_pit_pd_tbank src
				join (
					select s.scenario_id, s.scenario_name
						,case
							when s.scenario_id in (3,4,5,51)	then	'1'
							when s.scenario_id in (54,55,56,57)	then	'2'
							when s.scenario_id in (110)			then	'3'
							else 'unknown' end as stage
						,'PD' as value_type
						,case
							when s.scenario_id in (51,57)			then	'ms1_ttc_pd'
							when s.scenario_id in (3,4,5,54,55,56)	then	'ms1_pit_pd'
							when s.scenario_id in (110)				then	'default=1'
							else 'unknown' end as src_field_name
						,case
							when s.scenario_id in (5,56)		then	'SCN001'
							when s.scenario_id in (3,54)		then	'SCN002'
							when s.scenario_id in (4,55)		then	'SCN003'
							else 'NON FL' end as scenario_id_tbank
					from t_scenario s
					where s.scenario_id in (3,4,5,51,54,55,56,57,110)
				) se on 1=1
					and se.scenario_id_tbank = src.scenario_id
					or (src.scenario_id='SCN001' and se.scenario_id_tbank = 'NON FL') -- get non FL
			where as_of_date = '2018-04-30'
				and (stage=1 and forecast_month between 1 and 12)	-- stage 1 use T1 - T12
				 or (stage=2 and forecast_month > 0)				-- stage 2 use T1 - Txx(180)
				 or (stage=3 and forecast_month = 0)				-- stage 3 use T0
			order by 1,3
			*/
			
			IF @param_is_test = 1
			BEGIN
				PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Inserting master scale ' + @param_src_table_name + ' to ' + @table_type_name
			END

			--insert data with sp_executesql
			exec sp_executesql @nsql ,@paramDef ,@reporting_date	=	@param_reporting_date
												,@proc_name			=	@proc_name
			;

			select @idx_current_ms = @idx_current_ms+1;
		END -- End While
	END TRY
	BEGIN CATCH -- for catch random error
		
		--/******************************************************************/
		--/*End Process Log*/	
		--/******************************************************************/
		--update t_ecl_process_log_tbank 
		--set end_time		=	getdate(),
		--	execution_time	=	cast((getdate() - start_time) as time(0)),
		--	process_status	=	'Failed'
		--where process_id = @log_pid
		--	and cob_date = @cob_date;

		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = 1,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)

	END CATCH -- finish insert t_etl_adj_data_

	BEGIN TRANSACTION;
	BEGIN TRY -- start execute main process		
		declare @adjustment_id			d_id;
		declare @adj_type_id_crv_curve	d_id;

		-- get table type id
		SELECT @adj_type_id_crv_curve   = (SELECT typ.type_id FROM t_adj_type typ JOIN t_adj_category cat ON cat.category_id = typ.category_id WHERE typ.type_name = @table_type_name);
		-- prepare adjustment before load to stg
		EXEC @status = [dbo].[p_adj_save]
								@adjustment_id = @adjustment_id OUTPUT,
								@type_id = @adj_type_id_crv_curve,
								@reporting_date = @param_reporting_date,
								@comment = @param_src_table_name
		;

		-- change adjustment_id 0 to real adjustment_id
		update t_etl_adj_data_crv_curve
		set adjustment_id = @adjustment_id
		where adjustment_id = 0;

		IF @param_is_test = 1
		BEGIN
			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Running HMS p_etl_adj_xtr_crv_curve'
		END

		-- run hms
		EXEC @status = [dbo].[p_etl_adj_xtr_crv_curve]
                            @portfolio_date = @param_reporting_date,
                            @del_data_after_loading = 0,
                            @div_rate_by_100 = 0,
                            @adjustment_id = @adjustment_id,
                            @debug = @param_is_test

		IF @param_is_test = 1
		BEGIN
			declare @hms_result nvarchar(max);

			select @hms_result = FORMATMESSAGE('Inserted %i curve to t_crv_curve and %i curve point to t_crv_curve_point_value' ,count(distinct crv.curve_code), count(p.curve_point_value_id))
			from t_crv_curve crv join
				t_crv_curve_point_value p on crv.curve_id=p.curve_id
			where exists (
				select etl.curve_code
				from t_etl_adj_data_crv_curve etl
				where etl.adjustment_id=@adjustment_id
					and etl.curve_code=crv.curve_code
			);

			PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + @hms_result;
		END

		SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
		IF @error <> 0 OR @status <> 0
		  BEGIN
			RAISERROR('HMS encountered an error.', 16, 1, @proc_name, @error)
			RETURN 
		  END	

		COMMIT;
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK;
		
		--/******************************************************************/
		--/*End Process Log*/	
		--/******************************************************************/
		--update t_ecl_process_log_tbank 
		--set end_time		=	getdate(),
		--	execution_time	=	cast((getdate() - start_time) as time(0)),
		--	process_status	=	'Failed'
		--where process_id = @log_pid
		--	and cob_date = @cob_date;

		
		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'
							+ char(13)+char(10)+char(9)+'##################################################';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = 1,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
	END CATCH

	--/******************************************************************/
	--/*End Process Log*/	
	--/******************************************************************/
	--update t_ecl_process_log_tbank 
	--set end_time		=	getdate(),
	--	execution_time	=	cast((getdate() - start_time) as time(0)),
	--	process_status	=	'Success'
	--where process_id = @log_pid
	--	and cob_date = @cob_date;
	--Print '#################################################################################';

	--add logging here
	set @ale_message = N'End: ' + @proc_name;
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_FINISH', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				--@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				--@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

-- Return succes
RETURN (0) 

GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pit_pd_master_scale' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_pd_master_scale] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pit_pd_master_scale] has NOT been altered due to errors!'
END
GO
