
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pop_ead_cc_fp' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pop_ead_cc_fp] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pop_ead_cc_fp] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pop_ead_cc_fp]...'
GO

ALTER PROCEDURE [dbo].[p_stg_pop_ead_cc_fp] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime	= null,
	@param_is_test			bit			= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	Stored procedure for population t_etl_adj_data_flow_set_flow  *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

	-- variables for ale
	declare @ale_message						d_ale_message;
	declare @ale_context						xml;
	declare @ale_correlation_guid				UNIQUEIDENTIFIER;
	declare @ale_event_log_guid					UNIQUEIDENTIFIER;

-- Declare local variables
	declare @cob_date							datetime;
	declare @max_date							datetime				=	'9999-12-31';
	declare @entity								d_entity				=	N'TBANK';
	declare @lot_fn_code						d_std_record_function	=	N'ECLSEG';
	declare @max_lot_id							d_id;
	declare @scenario_context_id				d_id					=	301003;

	declare @adjustment_id						d_id;
	declare @adj_type_id						d_id;
	declare @src_table_name1					d_name					=	N't_retail_cc_fp_tbank'
	declare @src_table_name2					d_name					=	N''
	declare @src_table_name3					d_name					=	N''
	declare @table_type_name					d_name					=	N't_etl_adj_data_flow_set_flow';
	declare @pool_id_list						nvarchar(100)			=	'(8,9)';
	
	-- variables for t_ecl_process_log_tbank
	declare @log_pid							int;
	declare @log_process_type					varchar(20)				=	N'Retail';
	declare @log_model_name						varchar(100)			=	N'Pop CC&FP EAD';
	declare @log_result_table					varchar(50)				=	@table_type_name;

	-- variables for cursor
	declare @cursor_table_name					nvarchar(256);
	declare @cursor_stage						d_name;
	declare @cursor_scenario_id					d_id;
	declare @cursor_scenario_id_str				d_name;
	declare @cursor_scenario_name				d_name;
	declare @cursor_src_field_name				d_name;
	declare @cursor_src_flow_type				d_name;
	declare @cursor_model_name					d_name;

	-- variable for dynamic sql
	declare @nsql								nvarchar(max);
	declare @nsql_select						nvarchar(50);
	declare @nsql_column						nvarchar(max);
	declare @nsql_table							nvarchar(max);
	declare @nsql_filter						nvarchar(max);
	declare @nsql_insert						nvarchar(max);
	declare @paramDef							nvarchar(max);

	-- variable for insert t_etl_adj_data_xxx			
	declare @field_adjustment_id   				nvarchar(max);
	declare @field_line_nr		   				nvarchar(max);
	declare @field_flag         				nvarchar(max);
	declare @field_line_status         			nvarchar(max);
	declare @field_retry_count         			nvarchar(max);
	declare @field_line_date         			nvarchar(max);
	declare @field_entity	            		nvarchar(max);
	declare @field_object_origin				nvarchar(max);
	declare @field_object_key_type				nvarchar(max);
	declare @field_object_key_value				nvarchar(max);
	declare @field_flow_type    				nvarchar(max);
	declare @field_flow_set_id   				nvarchar(max);
	declare @field_scenario_id					nvarchar(max);
	declare @field_amount_type					nvarchar(max);
	declare @field_start_validity_date      	nvarchar(max);
	declare @field_end_validity_date			nvarchar(max);
	declare @field_calculation_date				nvarchar(max);
	declare @field_flow_date					nvarchar(max);
	declare @field_currency						nvarchar(max);
	declare @field_amount						nvarchar(max);
	declare @field_char_cust_element1			nvarchar(max);
	declare @field_char_cust_element2			nvarchar(max);
	declare @field_char_cust_element3			nvarchar(max);
	declare @field_char_cust_element4			nvarchar(max);
	declare @field_char_cust_element5			nvarchar(max);
	declare @field_num_cust_element1			nvarchar(max);
	declare @field_num_cust_element2			nvarchar(max);
	declare @field_num_cust_element3			nvarchar(max);
	declare @field_num_cust_element4			nvarchar(max);
	declare @field_num_cust_element5			nvarchar(max);
	declare @field_last_modified				nvarchar(max);
	declare @field_modified_by					nvarchar(max);
		
	-- assign cob date
	select @cob_date = isnull(@param_reporting_date,cob_date) from t_entity where entity = @entity;

	-- assign max lot id
	select @max_lot_id = max(lot.lot_id) from t_lot_definition lot where lot.std_record_function_code = @lot_fn_code;

	--add logging here
	set @ale_message = N'Start: ' + @proc_name
						+ N'(@param_reporting_date=' + CONVERT(varchar,@param_reporting_date)
						+ char(13)+char(10)+char(9)+N',@param_is_test=' + CONVERT(varchar,@param_is_test)
						+ char(13)+char(10)+char(9)+N',@cob_date=' + CONVERT(varchar,@cob_date)
						+ N')';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_START', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

	--/*Start Process Log*/ -- change to proc in the future
	--SET @log_pid = (select isnull(max(process_id),0)+1 
	--				from t_ecl_process_log_tbank
	--				where cob_date = @cob_date);

	--insert into t_ecl_process_log_tbank (cob_date,process_id,process_type,process_name,model_name,result_table,start_time,process_status)
	--select  @cob_date, @log_pid, @log_process_type, @proc_name, @log_model_name, @log_result_table, getdate(),'Processing';
	--/*End Process Log*/

-- Start actual procedure flow

	---- check input parameter
	--IF @param_reporting_date is null 
	--BEGIN 
	--	select @error_msg = isnull(@param_reporting_date,',@param_reporting_date');
	--	set @error_msg = N'This procedure must send parameter: ' + substring(@error_msg,2,len(@error_msg))
	--	RAISERROR (@error_msg, 16, 1, @proc_name)
	--	RETURN 1
	--END
		
	BEGIN TRY
		-- clear adjustment_id 0 before run insert
		delete t_etl_adj_data_flow_set_flow where adjustment_id=0;

		-- config table for cursor looping
		declare p_stg_pop_ead_cc_fp_cursor cursor local static for
		-- select target source table name and scenario mapping
		select t.TABLE_NAME, stage, se.scenario_id, se.scenario_name, se.flow_type, se.src_field_name
		from INFORMATION_SCHEMA.TABLES t
			join ( -- create configuration table with inline sql, may move to physical table in the future
				select s.scenario_id, s.scenario_name
					,case
						when CHARINDEX('stage 1',lower(s.scenario_name)) > 0	then	'1'
						when CHARINDEX('stage 2',lower(s.scenario_name)) > 0	then	'2'
						when CHARINDEX('stage 3',lower(s.scenario_name)) > 0	then	'3'
						else 'unknown' end as stage
					,case
						when CHARINDEX('stage 1',lower(s.scenario_name)) > 0	then	'EAD_1'
						when CHARINDEX('stage 2',lower(s.scenario_name)) > 0	then	'EAD_2'
						when CHARINDEX('stage 3',lower(s.scenario_name)) > 0	then	'EAD_3'
						else 'unknown' end as flow_type
					,case
						when CHARINDEX('stage 1',lower(s.scenario_name)) > 0	then	'ead_stage1'
						when CHARINDEX('stage 2',lower(s.scenario_name)) > 0	then	'ead_stage2'
						when CHARINDEX('stage 3',lower(s.scenario_name)) > 0	then	'ead_stage3'
						else 'unknown' end as src_field_name
				from t_scenario s
				where s.scenario_context_id = @scenario_context_id
					and s.scenario_name in ('Stage 1 NON FL','Stage 2 Specific Treatment Group NON FL','Stage 3 Specific Treatment Group')
			) se on 1=1 
		where t.TABLE_NAME in (@src_table_name1,@src_table_name2,@src_table_name3) -- change product table here, may change to dynamic sql for easy config and mantinance in the future
		
		open p_stg_pop_ead_cc_fp_cursor
		fetch next from p_stg_pop_ead_cc_fp_cursor into @cursor_table_name, @cursor_stage, @cursor_scenario_id, @cursor_scenario_name, @cursor_src_flow_type, @cursor_src_field_name

		While @@FETCH_STATUS = 0
		BEGIN -- looping insert to adjustment_id 0

			-- assign field for select & insert
			-- text must have single-quote/char(39) between text
			-- function, field, numeric do not need single-quote
			select	 @field_adjustment_id   			= N'0'												-- must update with p_adj_save before run HMS
					,@field_line_nr         			= N'row_number() over(order by src.deal_id)'		-- must update before run if insert multiple time
					,@field_flag            			= char(39)+N'U'+char(39)
					,@field_line_status     			= char(39)+N'0'+char(39)
					,@field_retry_count     			= N'0'
					,@field_line_date       			= N'CURRENT_TIMESTAMP'
					,@field_entity          			= N'@entity'
					,@field_object_origin   			= N'seg.item_object_origin'							-- if deal: t_ecl_segmentation.item_object_origin
					,@field_object_key_type 			= char(39)+N'2'+char(39)
					,@field_object_key_value			= N'CONCAT('''+case @param_is_test when 0 then '' else 'TEST-' end
															+''',src.deal_id,''|'',@entity)'
					,@field_flow_type       			= N'@cursor_src_flow_type'
					,@field_flow_set_id					= N'null'											-- generate when HMS
					,@field_scenario_id					= N'@cursor_stage'									-- change to stage id
					,@field_amount_type					= char(39)+N'CAPITA'+char(39)
					,@field_start_validity_date         = N'@cob_date'
					,@field_end_validity_date           = N'@max_date'
					,@field_calculation_date            = N'@cob_date'
					,@field_flow_date					= N'eomonth(dateadd(month,case @cursor_stage when ''3'' then 0 else 1 end,@cob_date))'
					,@field_currency					= N'seg.currency'									-- if deal: t_ecl_segmentation.currency
					,@field_amount						= N'case @cursor_stage'+char(13)+char(10)+
															+'when ''1''	then	src.ead_stage1'+char(13)+char(10)+
															+'when ''2''	then	src.ead_stage2'+char(13)+char(10)+
															+'when ''3''	then	src.ead_stage3'+char(13)+char(10)+
															+'else src.ead_stage3 end'
					,@field_char_cust_element1			= N'src.date_matur'									-- if deal: date_matur
					,@field_char_cust_element2			= N'CONVERT(nvarchar,@cursor_scenario_id)'			-- scenario_id
					,@field_char_cust_element3			= N'src.segment_name'								-- segment_name
					,@field_char_cust_element4			= N'@cob_date'										-- reporting_date
					,@field_char_cust_element5			= char(39)+N''+char(39)								-- free slot, not use
					,@field_num_cust_element1			= N'CONVERT(numeric,src.segment_id)'								-- segment_id
					,@field_num_cust_element2			= N'CONVERT(numeric,src.remaining_maturity)'						-- if deal: remaining_maturity
					,@field_num_cust_element3			= N'CONVERT(numeric,src.remaining_term)'							-- if deal: remaining_term
					,@field_num_cust_element4			= N'CONVERT(numeric,@cursor_stage)'					-- if deal: current stage?
					,@field_num_cust_element5			= N'0.0'											-- free slot, not use
					,@field_last_modified				= N'CURRENT_TIMESTAMP'
					,@field_modified_by					= N'@proc_name'
			;

			-- assign nsql
			select @nsql_insert = N'INSERT INTO t_etl_adj_data_flow_set_flow
				(adjustment_id, line_nr, flag, line_status, retry_count, line_date, entity, object_origin, object_key_type, object_key_value, flow_type, flow_set_id, scenario_id, amount_type, start_validity_date, end_validity_date, calculation_date, flow_date, currency, amount, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5, last_modified, modified_by)';
		
			select @nsql_select = N''+char(13)+char(10)+ case @param_is_test when 0 then ' select ' else ' select top 300 ' end;
		
			select @nsql_column = N''																	+char(13)+char(10)+
					@field_adjustment_id   					+'	as adjustment_id,'						+char(13)+char(10)+
					@field_line_nr         					+'	as line_nr,'							+char(13)+char(10)+
					@field_flag            					+'	as flag,'								+char(13)+char(10)+
					@field_line_status     					+'	as line_status,'						+char(13)+char(10)+
					@field_retry_count     					+'	as retry_count,'						+char(13)+char(10)+
					@field_line_date       					+'	as line_date,'							+char(13)+char(10)+
					@field_entity          					+'	as entity,'								+char(13)+char(10)+
					@field_object_origin   					+'	as object_origin,'						+char(13)+char(10)+
					@field_object_key_type 					+'	as object_key_type,'					+char(13)+char(10)+
					@field_object_key_value					+'	as object_key_value,'					+char(13)+char(10)+
					@field_flow_type       					+'	as flow_type,'							+char(13)+char(10)+
					@field_flow_set_id						+'	as flow_set_id,'						+char(13)+char(10)+
					@field_scenario_id						+'	as scenario_id,'						+char(13)+char(10)+
					@field_amount_type						+'	as amount_type,'						+char(13)+char(10)+
					@field_start_validity_date				+'	as start_validity_date,'				+char(13)+char(10)+
					@field_end_validity_date				+'	as end_validity_date,'					+char(13)+char(10)+
					@field_calculation_date					+'	as calculation_date,'					+char(13)+char(10)+
					@field_flow_date						+'	as flow_date,'							+char(13)+char(10)+
					@field_currency							+'	as currency,'							+char(13)+char(10)+
					@field_amount							+'	as amount,'								+char(13)+char(10)+
					@field_char_cust_element1				+'	as char_cust_element1,'					+char(13)+char(10)+
					@field_char_cust_element2				+'	as char_cust_element2,'					+char(13)+char(10)+
					@field_char_cust_element3				+'	as char_cust_element3,'					+char(13)+char(10)+
					@field_char_cust_element4				+'	as char_cust_element4,'					+char(13)+char(10)+
					@field_char_cust_element5				+'	as char_cust_element5,'					+char(13)+char(10)+
					@field_num_cust_element1				+'	as num_cust_element1,'					+char(13)+char(10)+
					@field_num_cust_element2				+'	as num_cust_element2,'					+char(13)+char(10)+
					@field_num_cust_element3				+'	as num_cust_element3,'					+char(13)+char(10)+
					@field_num_cust_element4				+'	as num_cust_element4,'					+char(13)+char(10)+
					@field_num_cust_element5				+'	as num_cust_element5,'					+char(13)+char(10)+
					@field_last_modified					+'	as last_modified,'						+char(13)+char(10)+
					@field_modified_by						+'	as modified_by';
				
			select @nsql_table = N''																	+char(13)+char(10)+
					N'from '+@cursor_table_name+' src'													+char(13)+char(10)+
						N'join t_ecl_segmentation seg on src.deal_id= seg.deal_tfi_id'					+char(13)+char(10)+
						N'and seg.lot_id = @max_lot_id'
		
			select @nsql_filter = N''																	+char(13)+char(10)+
					N'where src.deal_id is not null'													+char(13)+char(10)+
						N'and src.as_of_date = @cob_date';

			-- param definition
			set @paramDef = N'@proc_name			sysname
							,@cursor_scenario_id	d_id
							,@cursor_stage			d_name
							,@cursor_src_flow_type	d_name
							,@entity				d_entity
							,@cob_date				datetime
							,@max_date				datetime
							,@max_lot_id			d_id'

			-- final t-sql
			set @nsql = @nsql_insert + @nsql_select + @nsql_column + @nsql_table + @nsql_filter;

			-- check final t-sql length
			IF len(@nsql) > 4000 or @nsql is null
			BEGIN
				set @ale_message = N'SQL string is over than 4000 character or null.';
				set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, len(@nsql) as len_nsql
					, @nsql_insert as nsql_insert, @nsql_select as nsql_select
					, @nsql_column as nsql_column, @nsql_table as nsql_table
					, @nsql_filter as nsql_filter FOR XML PATH);

				EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
							@event_type                = N'E',
							@context                   = @ale_context,
							@message                   = @ale_message,
							@event_type_detail         = N'E',
							--@correlation_guid          = @ale_correlation_guid OUTPUT,
							--@timestamp_utc             = N'',
							--@event_log_guid            = @ale_event_log_guid OUTPUT,
							@use_synchronous_logging   = 0,
							@print_ind                 = @param_is_test,
							@print_level               = 0,
							@calling_proc_name         = @proc_name;

				PRINT '@nsql_insert:'	+ISNULL(@nsql_insert,'NULL')
				PRINT '@nsql_select:'	+ISNULL(@nsql_select,'NULL')
				PRINT '@nsql_column:'	+ISNULL(@nsql_column,'NULL')
				PRINT '@nsql_table:'	+ISNULL(@nsql_table,'NULL')
				PRINT '@nsql_filter:'	+ISNULL(@nsql_filter,'NULL')

				set @error_msg = @ale_message
				RAISERROR (@error_msg, 16, 1, @proc_name)
			END

			--add logging here
			set @ale_message = N'Inserting '+ @src_table_name1+','+@src_table_name2+','+@src_table_name3 + ' to ' + @table_type_name+' (adjustment_id:'+CONVERT(NVARCHAR,@adjustment_id)+')';
			set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @cob_date as cob_date, @nsql as nsql FOR XML PATH);

			EXEC p_ale_log @event_name            = N'CREATE_ADJUSTMENT', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT 
						@event_type                = N'I',
						@context                   = @ale_context,
						@message                   = @ale_message,
						@event_type_detail         = N'I',
						--@correlation_guid          = @ale_correlation_guid OUTPUT,
						--@timestamp_utc             = N'',
						--@event_log_guid            = @ale_event_log_guid OUTPUT,
						@use_synchronous_logging   = 0,
						@print_ind                 = @param_is_test,
						@print_level               = 0,
						@calling_proc_name         = @proc_name;

			RAISERROR(@ale_message,10,1,@proc_name);

			-- insert data with sp_executesql
			exec sp_executesql @nsql ,@paramDef ,@proc_name				=@proc_name
												,@cursor_scenario_id	=@cursor_scenario_id	
												,@cursor_stage			=@cursor_stage			
												,@cursor_src_flow_type	=@cursor_src_flow_type	
												,@entity				=@entity				
												,@cob_date				=@cob_date				
												,@max_date				=@max_date				
												,@max_lot_id			=@max_lot_id
			;

			BEGIN TRANSACTION;
			BEGIN TRY
				declare @comment	d_long_description		= N'EAD|'+@cursor_table_name+N'|'+@cursor_scenario_name
				-- get table type id
				SELECT @adj_type_id   = (SELECT typ.type_id FROM t_adj_type typ JOIN t_adj_category cat ON cat.category_id = typ.category_id WHERE typ.type_name = @table_type_name);
				-- prepare adjustment before load to stg
				set @adjustment_id = null;
				EXEC @status = [dbo].[p_adj_save]
										@adjustment_id = @adjustment_id OUTPUT,
										@type_id = @adj_type_id,
										@reporting_date = @param_reporting_date,
										@comment = @comment
				;

				-- update adjustment_id
				update t_etl_adj_data_flow_set_flow
				set  adjustment_id	= @adjustment_id
				where adjustment_id = 0;

				--add logging here
				set @ale_message = N'Running HMS.';
				set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @cob_date as cob_date, @nsql as nsql FOR XML PATH);

				EXEC p_ale_log @event_name            = N'START_ADJUSTMENT', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
							@event_type                = N'I',
							@context                   = @ale_context,
							@message                   = @ale_message,
							@event_type_detail         = N'I',
							--@correlation_guid          = @ale_correlation_guid OUTPUT,
							--@timestamp_utc             = N'',
							--@event_log_guid            = @ale_event_log_guid OUTPUT,
							@use_synchronous_logging   = 0,
							@print_ind                 = @param_is_test,
							@print_level               = 0,
							@calling_proc_name         = @proc_name;

				RAISERROR(@ale_message,10,1,@proc_name);

				-- run hms
				exec p_etl_adj_data_flow_tbank	@execution_date	= @cob_date, 
												@cleanup_type = 2,
												@cleanup_staging = 0,
												@only_cleanup_changed = 1,
												@use_delta_load = 1, 
												@adjustment_id	= @adjustment_id,
												@entity			= @entity,  
												@log_output     = 0

				COMMIT;
			END TRY
			BEGIN CATCH  -- rollback when failed inserting or hms
				IF @@TRANCOUNT > 0 ROLLBACK;

				--add logging here
				set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').';
				set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @cob_date as cob_date, @adjustment_id as adjustment_id FOR XML PATH);

				EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
							@event_type                = N'E',
							@context                   = @ale_context,
							@message                   = @ale_message,
							@event_type_detail         = N'E',
							--@correlation_guid          = @ale_correlation_guid OUTPUT,
							--@timestamp_utc             = N'',
							--@event_log_guid            = @ale_event_log_guid OUTPUT,
							@use_synchronous_logging   = 0,
							@print_ind                 = @param_is_test,
							@print_level               = 0,
							@calling_proc_name         = @proc_name;

				PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -E- ' + @ale_message
									+ char(13)+char(10)+N'@nsql=' + @nsql
									+ char(13)+char(10)+N'@paramDef=' + @paramDef;

				SELECT @error_msg =  @ale_message
						,@error_severity = ERROR_SEVERITY()
						,@error_state	 = ERROR_STATE();
				RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
			END CATCH

			fetch next from p_stg_pop_ead_cc_fp_cursor into @cursor_table_name, @cursor_stage, @cursor_scenario_id, @cursor_scenario_name, @cursor_src_flow_type, @cursor_src_field_name
		END

		close p_stg_pop_ead_cc_fp_cursor
		deallocate p_stg_pop_ead_cc_fp_cursor

	END TRY
	BEGIN CATCH -- for catch random error
		
		--/******************************************************************/
		--/*End Process Log*/	
		--/******************************************************************/
		--update t_ecl_process_log_tbank 
		--set end_time		=	getdate(),
		--	execution_time	=	cast((getdate() - start_time) as time(0)),
		--	process_status	=	'Failed'
		--where process_id = @log_pid
		--	and cob_date = @cob_date;

		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').'
							+ char(13)+char(10)+char(9)+'##################################################';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @cob_date as cob_date, @nsql as nsql FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = 1,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
	END CATCH

	--/******************************************************************/
	--/*End Process Log*/	
	--/******************************************************************/
	--update t_ecl_process_log_tbank 
	--set end_time		=	getdate(),
	--	execution_time	=	cast((getdate() - start_time) as time(0)),
	--	process_status	=	'Success'
	--where process_id = @log_pid
	--	and cob_date = @cob_date;
	--Print '#################################################################################';

	--add logging here
	set @ale_message = N'End: ' + @proc_name;
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @cob_date as cob_date FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_FINISH', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				--@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				--@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;
-- Return succes
RETURN (0) 

GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pop_ead_cc_fp' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pop_ead_cc_fp] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pop_ead_cc_fp] has NOT been altered due to errors!'
END
GO
