
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_etl_adj_data_flow_tbank' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_etl_adj_data_flow_tbank] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_etl_adj_data_flow_tbank] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_etl_adj_data_flow_tbank]...'
GO
ALTER PROCEDURE [dbo].p_etl_adj_data_flow_tbank (
        @execution_date                    DATETIME                  = NULL,
        @use_real_time_processing          TINYINT                   = 0,
        @cleanup_type                      TINYINT                   = 0,
        @cleanup_staging                   BIT                       = 0,
        @only_cleanup_changed              BIT                       = 0,
        @use_delta_load                    BIT                       = 0,
        @adjustment_id                     d_id                      = NULL,
        @entity                            d_entity                  = NULL,
        @dflt_entity                       d_entity                  = NULL,
        @dflt_object_origin                d_object_origin           = NULL,
        @dflt_object_key_type              d_object_key_type         = NULL,
        @dflt_object_key_value             d_object_key_value        = NULL,
        @dflt_flow_type                    d_flow_type               = NULL,
        @dflt_flow_set_id                  d_id                      = NULL,
        @dflt_scenario_id                  d_id                      = NULL,
        @dflt_amount_type                  d_amount_type             = NULL,
        @dflt_calculation_date             datetime                  = NULL,
        @dflt_flow_date                    datetime                  = NULL,
        @dflt_currency                     d_currency                = NULL,
        @dflt_amount                       d_amount                  = NULL,
        @dflt_flow_set_char_cust_element1  d_char_cust_element1      = NULL,
        @dflt_flow_set_char_cust_element2  d_char_cust_element2      = NULL,
        @dflt_flow_set_char_cust_element3  d_char_cust_element3      = NULL,
        @dflt_flow_set_char_cust_element4  d_char_cust_element4      = NULL,
        @dflt_flow_set_char_cust_element5  d_char_cust_element5      = NULL,
        @dflt_flow_set_num_cust_element1   d_num_cust_element1       = NULL,
        @dflt_flow_set_num_cust_element2   d_num_cust_element2       = NULL,
        @dflt_flow_set_num_cust_element3   d_num_cust_element3       = NULL,
        @dflt_flow_set_num_cust_element4   d_num_cust_element4       = NULL,
        @dflt_flow_set_num_cust_element5   d_num_cust_element5       = NULL,
        @log_output                        BIT                       = 0
    )
-- * Purpose: Load cash flows
-- * Inputs: 
-- *         @execution_date : instead of start_validity_date OR CURRENT_TIMESTAMP (real-time). 
-- *         @cleanup_type : 0: No cleanup
-- *                         1: Delete historical data if start_validity_date > @execution_date.
-- *                         2: Delete historical data if start_validity_date > @execution_date
-- *                            AND the previous delta load of SAME day is completely rolled back
-- *                            NOTE: Only use delta loads when using @cleanup_type = 2 !

AS
-- *******************************************************************************************
-- *         @cleanup_staging : Delete copied data from the staging table.
-- *         @entity : run for specific entity or for all (in case of NULL).
-- *         @adjustment_id : identifies what data to load from t_etl_adj_data_flow_set_flow.
-- *         @log_output : output log or not
-- * Returns: Nothing
-- * Notes: Checksum not used because it might be identical for different strings
-- * Notes: This procedure is added as an API procedure in the online-help. Whenever changes
-- *        occur in input parameters and/or process description, the changes should also be
-- *        integrated in t_sys_help_object and t_sys_help_object_member.
-- *******************************************************************************************
SET NOCOUNT ON                  -- suppress row count display

-- declare error/debug variables
DECLARE @proc_name sysname      -- procedure name
DECLARE @status    INT          -- return status
DECLARE @error     INT          -- saved error context
DECLARE @rowcount  INT          -- saved rowcount context
DECLARE @error_count     INT
DECLARE @warning_count   INT
DECLARE @msg       NVARCHAR(255) -- error message text
-- Declare the user and evd as a param to avoid in-select function calling
DECLARE @user      d_user
DECLARE @datediff       datetime
-- Max_date is declared in order to remain consistent with the reasoning behind the creation of user and evd
DECLARE @max_date   datetime
DECLARE @min_date   datetime
DECLARE @nsql       nvarchar(max)

-- initialise error/debug variables
SELECT  @proc_name = OBJECT_NAME( @@PROCID ),
        @status    = 0,
        @error     = 0,
        @rowcount  = 0,
        @execution_date = dbo.fn_datepart(@execution_date), -- Remove hour notation
        @user      = dbo.fn_user(),
        @max_date  = dbo.fn_max_date(), -- consistent with @user tweak
        @min_date  = dbo.fn_min_date()

SELECT @datediff = CASE WHEN @use_real_time_processing = 1 THEN N'1900/01/01 00:00:01'
                        WHEN @use_real_time_processing = 2 THEN N'1900/01/01 00:00:00.003'
                        ELSE N'1900/01/02 00:00:00.000' END

DECLARE @msg_to_add_to_event_log NVARCHAR(MAX)
DECLARE @event_log_id d_id

-- LOG table
DECLARE @log TABLE
(
    [id]                    INT IDENTITY(1,1)   NOT NULL,
    [Action]                NVARCHAR(255)        NOT NULL,
    Nbr_Of_Rows_Affected    INT                 NULL
)

-- Valid @cleanup_type
IF @cleanup_type NOT IN (0,1,2)
  BEGIN
    RAISERROR (52897, 16, 1, @proc_name, @cleanup_type)
    RETURN (1)
  END

-- ****************************************************
-- Extra validation needed in case no @execution_date is passed:
-- start_validity_date must be available in t_etl_adj_data_... AND meaningfull (= NOT @min_date and NOT @max_date)
-- ****************************************************
IF @execution_date IS NULL
BEGIN
    IF EXISTS (SELECT * FROM dbo.t_etl_adj_data_flow_set_flow
                WHERE (entity = @entity OR @entity IS NULL)
                  AND (adjustment_id = @adjustment_id OR @adjustment_id IS NULL) 
                  AND (start_validity_date IS NULL OR start_validity_date IN (@min_date, @max_date))
               )
    BEGIN
        RAISERROR(62807, 16, 1, @proc_name, N't_etl_adj_data_flow_set_flow')
        RETURN (1)
    END
END

-- ****************************************************
-- Create temp tables
-- ****************************************************
SELECT tbl = OBJECT_NAME(dc.parent_object_id), field = COL_NAME(dc.parent_object_id, dc.parent_column_id), def = dc.definition
  INTO #field_defaults
  FROM sys.default_constraints dc
 WHERE 1 = 2

SELECT CASE WHEN lf.flow_set_id IS NULL THEN NULL ELSE lf.flow_set_id END AS flow_set_id,  -- a little trick to make the field nullable
       ISNULL(elf.object_origin, lf.object_origin) AS object_origin,
       ISNULL(elf.object_key_type, lf.object_key_type) AS object_key_type,
       ISNULL(elf.object_key_value, lf.object_key_value) AS object_key_value,
       ISNULL(elf.entity, lf.entity) AS entity,
       ISNULL(elf.flow_type, lf.flow_type) AS flow_type,
       ISNULL(elf.scenario_id, lf.scenario_id) AS scenario_id,
       ISNULL(elf.amount_type, lf.amount_type) AS amount_type,
       ISNULL(elf.currency, lf.flow_currency) AS currency,
       ISNULL(elf.start_validity_date, @min_date) AS etl_start_validity_date
  INTO #changes
  FROM t_etl_adj_data_flow_set_flow elf
  JOIN v_flow lf
    ON elf.object_origin            = lf.object_origin
   AND elf.object_key_type          = lf.object_key_type
   AND elf.object_key_value         = lf.object_key_value
   AND elf.entity                   = lf.entity
   AND elf.flow_type                = lf.flow_type
   AND elf.scenario_id              = lf.scenario_id
   AND elf.amount_type              = lf.amount_type
   AND lf.end_validity_date         = @max_date
   AND lf.start_validity_date      <= ISNULL(@execution_date, elf.start_validity_date)
   AND lf.flow_date                 = elf.flow_date
   AND lf.flow_currency                  = elf.currency
 WHERE 1 = 2

-- ****************************************************
-- Load destination table defaults to be used when @dflt if not delivered
-- and NULL inserts could be forced when the value in ETL is NULL
-- ****************************************************
INSERT INTO #field_defaults (tbl, field, def)
SELECT OBJECT_NAME(dc.parent_object_id), COL_NAME(dc.parent_object_id, dc.parent_column_id), dc.definition
  FROM sys.default_constraints dc
 WHERE OBJECT_NAME(parent_object_id) LIKE 't_flow%'
   AND COL_NAME(dc.parent_object_id, dc.parent_column_id) NOT LIKE '%modified%'

  SELECT @error = @@error
  IF @error <> 0
    BEGIN
      RAISERROR(640007, 16, 1, @proc_name, N'#field_defaults')
      RETURN (1)
    END

SELECT @nsql = N'
UPDATE #field_defaults
   SET def = ' + def + N'
 WHERE tbl = ''' + tbl + N'''
   AND field = ''' + field + N'''
       '
  FROM #field_defaults
 WHERE def LIKE '%dbo%fn%'

EXEC (@nsql)

  SELECT @error = @@error
  IF @error <> 0
    BEGIN
      RAISERROR(640008, 16, 1, @proc_name, N'#field_defaults')
      RETURN(@error)
    END

WHILE EXISTS (SELECT * FROM #field_defaults WHERE LEFT(def, 1) = '(' AND RIGHT(def, 1) = ')')
  BEGIN
    UPDATE #field_defaults
       SET def = SUBSTRING(def,2, LEN(def)-2)
     WHERE LEFT(def, 1) = '(' AND RIGHT(def, 1) = ')'

      SELECT @error = @@error
      IF @error <> 0
        BEGIN
          RAISERROR(640008, 16, 1, @proc_name, N'#field_defaults')
          RETURN(@error)
        END
  END

UPDATE #field_defaults
   SET def = SUBSTRING(def,3, LEN(def)-3)
 WHERE LEFT(def, 2) = 'N''' AND RIGHT(def, 1) = ''''

  SELECT @error = @@error
  IF @error <> 0
    BEGIN
      RAISERROR(640008, 16, 1, @proc_name, N'#field_defaults')
      RETURN(@error)
    END

UPDATE #field_defaults
   SET def = SUBSTRING(def,2, LEN(def)-2)
 WHERE LEFT(def, 1) = '''' AND RIGHT(def, 1) = ''''

  SELECT @error = @@error
  IF @error <> 0
    BEGIN
      RAISERROR(640008, 16, 1, @proc_name, N'#field_defaults')
      RETURN(@error)
    END

SELECT  @dflt_entity                      = CASE WHEN tbl = 't_flow_set' AND field = 'entity' AND @dflt_entity IS NULL THEN def ELSE @dflt_entity END,
        @dflt_object_origin               = CASE WHEN tbl = 't_flow_set' AND field = 'object_origin' AND @dflt_object_origin IS NULL THEN def ELSE @dflt_object_origin END,
        @dflt_object_key_type             = CASE WHEN tbl = 't_flow_set' AND field = 'object_key_type' AND @dflt_object_key_type IS NULL THEN def ELSE @dflt_object_key_type END,
        @dflt_object_key_value            = CASE WHEN tbl = 't_flow_set' AND field = 'object_key_value' AND @dflt_object_key_value IS NULL THEN def ELSE @dflt_object_key_value END,
        @dflt_flow_type                   = CASE WHEN tbl = 't_flow_set' AND field = 'flow_type' AND @dflt_flow_type IS NULL THEN def ELSE @dflt_flow_type END,
        @dflt_flow_set_id                 = CASE WHEN tbl = 't_flow_set' AND field = 'flow_set_id' AND @dflt_flow_set_id IS NULL THEN def ELSE @dflt_flow_set_id END,
        @dflt_scenario_id                 = CASE WHEN tbl = 't_flow_set' AND field = 'scenario_id' AND @dflt_scenario_id IS NULL THEN def ELSE @dflt_scenario_id END,
        @dflt_amount_type                 = CASE WHEN tbl = 't_flow_set' AND field = 'amount_type' AND @dflt_amount_type IS NULL THEN def ELSE @dflt_amount_type END,
        @dflt_calculation_date            = CASE WHEN tbl = 't_flow_set' AND field = 'calculation_date' AND @dflt_calculation_date IS NULL THEN def ELSE @dflt_calculation_date END,
        @dflt_flow_date                   = CASE WHEN tbl = 't_flow' AND field = 'flow_date' AND @dflt_flow_date IS NULL THEN def ELSE @dflt_flow_date END,
        @dflt_currency                    = CASE WHEN tbl = 't_flow' AND field = 'flow_currency' AND @dflt_currency IS NULL THEN def ELSE @dflt_currency END,
        @dflt_amount                      = CASE WHEN tbl = 't_flow' AND field = 'flow_amount' AND @dflt_amount IS NULL THEN def ELSE @dflt_amount END,
        @dflt_flow_set_char_cust_element1 = CASE WHEN tbl = 't_flow_set' AND field = 'char_cust_element1' AND @dflt_flow_set_char_cust_element1 IS NULL THEN def ELSE @dflt_flow_set_char_cust_element1 END,
        @dflt_flow_set_char_cust_element2 = CASE WHEN tbl = 't_flow_set' AND field = 'char_cust_element2' AND @dflt_flow_set_char_cust_element2 IS NULL THEN def ELSE @dflt_flow_set_char_cust_element2 END,
        @dflt_flow_set_char_cust_element3 = CASE WHEN tbl = 't_flow_set' AND field = 'char_cust_element3' AND @dflt_flow_set_char_cust_element3 IS NULL THEN def ELSE @dflt_flow_set_char_cust_element3 END,
        @dflt_flow_set_char_cust_element4 = CASE WHEN tbl = 't_flow_set' AND field = 'char_cust_element4' AND @dflt_flow_set_char_cust_element4 IS NULL THEN def ELSE @dflt_flow_set_char_cust_element4 END,
        @dflt_flow_set_char_cust_element5 = CASE WHEN tbl = 't_flow_set' AND field = 'char_cust_element5' AND @dflt_flow_set_char_cust_element5 IS NULL THEN def ELSE @dflt_flow_set_char_cust_element5 END,
        @dflt_flow_set_num_cust_element1  = CASE WHEN tbl = 't_flow_set' AND field = 'num_cust_element1' AND @dflt_flow_set_num_cust_element1 IS NULL THEN def ELSE @dflt_flow_set_num_cust_element1 END,
        @dflt_flow_set_num_cust_element2  = CASE WHEN tbl = 't_flow_set' AND field = 'num_cust_element2' AND @dflt_flow_set_num_cust_element2 IS NULL THEN def ELSE @dflt_flow_set_num_cust_element2 END,
        @dflt_flow_set_num_cust_element3  = CASE WHEN tbl = 't_flow_set' AND field = 'num_cust_element3' AND @dflt_flow_set_num_cust_element3 IS NULL THEN def ELSE @dflt_flow_set_num_cust_element3 END,
        @dflt_flow_set_num_cust_element4  = CASE WHEN tbl = 't_flow_set' AND field = 'num_cust_element4' AND @dflt_flow_set_num_cust_element4 IS NULL THEN def ELSE @dflt_flow_set_num_cust_element4 END,
        @dflt_flow_set_num_cust_element5  = CASE WHEN tbl = 't_flow_set' AND field = 'num_cust_element5' AND @dflt_flow_set_num_cust_element5 IS NULL THEN def ELSE @dflt_flow_set_num_cust_element5 END
 FROM   #field_defaults

  SELECT @error = @@error
  IF @error <> 0
    BEGIN
      RAISERROR(640559, 16, 1, @proc_name)
      RETURN (1)
    END

-- ****************************************************
-- Finished retrieval of @dflt when missing
-- Start processing
-- ****************************************************
BEGIN TRANSACTION

-- p_etl_adj_data_flow_before_validate: create a procedure with this name to insert custom logic here...
IF EXISTS (SELECT * FROM sysobjects WHERE name = N'p_etl_adj_data_flow_before_validate')
BEGIN
    EXEC @status = sp_executesql N'EXEC p_etl_adj_data_flow_before_validate @adjustment_id = @adjustment_id, @execution_date = @execution_date',N'@execution_date DATETIME',@execution_date
    IF @status <> 0
    BEGIN
        PRINT N'Halted processing in enrichment procedure p_etl_adj_data_flow_before_validate'
        ROLLBACK TRANSACTION
        RETURN(1)
    END
END
-- ****************************************************
-- Validate and attempt to enrich the fields in the staging table.
-- Based on entries or defaults from @dflt_xxxx
-- ****************************************************
UPDATE upd
   SET entity = CASE WHEN entity IS NULL AND @dflt_entity IS NOT NULL THEN @dflt_entity ELSE entity END,
       object_origin = CASE WHEN object_origin IS NULL AND @dflt_object_origin IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_object_origin ELSE object_origin END,
       object_key_type = CASE WHEN object_key_type IS NULL AND @dflt_object_key_type IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_object_key_type ELSE object_key_type END,
       object_key_value = CASE WHEN object_key_value IS NULL AND @dflt_object_key_value IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_object_key_value ELSE object_key_value END,
       flow_type = CASE WHEN flow_type IS NULL AND @dflt_flow_type IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_type ELSE flow_type END,
       flow_set_id = CASE WHEN flow_set_id IS NULL AND @dflt_flow_set_id IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_id ELSE flow_set_id END,
       scenario_id = CASE WHEN scenario_id IS NULL AND @dflt_scenario_id IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_scenario_id ELSE scenario_id END,
       amount_type = CASE WHEN amount_type IS NULL AND @dflt_amount_type IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_amount_type ELSE amount_type END,
       calculation_date = CASE WHEN calculation_date IS NULL AND @dflt_calculation_date IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_calculation_date ELSE calculation_date END,
       flow_date = CASE WHEN flow_date IS NULL AND @dflt_flow_date IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_date ELSE flow_date END,
       currency = CASE WHEN currency IS NULL AND @dflt_currency IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_currency ELSE currency END,
       amount = CASE WHEN amount IS NULL AND @dflt_amount IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_amount ELSE amount END,
       flow_set_char_cust_element1 = CASE WHEN flow_set_char_cust_element1 IS NULL AND @dflt_flow_set_char_cust_element1 IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_char_cust_element1 ELSE flow_set_char_cust_element1 END,
       flow_set_char_cust_element2 = CASE WHEN flow_set_char_cust_element2 IS NULL AND @dflt_flow_set_char_cust_element2 IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_char_cust_element2 ELSE flow_set_char_cust_element2 END,
       flow_set_char_cust_element3 = CASE WHEN flow_set_char_cust_element3 IS NULL AND @dflt_flow_set_char_cust_element3 IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_char_cust_element3 ELSE flow_set_char_cust_element3 END,
       flow_set_char_cust_element4 = CASE WHEN flow_set_char_cust_element4 IS NULL AND @dflt_flow_set_char_cust_element4 IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_char_cust_element4 ELSE flow_set_char_cust_element4 END,
       flow_set_char_cust_element5 = CASE WHEN flow_set_char_cust_element5 IS NULL AND @dflt_flow_set_char_cust_element5 IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_char_cust_element5 ELSE flow_set_char_cust_element5 END,
       flow_set_num_cust_element1 = CASE WHEN flow_set_num_cust_element1 IS NULL AND @dflt_flow_set_num_cust_element1 IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_num_cust_element1 ELSE flow_set_num_cust_element1 END,
       flow_set_num_cust_element2 = CASE WHEN flow_set_num_cust_element2 IS NULL AND @dflt_flow_set_num_cust_element2 IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_num_cust_element2 ELSE flow_set_num_cust_element2 END,
       flow_set_num_cust_element3 = CASE WHEN flow_set_num_cust_element3 IS NULL AND @dflt_flow_set_num_cust_element3 IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_num_cust_element3 ELSE flow_set_num_cust_element3 END,
       flow_set_num_cust_element4 = CASE WHEN flow_set_num_cust_element4 IS NULL AND @dflt_flow_set_num_cust_element4 IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_num_cust_element4 ELSE flow_set_num_cust_element4 END,
       flow_set_num_cust_element5 = CASE WHEN flow_set_num_cust_element5 IS NULL AND @dflt_flow_set_num_cust_element5 IS NOT NULL AND (entity = @entity OR @entity IS NULL) THEN @dflt_flow_set_num_cust_element5 ELSE flow_set_num_cust_element5 END
  FROM t_etl_adj_data_flow_set_flow upd
 WHERE (adjustment_id = @adjustment_id OR @adjustment_id IS NULL)

SELECT @error = @@error
IF @error <> 0
  BEGIN
    ROLLBACK TRANSACTION
    RAISERROR(622014, 16, 1, @proc_name, 't_etl_adj_data_flow_set_flow', @error)
    RETURN (@error)
  END

-- ****************************************************
-- determine flows that have changes:
-- ****************************************************

-- simple changes
INSERT INTO #changes
    (flow_set_id, object_origin, object_key_type, object_key_value, entity, flow_type, scenario_id, amount_type, currency, etl_start_validity_date)
SELECT lf.flow_set_id AS flow_set_id,
       ISNULL(elf.object_origin, lf.object_origin) AS object_origin,
       ISNULL(elf.object_key_type, lf.object_key_type) AS object_key_type,
       ISNULL(elf.object_key_value, lf.object_key_value) AS object_key_value,
       ISNULL(elf.entity, lf.entity) AS entity,
       ISNULL(elf.flow_type, lf.flow_type) AS flow_type,
       ISNULL(elf.scenario_id, lf.scenario_id) AS scenario_id,
       ISNULL(elf.amount_type, lf.amount_type) AS amount_type,
       ISNULL(elf.currency, lf.flow_currency) AS currency,
       ISNULL(MIN(elf.start_validity_date), @min_date) AS etl_start_validity_date
  FROM t_etl_adj_data_flow_set_flow elf
  JOIN v_flow lf
    ON elf.object_origin            = lf.object_origin
   AND elf.object_key_type          = lf.object_key_type
   AND elf.object_key_value         = lf.object_key_value
   AND elf.entity                   = lf.entity
   AND ((elf.scenario_id            = lf.scenario_id) OR (elf.scenario_id IS NULL AND lf.scenario_id IS NULL))
   AND elf.flow_type                = lf.flow_type
   AND elf.amount_type              = lf.amount_type
   AND lf.end_validity_date         = @max_date
   AND lf.start_validity_date      <= ISNULL(@execution_date, elf.start_validity_date)
   AND lf.flow_date                 = elf.flow_date
   AND lf.flow_currency                  = elf.currency
 WHERE (elf.entity = @entity OR @entity IS NULL)
   AND (elf.adjustment_id = @adjustment_id OR @adjustment_id IS NULL)
   AND (ISNULL(elf.line_status, N'0') = N'0' OR elf.line_status = N'-1')
   AND (elf.amount                     <>  lf.flow_amount
    OR elf.calculation_date            <>  lf.calculation_date
    OR elf.flow_set_char_cust_element1 <>  lf.flow_set_char_cust_element1
    OR elf.flow_set_char_cust_element2 <>  lf.flow_set_char_cust_element2
    OR elf.flow_set_char_cust_element3 <>  lf.flow_set_char_cust_element3
    OR elf.flow_set_char_cust_element4 <>  lf.flow_set_char_cust_element4
    OR elf.flow_set_char_cust_element5 <>  lf.flow_set_char_cust_element5
    OR elf.flow_set_num_cust_element1  <>  lf.flow_set_num_cust_element1
    OR elf.flow_set_num_cust_element2  <>  lf.flow_set_num_cust_element2
    OR elf.flow_set_num_cust_element3  <>  lf.flow_set_num_cust_element3
    OR elf.flow_set_num_cust_element4  <>  lf.flow_set_num_cust_element4
    OR elf.flow_set_num_cust_element5  <>  lf.flow_set_num_cust_element5
    -- delta load delete is a change even though data on the etl record will probably be all the same
    OR (@use_delta_load = 1 AND elf.flag = N'D'))
 GROUP BY lf.flow_set_id,
       ISNULL(elf.object_origin, lf.object_origin),
       ISNULL(elf.object_key_type, lf.object_key_type),
       ISNULL(elf.object_key_value, lf.object_key_value),
       ISNULL(elf.entity, lf.entity),
       ISNULL(elf.flow_type, lf.flow_type),
       ISNULL(elf.scenario_id, lf.scenario_id),
       ISNULL(elf.amount_type, lf.amount_type),
       ISNULL(elf.currency, lf.flow_currency)
       
SELECT @error = @@error, @rowcount = @@rowcount

  IF @error <> 0
    BEGIN
    -- Failure fetching data, so quit procedure.
      ROLLBACK TRANSACTION
      RAISERROR(640007, 16, 1, @proc_name, N'#changes')
      RETURN (1)
    END

-- Create Index on #changes 1
CREATE CLUSTERED INDEX idx1_changed ON #changes(flow_set_id)
SELECT @error = @@error

IF @error <> 0
  BEGIN
    ROLLBACK TRANSACTION
    -- Failure creating index, so quit procedure.
    RETURN (1)
  END

-- flows (flow_date) non existent in live table
INSERT INTO #changes
    (flow_set_id, object_origin, object_key_type, object_key_value, entity, flow_type, scenario_id, amount_type, currency, etl_start_validity_date)
SELECT lf.flow_set_id, --possibly NULL!
       ISNULL(elf.object_origin, lf.object_origin) AS object_origin,
       ISNULL(elf.object_key_type, lf.object_key_type) AS object_key_type,
       ISNULL(elf.object_key_value, lf.object_key_value) AS object_key_value,
       ISNULL(elf.entity, lf.entity) AS entity,
       ISNULL(elf.flow_type, lf.flow_type) AS flow_type,
       ISNULL(elf.scenario_id, lf.scenario_id) AS scenario_id,
       ISNULL(elf.amount_type, lf.amount_type) AS amount_type,
       ISNULL(elf.currency, lf.flow_currency) AS currency,
       ISNULL(MIN(elf.start_validity_date), @min_date) AS etl_start_validity_date
  FROM t_etl_adj_data_flow_set_flow elf
  LEFT JOIN v_flow lf
    ON elf.object_origin            = lf.object_origin
   AND elf.object_key_type          = lf.object_key_type
   AND elf.object_key_value         = lf.object_key_value
   AND elf.entity                   = lf.entity
   AND elf.flow_type                = lf.flow_type
   AND ((elf.scenario_id            = lf.scenario_id) OR (elf.scenario_id IS NULL AND lf.scenario_id IS NULL))
   AND elf.amount_type              = lf.amount_type
   AND elf.currency                 = lf.flow_currency
   AND lf.end_validity_date         = @max_date
   AND lf.start_validity_date       <= ISNULL(@execution_date, elf.start_validity_date)
  LEFT JOIN #changes c
    ON lf.flow_set_id = c.flow_set_id
 WHERE NOT EXISTS (SELECT *
                     FROM v_flow v
                    WHERE elf.object_origin            = v.object_origin
                      AND elf.object_key_type          = v.object_key_type
                      AND elf.object_key_value         = v.object_key_value
                      AND elf.entity                   = v.entity
                      AND elf.flow_type                = v.flow_type
                      AND ((elf.scenario_id            = v.scenario_id) OR (elf.scenario_id IS NULL AND v.scenario_id IS NULL))
                      AND elf.amount_type              = v.amount_type
                      AND elf.currency                 = v.flow_currency 
                      AND elf.flow_date                = v.flow_date
                      AND v.end_validity_date          = @max_date
                      AND v.start_validity_date        <= ISNULL(@execution_date, elf.start_validity_date)
                      )
   AND c.flow_set_id IS NULL
   AND (elf.entity = @entity OR @entity IS NULL)
   AND (elf.adjustment_id = @adjustment_id OR @adjustment_id IS NULL)
   AND (ISNULL(elf.line_status, N'0') = N'0' OR elf.line_status = N'-1')
 GROUP BY lf.flow_set_id,
       ISNULL(elf.object_origin, lf.object_origin),
       ISNULL(elf.object_key_type, lf.object_key_type),
       ISNULL(elf.object_key_value, lf.object_key_value),
       ISNULL(elf.entity, lf.entity),
       ISNULL(elf.flow_type, lf.flow_type),
       ISNULL(elf.scenario_id, lf.scenario_id),
       ISNULL(elf.amount_type, lf.amount_type),
       ISNULL(elf.currency, lf.flow_currency)

SELECT @error = @@error, @rowcount = @rowcount + @@rowcount

  IF @error <> 0
    BEGIN
    -- Failure fetching data, so quit procedure.
      ROLLBACK TRANSACTION
      RAISERROR(640007, 16, 1, @proc_name, N'#changes')
      RETURN (1)
    END

-- flows (flow_date) non existent in etl table
INSERT INTO #changes
    (flow_set_id, object_origin, object_key_type, object_key_value, entity, flow_type, scenario_id, amount_type, currency, etl_start_validity_date)
SELECT lf.flow_set_id,
       lf.object_origin,
       lf.object_key_type,
       lf.object_key_value,
       lf.entity,
       lf.flow_type,
       lf.scenario_id,
       lf.amount_type,
       lf.flow_currency,
       ISNULL(MIN(elf.start_validity_date), @min_date)
  FROM v_flow lf
  JOIN t_etl_adj_data_flow_set_flow elf
    ON elf.object_origin            = lf.object_origin
   AND elf.object_key_type          = lf.object_key_type
   AND elf.object_key_value         = lf.object_key_value
   AND elf.entity                   = lf.entity
   AND elf.flow_type                = lf.flow_type
   AND ((elf.scenario_id            = lf.scenario_id) OR (elf.scenario_id IS NULL AND lf.scenario_id IS NULL))
   AND elf.amount_type              = lf.amount_type
   AND (elf.entity = @entity OR @entity IS NULL)
   AND (elf.adjustment_id = @adjustment_id OR @adjustment_id IS NULL)
   AND (ISNULL(elf.line_status, N'0') = N'0' OR elf.line_status = N'-1')
  LEFT JOIN #changes c
    ON lf.flow_set_id = c.flow_set_id
 WHERE lf.end_validity_date         = @max_date
   AND lf.start_validity_date      <= ISNULL(@execution_date, elf.start_validity_date)
   AND NOT EXISTS (SELECT *
                     FROM t_etl_adj_data_flow_set_flow etl
                    WHERE etl.object_origin            = lf.object_origin
                      AND etl.object_key_type          = lf.object_key_type
                      AND etl.object_key_value         = lf.object_key_value
                      AND etl.entity                   = lf.entity
                      AND etl.flow_type                = lf.flow_type
                      AND ((etl.scenario_id            = lf.scenario_id) OR (etl.scenario_id IS NULL AND lf.scenario_id IS NULL))
                      AND etl.amount_type              = lf.amount_type
                      AND etl.flow_date                = lf.flow_date
                      AND etl.currency                 = lf.flow_currency
                      AND (etl.entity = @entity OR @entity IS NULL)
                      AND (etl.adjustment_id = @adjustment_id OR @adjustment_id IS NULL)
                      AND (ISNULL(etl.line_status, N'0') = N'0' OR etl.line_status = N'-1')
                   )
  AND c.flow_set_id IS NULL
 GROUP BY lf.flow_set_id,
       lf.object_origin,
       lf.object_key_type,
       lf.object_key_value,
       lf.entity,
       lf.flow_type,
       lf.scenario_id,
       lf.amount_type,
       lf.flow_currency

SELECT @error = @@error, @rowcount = @rowcount + @@rowcount

  IF @error <> 0
    BEGIN
    -- Failure fetching data, so quit procedure.
      ROLLBACK TRANSACTION
      RAISERROR(640007, 16, 1, @proc_name, N'#changes')
      RETURN (1)
    END

-- only exit procedure when there is no cleanup requested
IF @rowcount = 0 and @cleanup_type = 0
  BEGIN
    PRINT N'Nothing to process.'
    ROLLBACK TRANSACTION
    -- Nothing to process, so quit the procedure.
    RETURN (0)
  END

-- ****************************************************
-- * Cleanup in case of a rerun.
-- ****************************************************
-- cleanup (1)
IF EXISTS(SELECT *
            FROM t_flow_set tfs
       LEFT JOIN t_flow tf
              ON tf.flow_set_id = tfs.flow_set_id
       LEFT JOIN #changes c
              ON c.flow_set_id = tfs.flow_set_id
           WHERE tfs.start_validity_date >= COALESCE(@execution_date, c.etl_start_validity_date, @max_date)
            --if no record in #changes (c.etl_start_validity_date will be NULL), no DELETE will happen, so use @max_date as condition to rule out these records
             AND (tfs.entity = @entity OR @entity IS NULL)
             AND (@use_delta_load = 0 OR c.flow_set_id IS NOT NULL))
  BEGIN
    -- only clean up when cleanup type = 1
    IF @cleanup_type = 0  -- 2
      BEGIN
        ROLLBACK TRANSACTION
        RAISERROR (52898, 16, 1, @proc_name, N't_flow')
        RETURN (1)
      END
    ELSE
      BEGIN
        -- ************************************************
        -- only clean up records that are in the new batch.
        -- @only_cleanup_changed = 1 OR @use_delta_load = 1
        -- (in case a "fix" batch has been delivered)
        --
        -- Otherwise cleanup all the delivered data, 
        -- if @execution_date is entered and can be used as a reference date 
        -- (no other reference available at this point)
        -- ************************************************
        IF @only_cleanup_changed = 0 AND @use_delta_load = 0 AND @execution_date IS NOT NULL
        BEGIN
            -- full cleanup of the flows.
            DELETE t_flow
              FROM t_flow tf
              JOIN t_flow_set tfs
                ON tfs.flow_set_id = tf.flow_set_id
             WHERE tfs.start_validity_date >= @execution_date
               AND (tfs.entity = @entity OR @entity IS NULL)

            SELECT @error = @@error, @rowcount = @@rowcount

            IF @error <> 0
              BEGIN
                -- Failure deleting data, so quit procedure.
                ROLLBACK TRANSACTION
                RETURN (@error)
              END
            IF @rowcount = 0
              BEGIN
                ROLLBACK TRANSACTION
                RAISERROR(N'There was Flow data to cleanup detected, but nothing got actually deleted. (1)', 16, 1)
                RETURN (1)
              END

            INSERT INTO @log([Action], Nbr_Of_Rows_Affected)
            VALUES(N'Intraday Run: Full Delete Cash Flows.', @rowcount)

            -- full cleanup of the flow sets.
            DELETE t_flow_set
              FROM t_flow_set tfs
             WHERE tfs.start_validity_date >= @execution_date
               AND (tfs.entity = @entity OR @entity IS NULL)

            SELECT @error = @@error, @rowcount = @@rowcount

            IF @error <> 0
              BEGIN
                -- Failure deleting data, so quit procedure.
                ROLLBACK TRANSACTION
                RETURN (@error)
              END
            IF @rowcount = 0
              BEGIN
                ROLLBACK TRANSACTION
                RAISERROR(N'There was Flow Set data to cleanup detected, but nothing got actually deleted.', 16, 1)
                RETURN (1)
              END

            INSERT INTO @log([Action], Nbr_Of_Rows_Affected)
            VALUES(N'Intraday Run: Full delete of the Cash Flow Sets.', @rowcount)

            -- set end_validity_date of finalized flow set records to 9991231
            UPDATE t_flow_set
               SET end_validity_date = @max_date
              FROM t_flow_set tf
             WHERE tf.end_validity_date <> @max_date
               AND tf.end_validity_date >= @execution_date - @datediff
               AND (tf.entity = @entity OR @entity IS NULL)

            SELECT @error = @@error, @rowcount = @@rowcount

            IF @error <> 0
              BEGIN
                -- Failure fixing end_validity_date, so quit procedure.
                ROLLBACK TRANSACTION
                RETURN (@error)
              END

            INSERT INTO @log([Action], Nbr_Of_Rows_Affected)
            VALUES(N'Intraday Run: Update end_validity_date of the deleted Cash Flow Sets.', @rowcount)

        END 
        ELSE --IF @only_cleanup_changed = 0 AND @use_delta_load = 0 AND @execution_date IS NOT NULL
        BEGIN
            --delta cleanup
            DELETE t_flow
              FROM t_flow tf
              JOIN t_flow_set tfs
                ON tfs.flow_set_id = tf.flow_set_id
              JOIN #changes c
                ON tfs.flow_set_id = c.flow_set_id
             WHERE tfs.start_validity_date >= ISNULL(@execution_date, c.etl_start_validity_date)

            SELECT @error = @@error, @rowcount = @@rowcount

            IF @error <> 0
              BEGIN
                -- Failure deleting data, so quit procedure.
                ROLLBACK TRANSACTION
                RETURN (@error)
              END
            IF @rowcount = 0 AND @only_cleanup_changed = 0
              BEGIN
                ROLLBACK TRANSACTION
                RAISERROR(N'There was Flow data to cleanup detected, but nothing got actually deleted. (2)', 16, 1)
                RETURN (1)
              END

            INSERT INTO @log([Action], Nbr_Of_Rows_Affected)
            VALUES(N'Intraday Run: Delta delete of the Cash Flows.', @rowcount)

           -- full cleanup of the flow sets.
            DELETE t_flow_set
              FROM t_flow_set tfs
              JOIN #changes c
                ON tfs.flow_set_id = c.flow_set_id
             WHERE tfs.start_validity_date >= ISNULL(@execution_date, c.etl_start_validity_date)

            SELECT @error = @@error, @rowcount = @@rowcount

            IF @error <> 0
              BEGIN
                -- Failure deleting data, so quit procedure.
                ROLLBACK TRANSACTION
                RETURN (@error)
              END
            IF @rowcount = 0 AND @only_cleanup_changed = 0
              BEGIN
                ROLLBACK TRANSACTION
                RAISERROR(N'There was Flow Set data to cleanup detected, but nothing got actually deleted.', 16, 1)
                RETURN (1)
              END

            INSERT INTO @log([Action], Nbr_Of_Rows_Affected)
            VALUES(N'Intraday Run: Delta delete of the Cash Flow Sets.', @rowcount)

            -- set end_validity_date of finalized flow set records to 99991231
            UPDATE t_flow_set
               SET end_validity_date = @max_date
              FROM t_flow_set tf
              JOIN #changes c
                ON tf.flow_set_id = c.flow_set_id
             WHERE tf.end_validity_date <> @max_date
               AND tf.end_validity_date >= ISNULL(@execution_date, c.etl_start_validity_date) - @datediff

            SELECT @error = @@error, @rowcount = @@rowcount

            IF @error <> 0
              BEGIN
                -- Failure fixing end_validity_date, so quit procedure.
                ROLLBACK TRANSACTION
                RETURN (@error)
              END
          END
      END
  END

-- ****************************************************
-- * Exit procedure when there is nothing to insert
-- ****************************************************
IF NOT EXISTS(SELECT * from #changes)
  BEGIN
    COMMIT TRANSACTION
    RETURN(0)
  END

-- ****************************************************
-- double use of the cleanup variable to avoid that all non-delivered records are set inactive (the data supplied is not always the complete set)
-- ****************************************************
DELETE FROM #changes
WHERE (object_origin IS NULL OR object_key_type IS NULL OR object_key_value IS NULL)  
    AND (@only_cleanup_changed = 1 OR
       @use_delta_load = 1 OR
       @execution_date IS NULL)
SELECT @error = @@error, @rowcount = @@rowcount
IF @error <> 0
  BEGIN
    ROLLBACK TRANSACTION
    PRINT N'Failure cleaning #changes, so quit procedure.'
    RETURN (@error)
  END

-- ****************************************************
-- Set end validity_date of existing flow sets
-- end validity date is set to previous cob_date
    -- in case of @use_delta_load = 0 (non delta-load): old records (not present) need to be finished: end current existing sets that have not been delivered with the new set
-- ****************************************************
    IF @use_delta_load = 0 AND @execution_date IS NOT NULL --Otherwise it is not possible to define on what date flows to close are based
        BEGIN
            UPDATE t_flow_set
               SET end_validity_date = @execution_date - @datediff
              FROM t_flow_set lf
             WHERE (lf.entity = @entity OR @entity IS NULL)
               AND lf.end_validity_date >= @execution_date - @datediff
               AND NOT EXISTS (SELECT * FROM #changes c
                                WHERE lf.flow_set_id = c.flow_set_id)

            SELECT @error = @@error, @rowcount = @@rowcount
            IF @error <> 0
                BEGIN
                    ROLLBACK TRANSACTION
                    -- Failure updating end validity, so quit procedure.
                    RETURN (@error)
                END

             INSERT INTO @log([Action], Nbr_Of_Rows_Affected)
             VALUES(N'Not re-entered Cash flows are closed: Update of end_validity_date.', @rowcount)
        END

UPDATE t_flow_set
   SET end_validity_date = ISNULL(@execution_date, c.etl_start_validity_date) - @datediff
  FROM t_flow_set lf
  JOIN #changes c
    ON lf.flow_set_id = c.flow_set_id
 WHERE (lf.entity = @entity OR @entity IS NULL)
-- always need to do the above:
--   * in case of non delta-load only inserts/updates, old record needs to be finished, new record will be inserted later on
--   * in case of delta-load update/insert, old record needs to be ended, new record will be inserted later on
--   * in case of delta-load delete: no actual delete is executed instead the old record is 'just' set to finished

SELECT @error = @@error, @rowcount = @@rowcount
IF @error <> 0
  BEGIN
    ROLLBACK TRANSACTION
    -- Failure updating end validity, so quit procedure.
    RETURN (@error)
  END

INSERT INTO @log([Action], Nbr_Of_Rows_Affected)
VALUES(N'Changed Cash flows: Update of end_validity_date.', @rowcount)

select * from #changes
-- ****************************************************
-- * Create new flow sets.
-- ****************************************************
INSERT INTO t_flow_set
      (object_origin, object_key_type, object_key_value, entity, flow_type, scenario_id, amount_type, start_validity_date, end_validity_date, calculation_date, char_cust_element1, char_cust_element2, char_cust_element3, char_cust_element4, char_cust_element5, num_cust_element1, num_cust_element2, num_cust_element3, num_cust_element4, num_cust_element5, last_modified, modified_by)
select distinct
	   y.object_origin,
       y.object_key_type,
       y.object_key_value,
       y.entity,
       y.flow_type,
       y.scenario_id,
       y.amount_type,
       y.start_validity_date,
       end_validity_date = ISNULL(y.cal_start_date - @datediff, @max_date),
       y.calculation_date,
       y.flow_set_char_cust_element1,
       y.flow_set_char_cust_element2,
       y.flow_set_char_cust_element3,
       y.flow_set_char_cust_element4,
       y.flow_set_char_cust_element5,
       y.flow_set_num_cust_element1,
       y.flow_set_num_cust_element2,
       y.flow_set_num_cust_element3,
       y.flow_set_num_cust_element4,
       y.flow_set_num_cust_element5,
	   CURRENT_TIMESTAMP,
       @user
from (
SELECT 
       c.object_origin,
       c.object_key_type,
       c.object_key_value,
       c.entity,
       c.flow_type,
       c.scenario_id,
       c.amount_type,
       start_validity_date = COALESCE(@execution_date, fsf.start_validity_date, @min_date),
	   rank() over (partition by x.object_origin, x.object_key_type, x.object_key_value, x.entity, x.flow_type, x.scenario_id, x.amount_type, x.adjustment_id, fsf.start_validity_date
					order by x.start_validity_date) as date_rank,
	   x.start_validity_date as cal_start_date,
       fsf.calculation_date,
       fsf.flow_set_char_cust_element1,
       fsf.flow_set_char_cust_element2,
       fsf.flow_set_char_cust_element3,
       fsf.flow_set_char_cust_element4,
       fsf.flow_set_char_cust_element5,
       fsf.flow_set_num_cust_element1,
       fsf.flow_set_num_cust_element2,
       fsf.flow_set_num_cust_element3,
       fsf.flow_set_num_cust_element4,
       fsf.flow_set_num_cust_element5
  FROM #changes c
  JOIN t_etl_adj_data_flow_set_flow fsf
    ON fsf.object_origin          = c.object_origin
   AND fsf.object_key_type        = c.object_key_type
   AND fsf.object_key_value       = c.object_key_value
   AND fsf.entity                 = c.entity
   AND fsf.flow_type              = c.flow_type
   AND ((fsf.scenario_id          = c.scenario_id) OR (fsf.scenario_id IS NULL AND c.scenario_id IS NULL))
   AND fsf.amount_type            = c.amount_type
   AND (fsf.adjustment_id         = @adjustment_id OR @adjustment_id IS NULL)
   AND (ISNULL(fsf.line_status, N'0') = N'0' OR fsf.line_status = N'-1')
  left join t_etl_adj_data_flow_set_flow x 
  on x.object_origin              = fsf.object_origin 
	AND x.object_key_type         = fsf.object_key_type 
	AND x.object_key_value        = fsf.object_key_value 
	AND x.entity                  = fsf.entity 
	AND x.flow_type               = fsf.flow_type 
	AND ((x.scenario_id           = fsf.scenario_id) OR (x.scenario_id IS NULL AND fsf.scenario_id IS NULL))
	AND x.amount_type             = fsf.amount_type 
	AND x.start_validity_date     > fsf.start_validity_date 
	AND x.adjustment_id           = fsf.adjustment_id 
	AND (ISNULL(x.line_status, N'0') = N'0' OR x.line_status = N'-1')
 WHERE (@use_delta_load = 0 OR fsf.flag IN (N'I',N'U'))
 ) y
 where y.date_rank = 1

SELECT @error = @@error, @rowcount = @@rowcount
IF @error <> 0
  BEGIN
    ROLLBACK TRANSACTION
    -- Failure creating new flow sets, so quit procedure.
    RETURN (@error)
  END

INSERT INTO @log([Action], Nbr_Of_Rows_Affected)
VALUES(N'Insert of new Cash Flow Sets.', @rowcount)

SET @msg_to_add_to_event_log = N'Insert into t_flow of ' + CONVERT(NVARCHAR(MAX), @rowcount) + N' new Cash Flow Sets.'
EXEC p_ale_log_event @event_name      = N'HMSInsert',
                     @message         = @msg_to_add_to_event_log,
                     @context_value   = N'',
                     @event_log_id    = @event_log_id OUTPUT

-- ****************************************************
-- * Create Cash flows.
-- ****************************************************
INSERT INTO t_flow
   (flow_set_id, flow_date, flow_currency, flow_amount, last_modified, modified_by)
SELECT DISTINCT fs.flow_set_id, fsf.flow_date, fsf.currency, fsf.amount, CURRENT_TIMESTAMP , @user
  FROM #changes c
  JOIN t_etl_adj_data_flow_set_flow fsf
    ON fsf.object_origin          = c.object_origin
   AND fsf.object_key_type        = c.object_key_type
   AND fsf.object_key_value       = c.object_key_value
   AND fsf.entity                 = c.entity
   AND fsf.flow_type              = c.flow_type
   AND ((fsf.scenario_id          = c.scenario_id) OR (fsf.scenario_id IS NULL AND c.scenario_id IS NULL))
   AND fsf.amount_type            = c.amount_type
   AND (fsf.adjustment_id         = @adjustment_id OR @adjustment_id IS NULL)
   AND (ISNULL(fsf.line_status,  N'0') = N'0' OR fsf.line_status = N'-1')
  JOIN t_flow_set fs
    ON c.object_origin            = fs.object_origin
   AND c.object_key_type          = fs.object_key_type
   AND c.object_key_value         = fs.object_key_value
   AND c.entity                   = fs.entity
   AND c.flow_type                = fs.flow_type
   AND ((c.scenario_id            = fs.scenario_id) OR (c.scenario_id IS NULL AND fs.scenario_id IS NULL))
   AND c.amount_type              = fs.amount_type
   AND fs.start_validity_date     >= ISNULL(@execution_date, c.etl_start_validity_date)
WHERE (@use_delta_load = 0 OR fsf.flag IN (N'I',N'U'))

SELECT @error = @@error, @rowcount = @@rowcount
IF @error <> 0
  BEGIN
    ROLLBACK TRANSACTION
    -- Failure updating end validity, so quit procedure.
    RETURN (@error)
  END

INSERT INTO @log([Action], Nbr_Of_Rows_Affected)
VALUES(N'Insert of new Cash Flows.',@rowcount)

SET @msg_to_add_to_event_log = N'Insert into t_flow of ' + CONVERT(NVARCHAR(MAX), @rowcount) + N' new Cash Flows.'
EXEC p_ale_log_event @event_name      = N'HMSInsert',
                     @message         = @msg_to_add_to_event_log,
                     @context_value   = N'',
                     @event_log_id    = @event_log_id OUTPUT


IF @cleanup_staging = 1
  BEGIN
    -- Cleanup t_etl_adj_data_flow_set_flow
    DELETE dbo.t_etl_adj_data_flow_set_flow
      FROM dbo.t_etl_adj_data_flow_set_flow t
     WHERE (entity = @entity OR @entity IS NULL)
       AND (adjustment_id = @adjustment_id OR @adjustment_id IS NULL)

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- check for error
    IF @error <> 0
      BEGIN
        ROLLBACK TRANSACTION
        RAISERROR (62806, 16, 1, @proc_name, N't_etl_adj_data_flow_set_flow')
        RETURN (@error)
      END

    INSERT INTO @log([Action], Nbr_Of_Rows_Affected)
    VALUES(N'Deletion of copied DTS data.', @rowcount)

  END


COMMIT TRANSACTION

IF @log_output = 1
  BEGIN
    SELECT [Action], Nbr_Of_Rows_Affected
      FROM @log
     --WHERE Nbr_Of_Rows_Affected <> 0
     ORDER BY [id]
  END

RETURN (@status)

GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_etl_adj_data_flow_tbank' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_etl_adj_data_flow_tbank] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_etl_adj_data_flow_tbank] has NOT been altered due to errors!'
END
GO
