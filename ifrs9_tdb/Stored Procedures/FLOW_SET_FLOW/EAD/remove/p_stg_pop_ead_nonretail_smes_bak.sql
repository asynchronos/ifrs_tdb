
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pop_ead_nonretail_smes' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pop_ead_nonretail_smes] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pop_ead_nonretail_smes] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pop_ead_nonretail_smes]...'
GO

ALTER PROCEDURE [dbo].[p_stg_pop_ead_nonretail_smes] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime	= null,
	@param_is_test			bit			= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	Stored procedure for population t_etl_adj_data_flow_set_flow  *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

	-- variables for t_ecl_process_log_tbank
	declare @log_pid				int;
	declare @log_process_type		varchar(20)			=	'Non-Retail&SMEs';
	declare @log_model_name			varchar(100)		=	'Pop Non-Retail&SMEs EAD';
	declare @log_result_table		varchar(50)			=	't_etl_adj_data_flow_set_flow';

	-- variables for ale
	declare @ale_message			d_ale_message;
	declare @ale_context			xml;
	declare @ale_correlation_guid	UNIQUEIDENTIFIER;
	declare @ale_event_log_guid		UNIQUEIDENTIFIER;
	
-- Declare local variables
	declare @cob_date				datetime;
	declare @max_date				datetime			=	'9999-12-31';
	declare @pool_id_list			nvarchar(100)		=	'(60,61,62,63,64,65)'; -- remove 63 for temp FP because of 1 deal has more acc
	declare @lot_fn_code			d_std_record_function		= N'ECLSEG';

	-- variables for cursor
	declare @table_name				nvarchar(256);
	declare @stage					d_name;
	declare @scenario_id			d_id;
	declare @scenario_id_str		d_name;
	declare @scenario_name			d_name;
	declare @src_field_name			d_name;
	declare @src_flow_type			d_name;

	-- list of constant variable
	declare @adjustment_id			d_id;
	declare @adj_type_id_crv_curve	d_id;
	declare @stg_flag				d_delta_flag		=	N'U';
	declare @stg_line_status		d_row_status		=	N'0';
	declare @stg_retry_count		d_id				=	0;
	declare @stg_entity				d_entity			=	N'TBANK';
	--declare @stg_object_origin		d_object_origin		=	N'LODEP';
	declare @stg_object_key_type	d_object_key_type	=	N'2';
	declare @stg_amount_type		d_amount_type		=	N'CAPITA';
	declare @stg_currency			d_currency			=	N'THB';
	declare @table_type_name		d_name				=	@log_result_table;

	-- assign cob date
	select @cob_date = isnull(@param_reporting_date,cob_date)
	from t_entity
	where entity = 'TBANK';

-- Start actual procedure flow
	
	--add logging here
	set @ale_message = N'Start: ' + @proc_name
						+ N'(@param_reporting_date=' + CONVERT(varchar,@param_reporting_date)
						+ char(13)+char(10)+char(9)+N',@param_is_test=' + CONVERT(varchar,@param_is_test)
						+ char(13)+char(10)+char(9)+N',@cob_date=' + CONVERT(varchar,@cob_date)
						+ N')';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_START', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = @param_is_test,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;
	
	-- print out msg
	IF @param_is_test = 1
	BEGIN
		PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + @ale_message;
	END

	BEGIN TRY

		/*Start Process Log*/
		SET @log_pid = (select isnull(max(process_id),0)+1 
						from t_ecl_process_log_tbank
						where cob_date = @cob_date);

		insert into t_ecl_process_log_tbank (cob_date,process_id,process_type,process_name,model_name,result_table,start_time,process_status)
		select  @cob_date, @log_pid, @log_process_type, @proc_name, @log_model_name, @log_result_table, getdate(),'Processing';
		/*End Process Log*/

		-- config table for cursor looping
		declare p_stg_pop_ead_nonretail_smes_cursor cursor local static for
		-- select target source table name and scenario mapping
		select t.TABLE_NAME, stage, se.scenario_id, se.scenario_name, se.flow_type, se.src_field_name
		from INFORMATION_SCHEMA.TABLES t
			join ( -- create configuration table with inline sql, may move to physical table in the future
				select s.scenario_id, s.scenario_name
					,case
						when CHARINDEX('stage 1',lower(s.scenario_name)) > 0	then	'1'
						when CHARINDEX('stage 2',lower(s.scenario_name)) > 0	then	'2'
						when CHARINDEX('stage 3',lower(s.scenario_name)) > 0	then	'3'
						else 'unknown' end as stage
					,case
						when CHARINDEX('stage 1',lower(s.scenario_name)) > 0	then	'EAD_1'
						when CHARINDEX('stage 2',lower(s.scenario_name)) > 0	then	'EAD_2'
						when CHARINDEX('stage 3',lower(s.scenario_name)) > 0	then	'EAD_3'
						else 'unknown' end as flow_type
					,case
						when CHARINDEX('stage 1',lower(s.scenario_name)) > 0	then	'ead_stage1'
						when CHARINDEX('stage 2',lower(s.scenario_name)) > 0	then	'ead_stage2'
						when CHARINDEX('stage 3',lower(s.scenario_name)) > 0	then	'ead_stage3'
						else 'unknown' end as src_field_name
				from t_scenario s
				where s.scenario_name in ('Stage 1 NON FL','Stage 2 Specific Treatment Group NON FL','Stage 3 Specific Treatment Group')
			) se on 1=1 
		where t.TABLE_NAME in ('t_nonretail_ead_tbank') -- change product table here, may change to dynamic sql for easy config and mantinance in the future
		
		open p_stg_pop_ead_nonretail_smes_cursor
		fetch next from p_stg_pop_ead_nonretail_smes_cursor into @table_name, @stage, @scenario_id, @scenario_name, @src_flow_type, @src_field_name

		-- variable for dynamic t-sql
		declare @nsql nvarchar(max);
		declare @nsql_select nvarchar(50);
		declare @nsql_column nvarchar(max);
		declare @nsql_table nvarchar(max);
		declare @nsql_insert nvarchar(max);
		declare @paramDef nvarchar(max);

		While @@FETCH_STATUS = 0
		BEGIN

			BEGIN TRY -- handle error for continue loop to next source
				--add logging here
				set @ale_message = CONCAT(@table_name,'|',@stage,'|',@scenario_id,'|',@scenario_name,'|',@src_flow_type,'|',@src_field_name);

				EXEC p_ale_log_detail @event_log_guid          = @ale_event_log_guid,
									@event_type                = N'I',
									@message                   = @ale_message,
									--@timestamp_utc             = N'',
									@use_synchronous_logging   = 0,
									@print_ind                 = @param_is_test;
			
				select @nsql_insert = 'INSERT INTO t_etl_adj_data_flow_set_flow
					   (adjustment_id, line_nr, flag, line_status, retry_count, line_date, entity, object_origin, object_key_type, object_key_value, flow_type, flow_set_id, scenario_id, amount_type, start_validity_date, end_validity_date, calculation_date, flow_date, currency, amount, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5, last_modified, modified_by)';
		
				select @nsql_select = case @param_is_test when 0 then ' select ' else 'select top 300 ' end;
		
				select @nsql_column = 
					N' @adjustment_id							as	adjustment_id
					   ,row_number() over(order by src.deal_id)	as	line_nr
					   ,@stg_flag								as	flag
					   ,@stg_line_status						as	line_status
					   ,@stg_retry_count						as	retry_count
					   ,GETDATE()								as	line_date
					   ,@stg_entity								as	entity
					   ,seg.item_object_origin					as	object_origin
					   ,@stg_object_key_type					as	object_key_type
					   ,src.deal_id+''|''+@stg_entity			as	object_key_value
					   ,@src_flow_type							as	flow_type
					   ,null									as	flow_set_id
					   ,'+@stage+'								as	scenario_id
					   ,@stg_amount_type						as	amount_type
					   ,src.as_of_date							as	start_validity_date
					   ,@max_date								as	end_validity_date
					   ,src.as_of_date							as	calculation_date
					   ,eomonth(dateadd(month,case when '+@stage+'<>3 then 1 else 0 end,src.as_of_date)) as	flow_date
					   ,'''+@stg_currency+'''					as	currency
					   ,src.'+@src_field_name+'					as	amount
					   ,src.date_matur							as	flow_set_char_cust_element1
					   ,@scenario_id							as	flow_set_char_cust_element2
					   ,src.segment_name						as	flow_set_char_cust_element3
					   ,CONVERT(NVARCHAR,@cob_date)				as	flow_set_char_cust_element4
					   ,N''''									as	flow_set_char_cust_element5
					   ,CONVERT(numeric,src.segment_id)			as	flow_set_num_cust_element1
					   ,src.remaining_maturity					as	flow_set_num_cust_element2
					   ,src.remaining_term						as	flow_set_num_cust_element3
					   ,'+@stage+'								as	flow_set_num_cust_element4
					   ,0.00									as	flow_set_num_cust_element5
					   ,CURRENT_TIMESTAMP						as	last_modified
					   ,dbo.fn_user()							as	modified_by ';

				select @nsql_table =
				N'from '+@table_name+' src
					join t_ecl_segmentation seg on src.deal_id= seg.deal_tfi_id
					and seg.lot_id = (select max(lot.lot_id) from t_lot_definition lot where lot.std_record_function_code = @lot_fn_code)
					join t_pool p on p.pool_id=src.segment_id
					and p.pool_id in '+@pool_id_list+N'
				where src.deal_id is not null
					and src.as_of_date = @cob_date
					and src.ead_method not in (''GCA'',''NO ECL'')
					and and deal_id not in (''FP001356500179'',''FP001359501124'',''FP001359501221'',''FP001359502065'',''FP001359501166'',''FP001359501182'',''FP001359501205'')';-- temporay filter until FP not duplicate

				-- param definition
				set @paramDef = N'@cob_date datetime
								,@adjustment_id			d_id
								,@stg_flag				d_delta_flag
								,@stg_line_status		d_row_status
								,@stg_retry_count		d_id
								,@stg_entity			d_entity
								,@stg_object_key_type	d_object_key_type
								,@stg_amount_type		d_amount_type
								,@src_flow_type			d_name
								,@scenario_id			d_id
								,@max_date				datetime
								,@lot_fn_code			d_std_record_function
				';-- end prepare dynamic t-sql
				
				-- final t-sql
				set @nsql = @nsql_insert + char(13) + char(10) + @nsql_select + @nsql_column + char(13) + char(10) + @nsql_table;
				-- check final t-sql length
				IF len(@nsql) > 4000
				BEGIN
					set @ale_message = N'SQL string is over than 4000 character.';
					set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql FOR XML PATH);

					EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
								@event_type                = N'E',
								@context                   = @ale_context,
								@message                   = @ale_message,
								@event_type_detail         = N'E',
								--@correlation_guid          = @ale_correlation_guid OUTPUT,
								--@timestamp_utc             = N'',
								--@event_log_guid            = @ale_event_log_guid OUTPUT,
								@use_synchronous_logging   = 0,
								@print_ind                 = @param_is_test,
								@print_level               = 0,
								@calling_proc_name         = @proc_name;

					set @error_msg = @ale_message
					RAISERROR (@error_msg, 16, 1, @proc_name)
				END
				
				BEGIN TRANSACTION;
				BEGIN TRY -- handle error for continue loop to next source

					-- get table type id
					SELECT @adj_type_id_crv_curve   = (SELECT typ.type_id FROM t_adj_type typ JOIN t_adj_category cat ON cat.category_id = typ.category_id WHERE typ.type_name = @table_type_name);
					-- prepare adjustment before load to stg
					set @adjustment_id = null;
					EXEC @status = [dbo].[p_adj_save]
											@adjustment_id = @adjustment_id OUTPUT,
											@type_id = @adj_type_id_crv_curve,
											@reporting_date = @param_reporting_date,
											@comment = @table_name
					;

					set @ale_message = N'Inserting: '+CONCAT(@table_name,'|',@stage,'|',@scenario_id,'|',@scenario_name,'|',@src_flow_type,'|',@src_field_name);

					EXEC p_ale_log_detail @event_log_guid          = @ale_event_log_guid,
										@event_type                = N'I',
										@message                   = @ale_message,
										--@timestamp_utc             = N'',
										@use_synchronous_logging   = 0,
										@print_ind                 = @param_is_test;
					
					RAISERROR(@ale_message,10,1,@proc_name)
					SET @ale_message = CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'@adjustment_id: ' + convert(varchar,@adjustment_id)
					RAISERROR(@ale_message,10,1,@proc_name)
					SET @ale_message = CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Inserting ' + @table_name + ' to ' + @table_type_name
					RAISERROR(@ale_message,10,1,@proc_name)

					IF @param_is_test = 1
					BEGIN
						PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'@adjustment_id: ' + convert(varchar,@adjustment_id)
						PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Inserting ' + @table_name + ' to ' + @table_type_name
					END

					-- execute stored procedure for populate data and load hms of each table with each scenario
					-- insert data with sp_executesql
					exec sp_executesql @nsql ,@paramDef ,@cob_date				=@cob_date
														,@adjustment_id			=@adjustment_id
														,@stg_flag				=@stg_flag
														,@stg_line_status		=@stg_line_status
														,@stg_retry_count		=@stg_retry_count
														,@stg_entity			=@stg_entity
														,@stg_object_key_type	=@stg_object_key_type
														,@stg_amount_type		=@stg_amount_type
														,@src_flow_type			=@src_flow_type
														,@scenario_id			=@scenario_id
														,@max_date				=@max_date
														,@lot_fn_code			=@lot_fn_code
					;

					SET @ale_message = CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + N'Start HMS.'
					RAISERROR(@ale_message,10,1,@proc_name)

					-- run hms
					exec p_etl_adj_data_flow_tbank	@execution_date	= @cob_date, 
													@cleanup_type = 2,
													@cleanup_staging = 0,
													@only_cleanup_changed = 1,
													@use_delta_load = 1, 
													@adjustment_id	= @adjustment_id,
													@entity			= N'TBANK',  
													@log_output     = 0

					COMMIT;
				END TRY
				BEGIN CATCH  -- rollback when failed inserting or hms
					IF @@TRANCOUNT > 0 ROLLBACK;

					--add logging here
					set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').';
					set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql, @paramDef as paramDef FOR XML PATH);

					EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
								@event_type                = N'E',
								@context                   = @ale_context,
								@message                   = @ale_message,
								@event_type_detail         = N'E',
								--@correlation_guid          = @ale_correlation_guid OUTPUT,
								--@timestamp_utc             = N'',
								--@event_log_guid            = @ale_event_log_guid OUTPUT,
								@use_synchronous_logging   = 0,
								@print_ind                 = @param_is_test,
								@print_level               = 0,
								@calling_proc_name         = @proc_name;

					PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -E- ' + @ale_message
										+ char(13)+char(10)+N'@nsql=' + @nsql
										+ char(13)+char(10)+N'@paramDef=' + @paramDef;

					SELECT @error_msg =  @ale_message
							,@error_severity = ERROR_SEVERITY()
							,@error_state	 = ERROR_STATE();
					RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
				END CATCH
			END TRY
			BEGIN CATCH -- print error and continue, user severity 10 or lower
				--add logging here
				set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').';
				set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql, @paramDef as paramDef FOR XML PATH);

				EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
							@event_type                = N'E',
							@context                   = @ale_context,
							@message                   = @ale_message,
							@event_type_detail         = N'E',
							--@correlation_guid          = @ale_correlation_guid OUTPUT,
							--@timestamp_utc             = N'',
							--@event_log_guid            = @ale_event_log_guid OUTPUT,
							@use_synchronous_logging   = 0,
							@print_ind                 = @param_is_test,
							@print_level               = 0,
							@calling_proc_name         = @proc_name;
		
				SELECT @error_msg =  @ale_message
						,@error_severity = ERROR_SEVERITY()
						,@error_state	 = ERROR_STATE();
				RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
			END CATCH

			fetch next from p_stg_pop_ead_nonretail_smes_cursor into @table_name, @stage, @scenario_id, @scenario_name, @src_flow_type, @src_field_name
		END

		close p_stg_pop_ead_nonretail_smes_cursor
		deallocate p_stg_pop_ead_nonretail_smes_cursor

		/******************************************************************/
		/*End Process Log*/	
		/******************************************************************/
		update t_ecl_process_log_tbank 
		set end_time		=	getdate(),
			execution_time	=	cast((getdate() - start_time) as time(0)),
			process_status	=	'Success'
		where process_id = @log_pid
			and cob_date = @cob_date;

		Print '#################################################################################';

	END TRY
	BEGIN CATCH -- for catch random error
		
		/******************************************************************/
		/*End Process Log*/	
		/******************************************************************/
		update t_ecl_process_log_tbank 
		set end_time		=	getdate(),
			execution_time	=	cast((getdate() - start_time) as time(0)),
			process_status	=	'Failed'
		where process_id = @log_pid
			and cob_date = @cob_date;
		Print '#################################################################################';

		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql, @paramDef as paramDef FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = @param_is_test,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;
		
		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
	END CATCH

	--add logging here
	set @ale_message = N'End: ' + @proc_name;
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_FINISH', --ETL_ERROR --PROC_START --PROC_FINISH
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				--@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				--@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 0,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;
	
	-- print out msg
	IF @param_is_test = 1
	BEGIN
		PRINT CONVERT(NVARCHAR,CURRENT_TIMESTAMP,121) + N' -I- ' + @ale_message;
	END
-- Return succes
RETURN (0) 

GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pop_ead_nonretail_smes' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pop_ead_nonretail_smes] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pop_ead_nonretail_smes] has NOT been altered due to errors!'
END
GO
