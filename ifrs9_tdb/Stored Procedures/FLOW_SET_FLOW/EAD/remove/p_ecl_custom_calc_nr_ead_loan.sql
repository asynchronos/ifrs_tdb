﻿
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_ecl_custom_calc_nr_ead_loan' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_ecl_custom_calc_nr_ead_loan] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_ecl_custom_calc_nr_ead_loan] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_ecl_custom_calc_nr_ead_loan]...'
GO
ALTER procedure [dbo].[p_ecl_custom_calc_nr_ead_loan] 
as
begin


DECLARE @contract_id			int
DECLARE @object_key_value		d_object_key_value
DECLARE @ecl_outstanding_amount decimal(25,8)
DECLARE @ecl_effective_yield	decimal(25,16)
DECLARE @interest_basis_type	d_interest_basis
DECLARE @reporting_date			date
DECLARE @deal_id				d_deal_id
DECLARE @deal_id_prev			d_deal_id
DECLARE @entity					d_entity
DECLARE @maturity_date			date
DECLARE @flow_set_id			int
DECLARE @flow_date				date
DECLARE @currency				varchar(50)
DECLARE @amount					decimal(25,8)
DECLARE @amount_capita			decimal(25,8)
DECLARE @amount_intcon			decimal(25,8)
DECLARE @day_count				int

DECLARE @no_day					int
DECLARE @outstand_end_period	decimal(25,8)
DECLARE @total_cf_in			decimal(25,8)
DECLARE @PREV_OUTS				decimal(25,8)
DECLARE @int_by_eff_yield		decimal(25,8)
DECLARE @prev_r					decimal(25,8)
DECLARE @prev_outs				decimal(25,8)
DECLARE @outstand_init_cost		decimal(25,8)
DECLARE @int_eff_yield_day		decimal(25,8)
declare @eff_int_eod			decimal(25,8)
DECLARE @amo_cost_end_period	decimal(25,8)
DECLARE @eir_int_adj			decimal(25,8)
DECLARE @adj_id					d_id  = 2      --fix adjustment_id = 1 for custom NR EAD 

DECLARE @p_scn_id				d_id			
--DECLARE @p_scn_max				int=3
DECLARE @set_num				int=0	
DECLARE @set_char				varchar(1)=''		
DECLARE @line_nr				d_id
DECLARE @p_object_origin		varchar(6)='LODEP'		
DECLARE @p_object_key_type		varchar(1)='2'
DECLARE @p_amt_type				varchar(10)='CAPITA'
DECLARE @end_validity_date		date= '9999-12-31 00:00:00.000'
DECLARE @start_validity_date	date
DECLARE @cob_date				datetime
--declare @scenario_id  			d_id	
DECLARE @rate_thb				d_rate
DECLARE @pool_id				d_pool_id
DECLARE @pool_name				d_name

	select	@cob_date=cob_date 
	from	t_entity 
	where	entity = 'TBANK'
	--SET @cob_date = '2018-03-31'

	-----------------------------------
	-- get rate for convert currency --
	-----------------------------------
	select	@rate_thb=rate 
	from	t_currency_rate 
	where	to_currency	= 'USD'
	and		rate_date	= @cob_date

	----------------------
	-- clear temp table --
	----------------------
	if object_id('tempdb..#w_eom30years') is not null 
		drop table #w_eom30years;
	if object_id('tempdb..#w_flow_set') is not null 
		drop table #w_flow_set;
	if object_id('tempdb..#w_flow_max') is not null 
		drop table #w_flow_max;
	if object_id('tempdb..#w_loan_val') is not null 
		drop table #w_loan_val;
	-------------------------------------------------------------
	-- end of month 30 years --
	-------------------------------------------------------------
	with dates as	(	select EOMONTH(@cob_date) as dum_date
						union all
						select EOMONTH( dateadd(DAY,1,dum_date) )
						from dates
						where dum_date < dateadd(yy,30,getdate())
					)
	select	* into #w_eom30years 
	from	dates 
	option (maxrecursion 4000);
	create nonclustered index idx1_w_eom30years ON #w_eom30years(dum_date)	
	-------------------------------------------------------------
	-- Total EAD contract in flow_set --	
	-------------------------------------------------------------	
	select contract_id,item_object_key_value,outstanding_amount,effective_yield,interest_basis,@cob_date reporting_date,deal_id,entity,maturity_date
			,amount_type,flow_set_id,pool_id,pool_name
	into	#w_flow_set
    from 
	(
	--## normal term loan ##--
	--select	s.contract_id,s.item_object_key_value,s.outstanding_amount,s.effective_yield,s.interest_basis,@cob_date reporting_date,fs.deal_id,fs.entity,s.maturity_date
	--		,fs.amount_type,fs.flow_set_id,p.pool_id,p.pool_name
	--from	t_ecl_segmentation s 		
	--join	(	select	max(lot_id) lot_id
	--			from	t_lot_definition 
	--			where	std_record_function_code ='ECLSEG'
	--			and		reporting_date = @cob_date
	--			--and		lot_id = 104
	--		) lot
	--on		s.lot_id=lot.lot_id
	--join	t_pool p
	--on		p.pool_id = s.segment_pool_id
	--and		p.pool_name in ('UPL','T-LOAN','SPL','HL','MRTA','STAFF-HL','STAFF-FP','STAFF-HP','HP-CYB')
	--join	t_trn_loan_flow_set fs 
	--on		fs.deal_id + '|' + fs.entity = s.item_object_key_value
	--where	@cob_date between fs.start_validity_date and fs.end_validity_date		
	--and		fs.flow_type='CONTRA'
	--and		s.item_object_origin = 'LODEP'
	--and		s.segment_pool_id = p.pool_id
	--and		s.effective_yield is not null
	--and		s.outstanding_amount > 0
	--and		s.maturity_date	>= @cob_date -- fillter only found flow 		
	--and		s.contract_id = '3781130'
	--union all 
	--## nonretail ##--
	select	s.contract_id,s.item_object_key_value,s.outstanding_amount,s.effective_yield,s.interest_basis,@cob_date reporting_date,fs.deal_id,fs.entity,s.maturity_date
			,fs.amount_type,fs.flow_set_id,p.pool_id,p.pool_name	
	from	t_ecl_segmentation s 		
	join	(	select	max(lot_id) lot_id
				from	t_lot_definition 
				where	std_record_function_code ='ECLSEG'
				and		reporting_date = @cob_date				
			) lot
	on		s.lot_id=lot.lot_id
	join	t_pool p
	on		p.pool_id = s.segment_pool_id
	and		p.pool_name in ( 'Sovereign','Bank','NBFI','Corporate','Commercial','SMEs' )
	join	t_trn_loan_flow_set fs 
	on		fs.deal_id + '|' + fs.entity = s.item_object_key_value
	join	t_nonretail_ead_tbank nr
	on		nr.deal_id = fs.deal_id
	where	@cob_date between fs.start_validity_date and fs.end_validity_date		
	and		fs.flow_type='CONTRA'
	and		nr.ead_method = 'GCA'
	and		s.item_object_origin = 'LODEP'	
	and		s.effective_yield is not null
	and		s.outstanding_amount > 0
	and		s.maturity_date	>= @cob_date 
	-- Bik-20180927: Interim Solution for filter out duplicate deal_id
	and s.deal_tfi_id not in ('FP001356500179','FP001359501124','FP001359501221'
	,'FP001359502065','FP001359501166','FP001359501182','FP001359501205')
	) tt
	create nonclustered index idx1_w_flow_set ON #w_flow_set(flow_set_id,contract_id)	
	-------------------------------------------------------------
	-- Max flow_date all contact in EAD --	
	-------------------------------------------------------------
	select	fs.contract_id , max(currency) currency_max ,max(flow_date) flow_date_max
	into	#w_flow_max
	from	#w_flow_set fs
	join	t_trn_loan_flow f 
	on		f.flow_set_id = fs.flow_set_id
	group by fs.contract_id
	create nonclustered index idx1_w_flow_max ON #w_flow_max(contract_id)	
	-------------------------------------------------------------
	DECLARE flow_cursor CURSOR FOR 		
		select	contract_id,item_object_key_value,outstanding_amount,effective_yield,interest_basis,reporting_date,deal_id,entity,maturity_date		
				,flow_date,currency,pool_id,pool_name
				,max(amount_capita) amount_capita,max(amount_intcon) amount_intcon,max(day_count) day_count
		from	(
		select	t.*,datediff(day,lag(flow_date,1) over (partition by deal_id ORDER BY flow_date),flow_date) as day_count
		from	(
		select	fs.contract_id,fs.item_object_key_value,fs.outstanding_amount,fs.effective_yield,fs.interest_basis,fs.reporting_date,fs.deal_id,fs.entity,fs.maturity_date		
				,f.flow_date,f.currency,fs.pool_id,fs.pool_name
				,sum(case when fs.amount_type = 'CAPITA'  then  isnull(f.amount,0) else 0 end) as amount_capita
				,sum(case when fs.amount_type in ('INTCON','NA','UPFINT') then  isnull(f.amount,0) else 0 end) as amount_intcon		
		from	#w_flow_set fs
		left	join t_trn_loan_flow f 
		on		f.flow_set_id = fs.flow_set_id			
		group by fs.contract_id,fs.item_object_key_value,fs.outstanding_amount,fs.effective_yield,fs.interest_basis,fs.reporting_date,fs.deal_id,fs.entity,fs.maturity_date		
		,f.flow_date,f.currency,fs.pool_id,fs.pool_name
		union all
		select	fs.contract_id,fs.item_object_key_value,fs.outstanding_amount,fs.effective_yield,fs.interest_basis,fs.reporting_date,fs.deal_id,fs.entity,fs.maturity_date		
				,dum_date,currency_max,fs.pool_id,fs.pool_name,0.0,0.0
		from	#w_flow_set fs
		join	#w_eom30years 
		on		1 = 1
		join	#w_flow_max f
		on		fs.contract_id = f.contract_id
		where	dum_date between eomonth(reporting_date) and eomonth(flow_date_max)
		) t
		) tt
		group by contract_id,item_object_key_value,outstanding_amount,effective_yield,interest_basis,reporting_date,deal_id,entity,maturity_date		
				,flow_date,currency,pool_id,pool_name
		order by contract_id,flow_date

	OPEN flow_cursor  		
		WHILE (1 = 1)
		begin
			FETCH NEXT FROM flow_cursor INTO @contract_id,@object_key_value,@ecl_outstanding_amount,@ecl_effective_yield,@interest_basis_type,@reporting_date,@deal_id,@entity,@maturity_date
											,@flow_date,@currency,@pool_id,@pool_name
											,@amount_capita,@amount_intcon,@day_count
			IF (@@FETCH_STATUS <> 0) BREAK;			
			begin
				---------------------------
				-- start reset parameter --
				---------------------------
				if @deal_id <> @deal_id_prev or @deal_id_prev is null 				
					begin						
						set @deal_id_prev	= @deal_id
						SET @prev_outs		= @ecl_outstanding_amount
						set @prev_r			= @ecl_outstanding_amount
						------------------------------
						select	@line_nr=isnull(max(line_nr),0)+1 
						from	t_etl_adj_data_flow_set_flow 
						where	entity			= @entity
						and		adjustment_id	= @adj_id 	
						-----------------------------------
						-- get rate for convert currency --
						-----------------------------------
						if @currency <> 'THB' 
						begin
							select	@rate_thb=rate 
							from	t_currency_rate 
							where	to_currency	= @currency
							and		rate_date	= @cob_date
						end			
						------------------------------
						-- EAD Stage3 and SCN 1,2,3 --
						------------------------------
						if @reporting_date = eomonth(@reporting_date)
						begin							
							------------------
							--select	@amount=amount
							--from	#w_loan_val
							--where	deal_id = @deal_id
							select	@amount=sum(isnull(amount,0)) 
							from	t_trn_loan_valuation
							where	entity	= @entity
							and		deal_id = @deal_id
							and		valuation_type in ( 'OUTSTANDING AI','OUTSTANDING PRINCIPAL')
							and		valuation_date = @reporting_date
							------------------
							--<#loop scn#ST>--
							------------------
							--declare c_scn_stg3 cursor for (	select	scenario_id
							--								from	t_scenario
							--								where	scenario_context_id = 301003
							--								and		scenario_name like 'Stage 3%'	)
							--begin 
							--	open c_scn_stg3  		
							--	while (1 = 1)
							--	begin
							--	fetch next from c_scn_stg3 into @p_scn_id
							--		if (@@fetch_status <> 0) break;							
							--		begin 
										INSERT INTO dbo.t_etl_adj_data_flow_set_flow
												(
													  adjustment_id, line_nr, flag, line_status, retry_count, line_date
													, entity, object_origin, object_key_type, object_key_value
													, flow_type, flow_set_id, scenario_id, amount_type
													, start_validity_date, end_validity_date, calculation_date, flow_date
													, currency, amount
													, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5
													, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5
													, last_modified, modified_by 
												)
										VALUES
												(
													 @adj_id, @line_nr, 'U', 0, 0, current_timestamp
													,@entity,@p_object_origin,@p_object_key_type,@object_key_value
													--,'EAD_3',null,@p_scn_id,@p_amt_type
													,'EAD_3',null,3,@p_amt_type
													,@reporting_date,@end_validity_date,@reporting_date,@flow_date
													,case	when ( @currency <> 'THB' and @rate_thb is not null ) 
															then 'THB' 
															else @currency 
													 end													
													,case	when ( @currency <> 'THB' and @rate_thb is not null ) 
															then round((@amount*@rate_thb),2) 
															else @amount 
													 end 
													,@set_char,@set_char,@pool_name,@set_char,@set_char
													,@pool_id,@set_num,@set_num,@set_num,@set_num
												   ,CURRENT_TIMESTAMP,SYSTEM_USER	
											   )
										set @line_nr	= @line_nr + 1														
							--		end										
							--	end		
							--	close c_scn_stg3
							--	deallocate c_scn_stg3		
							--end				
							--set @p_scn_id = 1 
							------------------
							--<#loop scn#ED>--
							------------------
						end
					end
				-------------------------
				-- end reset parameter --
				-------------------------
			

				---------------------------------------------------------------
				-- process ead_2 --
				---------------------------------------------------------------
				set @no_day = coalesce(@day_count,datediff(day,@reporting_date,@flow_date))
				set @outstand_end_period = @prev_outs - @amount_capita
				set @total_cf_in = @amount_capita + @amount_intcon
	
				if @interest_basis_type = '101'
					begin 
						set @int_by_eff_yield = round(
													case when @prev_r = @ecl_outstanding_amount
													then @prev_outs * (power(1 + @ecl_effective_yield,(@no_day)/365.0000000000000000) - 1)
													else @prev_r * (power(1 + @ecl_effective_yield,(@no_day-1)/365.0000000000000000) - 1) 
													end
													,2)						
					end
				else if @interest_basis_type = '102'
					begin 
						set @int_by_eff_yield = round(
													case when @prev_r = @ecl_outstanding_amount
													then @prev_outs * (power(1 + @ecl_effective_yield,(@no_day)/360.0000000000000000) - 1)
													else @prev_r * (power(1 + @ecl_effective_yield,(@no_day-1)/360.0000000000000000) - 1) 
													end
													,2)						
					end
				else if @interest_basis_type = '103'
					begin 
						set @int_by_eff_yield = round(
													case when @prev_r = @ecl_outstanding_amount
													then @prev_outs * (power(1 + @ecl_effective_yield,30/360.0000000000000000) - 1)
													else @prev_r * (power(1 + @ecl_effective_yield,30/360.0000000000000000) - 1) 
													end
													,2)						
					end
	
				set @outstand_init_cost = round(@prev_r + @int_by_eff_yield - (@amount_capita + @amount_intcon),2)
	
				if @outstand_init_cost < 0 begin  set @outstand_init_cost =  0 end

				if @interest_basis_type = '101'
					set @int_eff_yield_day = round(@outstand_init_cost * (power(1 + @ecl_effective_yield, 1/365.0000000000000000) - 1),2)
				else if @interest_basis_type = '102'
					set @int_eff_yield_day = round(@outstand_init_cost * (power(1 + @ecl_effective_yield, 1/360.0000000000000000) - 1),2)
				else if @interest_basis_type = '103'
					set @int_eff_yield_day = round(@outstand_init_cost * (power(1 + @ecl_effective_yield, 1/360.0000000000000000) - 1),2)
				
				set @eff_int_eod = round(@int_eff_yield_day + @int_by_eff_yield,2)

				set @amo_cost_end_period = round(
						case when @prev_r = @ecl_outstanding_amount 
						then @prev_r + @eff_int_eod - @total_cf_in 
						else @prev_r + @eff_int_eod - @total_cf_in 
						end,2)

				if @amo_cost_end_period < 0 begin  set @amo_cost_end_period =  0 end

				set @eir_int_adj = round(@eff_int_eod - @amount_intcon,2)

				set @prev_outs = @outstand_end_period
				set @prev_r = @amo_cost_end_period

				--finish

				---PRINT('@@flow_date					: ' + cast(@flow_date as varchar(50))) 
				if @flow_date = eomonth(@flow_date) --and (@amo_cost_end_period>0)
				Begin
				
					------------------------------
					-- EAD Stage2 and SCN 1,2,3 --
					------------------------------										
					------------------
					--<#loop scn#ST>--
					------------------
					--declare c_scn_stg2 cursor for (	select	min(scenario_id)
					--								from	t_scenario
					--								where	scenario_context_id = 301003
					--								and		scenario_name like 'Stage 2%'	)
					--begin 
					--	open c_scn_stg2  		
					--	while (1 = 1)
					--	begin
					--	fetch next from c_scn_stg2 into @p_scn_id
					--		if (@@fetch_status <> 0) break;							
					--		begin 
								INSERT INTO dbo.t_etl_adj_data_flow_set_flow
										(
											adjustment_id, line_nr, flag, line_status, retry_count, line_date
											, entity, object_origin, object_key_type, object_key_value
											, flow_type, flow_set_id, scenario_id, amount_type
											, start_validity_date, end_validity_date, calculation_date, flow_date
											, currency, amount
											, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5
											, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5
											, last_modified, modified_by 
										)
								VALUES
										(
											 @adj_id, @line_nr, 'U', 0, 0, current_timestamp
											,@entity,@p_object_origin,@p_object_key_type,@object_key_value
											--,'EAD_2',null,@p_scn_id,@p_amt_type
											,'EAD_2',null,2,@p_amt_type
											,@reporting_date,@end_validity_date,@reporting_date,@flow_date											
											,case	when ( @currency <> 'THB' and @rate_thb is not null ) 
													then 'THB' 
													else @currency 
											 end
											,case	when ( @currency <> 'THB' and @rate_thb is not null ) 
													then round((@amo_cost_end_period*@rate_thb),2) 
													else @amo_cost_end_period 
											 end 
											,@set_char,@set_char,@pool_name,@set_char,@set_char
											,@pool_id,@set_num,@set_num,@set_num,@set_num
											,CURRENT_TIMESTAMP,SYSTEM_USER	
										)
								set @line_nr	= @line_nr + 1												
					--		end
					--	end		
					--	close c_scn_stg2
					--	deallocate c_scn_stg2		
					--end				
					------------------
					--<#loop scn#ED>--
					------------------	
				end
			end --if	
		end --while
	
	CLOSE flow_cursor
	DEALLOCATE flow_cursor
		
	----------------
	-- EAD Stage1 --
	----------------	
	select	@line_nr=max(line_nr)
	from	t_etl_adj_data_flow_set_flow 
	where	entity				= @entity
	and		adjustment_id		= @adj_id 		
	---------------------------------------------------	
	if @line_nr > 0
	begin
		insert into t_etl_adj_data_flow_set_flow
							(
								 adjustment_id, line_nr, flag, line_status, retry_count, line_date
								, entity, object_origin, object_key_type, object_key_value
								, flow_type, flow_set_id, scenario_id, amount_type
								, start_validity_date, end_validity_date, calculation_date, flow_date
								, currency, amount
								, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5
								, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5
								, last_modified, modified_by 
							)
		select  @adj_id
				,@line_nr + row_number() over(order by object_key_value,flow_date ) 
				,'U', 0, 0, current_timestamp
				,@entity,@p_object_origin,@p_object_key_type,object_key_value				
				,'EAD_1',null,1,@p_amt_type
				,start_validity_date,end_validity_date,@reporting_date,flow_date
				,currency,amount
				,'','',flow_set_char_cust_element3,'',''
				,flow_set_num_cust_element1,0,0,0,0						
			   ,CURRENT_TIMESTAMP,SYSTEM_USER									
		from	(	select	*,row_number() over(partition by object_key_value order by flow_date) rn
					from	t_etl_adj_data_flow_set_flow
					where	adjustment_id = 1
					and     flow_type = 'EAD_2'	
					and		scenario_id = 2					
				)  t1
		where	rn <= 12
/*****			
		-------------
		-- case  run by stage
		-------------
		declare @flow_set_char_cust_element3		d_char_cust_element1
		declare @flow_set_num_cust_element1			d_num_cust_element1
		declare c_ead_stg1 cursor for 				
			select  object_key_value , amount 
					,start_validity_date,flow_date,flow_set_char_cust_element3,flow_set_num_cust_element1				
			from	(	select	*,row_number() over(partition by object_key_value order by flow_date) rn
						from	t_etl_adj_data_flow_set_flow
						where	adjustment_id = @adj_id
						and     flow_type = 'EAD_2'	
						and		scenario_id = 2					
						--and		scenario_id = ( select	min(scenario_id)
						--						from	t_scenario
						--						where	scenario_context_id = 301003
						--						and		scenario_name like 'Stage 2%' )
					)  t1
			where	rn <= 12

		OPEN c_ead_stg1  
		FETCH NEXT FROM c_ead_stg1 INTO @object_key_value,@amount,@start_validity_date,@flow_date,@flow_set_char_cust_element3,@flow_set_num_cust_element1
		WHILE @@FETCH_STATUS = 0
		begin
			--declare c_scn_stg1 cursor for (	select	min(scenario_id)
			--								from	t_scenario
			--								where	scenario_context_id = 301003
			--								and		scenario_name like 'Stage 1%'	)
			--begin 
			--	open c_scn_stg1 		
			--	while (1 = 1)
			--	begin
			--	fetch next from c_scn_stg1 into @p_scn_id
			--		if (@@fetch_status <> 0) break;		
			--		begin 
						set @line_nr	= @line_nr + 1	
						insert into t_etl_adj_data_flow_set_flow
							(
								 adjustment_id, line_nr, flag, line_status, retry_count, line_date
								, entity, object_origin, object_key_type, object_key_value
								, flow_type, flow_set_id, scenario_id, amount_type
								, start_validity_date, end_validity_date, calculation_date, flow_date
								, currency, amount
								, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5
								, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5
								, last_modified, modified_by 
							)
						values
							(
								 @adj_id, @line_nr, 'U', 0, 0, current_timestamp
								,@entity,@p_object_origin,@p_object_key_type,@object_key_value
								--,'EAD_1',null,@p_scn_id,@p_amt_type
								,'EAD_1',null,1,@p_amt_type
								,@start_validity_date,@end_validity_date,@reporting_date,@flow_date
								,@currency,@amount
								,'','',@flow_set_char_cust_element3,'',''
								,@flow_set_num_cust_element1,0,0,0,0						
							   ,CURRENT_TIMESTAMP,SYSTEM_USER	
						   )													
				--	end						
				--end		
				--close c_scn_stg1
				--deallocate c_scn_stg1		
			--end															
			FETCH NEXT FROM c_ead_stg1 INTO @object_key_value,@amount,@start_validity_date,@flow_date,@flow_set_char_cust_element3,@flow_set_num_cust_element1
		end
		CLOSE c_ead_stg1
		DEALLOCATE c_ead_stg1 
*****/	
	end 

	-- run hms
	exec p_etl_adj_data_flow_tbank	@execution_date	= @cob_date, 
									@cleanup_type = 2,
									@cleanup_staging = 0,
									@only_cleanup_changed = 1,
									@use_delta_load = 1, 
									@adjustment_id	= @adj_id,
									@entity			= @entity,  
									@log_output     = 0

	----------------------
	-- clear temp table --
	----------------------
	if object_id('tempdb..#w_eom30years') is not null 
		drop table #w_eom30years;
	if object_id('tempdb..#w_flow_set') is not null 
		drop table #w_flow_set;
	if object_id('tempdb..#w_flow_max') is not null 
		drop table #w_flow_max;
	if object_id('tempdb..#w_loan_val') is not null 
		drop table #w_loan_val;		
END







GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_ecl_custom_calc_nr_ead_loan' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_ecl_custom_calc_nr_ead_loan] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_ecl_custom_calc_nr_ead_loan] has NOT been altered due to errors!'
END
GO
