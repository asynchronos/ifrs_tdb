
IF NOT EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pop_ead_main_deal_1_point' AND schema_id = SCHEMA_ID('dbo'))
  BEGIN
    PRINT N'Creating procedure [dbo].[p_stg_pop_ead_main_deal_1_point] ...'
    EXEC (N'CREATE PROCEDURE [dbo].[p_stg_pop_ead_main_deal_1_point] AS RETURN(0)')
    WAITFOR DELAY N'00:00:00.003'
  END
PRINT N'Altering procedure [dbo].[p_stg_pop_ead_main_deal_1_point]...'
GO

ALTER PROCEDURE [dbo].[p_stg_pop_ead_main_deal_1_point] 
	-- Add the parameters for the stored procedure here
	@param_reporting_date	datetime,
	@param_scenario_id		d_name,
	@param_scenario_name	d_name,
	@param_stage			d_name,
	@param_src_value_type	d_name,
	@param_src_table_name	d_name,
	@param_src_field_name	d_name,
	@param_model_name		d_name,
	@param_is_test			bit				= 0
AS
-- ********************************************************************************
-- * Author:		Prakit Sirisaksathaporn <biku>                                *
-- * Description:	This stored procedure will populate data for				  *
-- *				t_etl_adj_data_crv_curve and run HMS process to live table	  *
-- *				t_crv_curve and t_crv_curve_point_value.					  *
-- *																			  *
-- * Inputs:		- @param_reporting_date: reporting date/cob date              *
-- *				- @param_src_table_name: point in time source table name      *
-- *				- @param_scenario_id: scenario_id/list of scenario_id		  *
-- *				- @param_scenario_name: scenario_name						  *
-- *				- @param_stage: stage name for mapping table/column			  *
-- *				- @param_is_test: populated recored                           *
-- *					0(default):populate all data							  *
-- *					1:poulate only top 100									  *
-- ********************************************************************************

-- Declare error/debug variables
DECLARE @proc_name  sysname -- procedure name
DECLARE @status     INT     -- return status
DECLARE @error      INT     -- saved error context
DECLARE @rowcount   INT     -- saved rowcount context
DECLARE @error_msg	NVARCHAR(MAX) 
DECLARE @error_severity	INT
DECLARE @error_state	INT
DECLARE @adj_err_line	INT

-- Initialise error/debug variables
SELECT @proc_name		= OBJECT_NAME( @@PROCID ),
       @status			= 0,
       @error			= 0,
       @rowcount		= 0,
	   @error_msg		= '',
	   @error_severity	= 0,
	   @error_state		= 0,
	   @adj_err_line	= 7 -- adjust error line

	-- variables for ale
	declare @ale_message						d_ale_message;
	declare @ale_context						xml;
	declare @ale_correlation_guid				UNIQUEIDENTIFIER;
	declare @ale_event_log_guid					UNIQUEIDENTIFIER;

-- Declare local variables
	declare @cob_date							datetime;
	declare @max_date							datetime				=	'9999-12-31';
	declare @entity								d_entity				=	N'TBANK';
	declare @lot_fn_code						d_std_record_function	=	N'ECLSEG';
	declare @max_lot_id							d_id;
	declare @scenario_context_id				d_id					=	301003;

	declare @adjustment_id						d_id					=	0;
	declare @adj_type_id						d_id;
	declare @src_table_name1					d_name					=	@param_src_table_name;
	declare @src_table_name2					d_name					=	N''
	declare @src_table_name3					d_name					=	N''
	declare @table_type_name					d_name					=	N't_etl_adj_data_flow_set_flow';

	-- variable for dynamic sql
	declare @nsql								nvarchar(max);
	declare @nsql_select						nvarchar(50);
	declare @nsql_column						nvarchar(max);
	declare @nsql_table							nvarchar(max);
	declare @nsql_filter						nvarchar(max);
	declare @nsql_insert						nvarchar(max);
	declare @paramDef							nvarchar(max);
																
	-- variable for insert t_etl_adj_data_xxx					
	declare @field_adjustment_id   				nvarchar(max);
	declare @field_line_nr		   				nvarchar(max);
	declare @field_flag         				nvarchar(max);
	declare @field_line_status         			nvarchar(max);
	declare @field_retry_count         			nvarchar(max);
	declare @field_line_date         			nvarchar(max);
	declare @field_entity	            		nvarchar(max);
	declare @field_object_origin				nvarchar(max);
	declare @field_object_key_type				nvarchar(max);
	declare @field_object_key_value				nvarchar(max);
	declare @field_flow_type    				nvarchar(max);
	declare @field_flow_set_id   				nvarchar(max);
	declare @field_scenario_id					nvarchar(max);
	declare @field_amount_type					nvarchar(max);
	declare @field_start_validity_date      	nvarchar(max);
	declare @field_end_validity_date			nvarchar(max);
	declare @field_calculation_date				nvarchar(max);
	declare @field_flow_date					nvarchar(max);
	declare @field_currency						nvarchar(max);
	declare @field_amount						nvarchar(max);
	declare @field_char_cust_element1			nvarchar(max);
	declare @field_char_cust_element2			nvarchar(max);
	declare @field_char_cust_element3			nvarchar(max);
	declare @field_char_cust_element4			nvarchar(max);
	declare @field_char_cust_element5			nvarchar(max);
	declare @field_num_cust_element1			nvarchar(max);
	declare @field_num_cust_element2			nvarchar(max);
	declare @field_num_cust_element3			nvarchar(max);
	declare @field_num_cust_element4			nvarchar(max);
	declare @field_num_cust_element5			nvarchar(max);
	declare @field_last_modified				nvarchar(max);
	declare @field_modified_by					nvarchar(max);
		
	-- assign cob date
	select @cob_date = isnull(@param_reporting_date,cob_date) from t_entity where entity = @entity;

	-- assign max lot id
	select @max_lot_id = max(lot.lot_id) from t_lot_definition lot where lot.std_record_function_code = @lot_fn_code;

	IF @param_reporting_date is null 
		or @param_scenario_id is null
		or @param_scenario_name is null
		or @param_stage is null
		or @param_src_value_type is null
		or @param_src_table_name is null
		or @param_src_field_name is null
		or @param_model_name is null
	BEGIN
		select @error_msg = isnull(@param_reporting_date,',@param_reporting_date')
							+ isnull(@param_scenario_id,',@param_scenario_id')
							+ isnull(@param_scenario_name,',@param_scenario_name')
							+ isnull(@param_stage,',@param_stage')
							+ isnull(@param_src_value_type,',@param_src_value_type')
							+ isnull(@param_src_table_name,',@param_src_table_name')
							+ isnull(@param_src_field_name,',@param_src_field_name')
							+ isnull(@param_model_name,',@param_model_name');
		set @error_msg = N'This procedure must send parameter: ' + substring(@error_msg,2,len(@error_msg))
		RAISERROR (@error_msg, 16, 1, @proc_name)
		RETURN 1
	END

	--add logging here
	set @ale_message = N'Start: ' + @proc_name
						+ N'(@param_reporting_date=' + CONVERT(varchar,@param_reporting_date)
						+ char(13)+char(10)+char(9)+N',@param_scenario_id=' + CONVERT(varchar,@param_scenario_id)
						+ char(13)+char(10)+char(9)+N',@param_scenario_name=' + CONVERT(varchar,@param_scenario_name)
						+ char(13)+char(10)+char(9)+N',@param_stage=' + CONVERT(varchar,@param_stage)
						+ char(13)+char(10)+char(9)+N',@param_src_value_type=' + CONVERT(varchar,@param_src_value_type)
						+ char(13)+char(10)+char(9)+N',@param_src_field_name=' + CONVERT(varchar,@param_src_field_name)
						+ char(13)+char(10)+char(9)+N',@param_model_name=' + CONVERT(varchar,@param_model_name)
						+ char(13)+char(10)+char(9)+N',@param_is_test=' + CONVERT(varchar,@param_is_test)
						+ char(13)+char(10)+char(9)+N',@cob_date=' + CONVERT(varchar,@cob_date)
						+ N')';
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_START', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

-- Start actual procedure flow
	DECLARE @is_nrt_smes	int;

	SELECT @is_nrt_smes		=	case @param_model_name 
									when 'NRT_SMEs' then 1
								else 0 end;
	
	BEGIN TRANSACTION;
	BEGIN TRY
		declare @max_line_nr int;
		select @max_line_nr=isnull(max(fsf.line_nr),0)
		from t_etl_adj_data_flow_set_flow fsf
		where fsf.adjustment_id=0;

		-- assign field for select & insert
		-- src = source table, seg = t_ecl_segmentation
		-- text must have single-quote/char(39) between text
		-- function, field, numeric do not need single-quote
		select	 @field_adjustment_id   			= N'@adjustment_id'									-- must update with p_adj_save before run HMS
				,@field_line_nr         			= N'row_number() over(order by src.deal_id)'	-- must update before run if insert multiple time
				,@field_flag            			= char(39)+N'U'+char(39)
				,@field_line_status     			= char(39)+N'0'+char(39)
				,@field_retry_count     			= N'0'
				,@field_line_date       			= N'CURRENT_TIMESTAMP'
				,@field_entity          			= N'@entity'
				,@field_object_origin   			= N'seg.item_object_origin'							-- if deal: t_ecl_segmentation.item_object_origin
				,@field_object_key_type 			= char(39)+N'2'+char(39)
				,@field_object_key_value			= N'CONCAT('+char(39)+case @param_is_test when 0 then '' else 'TEST-' end+char(39)
														+',src.deal_id,''|'',@entity)'
				,@field_flow_type       			= N'@param_src_value_type'
				,@field_flow_set_id					= N'null'											-- generate when HMS
				,@field_scenario_id					= N'@param_stage'									-- change to stage id
				,@field_amount_type					= char(39)+N'CAPITA'+char(39)
				,@field_start_validity_date         = N'@cob_date'
				,@field_end_validity_date           = N'@max_date'
				,@field_calculation_date            = N'@cob_date'
				,@field_flow_date					= N'eomonth(dateadd(month,case @param_stage when ''3'' then 0 else 1 end,@cob_date))'
				,@field_currency					= N'seg.currency'									-- if deal: t_ecl_segmentation.currency
				,@field_amount						= N'case @param_stage'+char(13)+char(10)+
														+'when ''1''	then	src.ead_stage1'+char(13)+char(10)+
														+'when ''2''	then	src.ead_stage2'+char(13)+char(10)+
														+'when ''3''	then	src.ead_stage3'+char(13)+char(10)+
														+'else src.ead_stage3 end'
				,@field_char_cust_element1			= N'src.date_matur'									-- if deal: date_matur
				,@field_char_cust_element2			= N'CONVERT(nvarchar,@param_scenario_id)'			-- scenario_id
				,@field_char_cust_element3			= N'src.segment_name'								-- segment_name
				,@field_char_cust_element4			= N'@cob_date'										-- reporting_date
				,@field_char_cust_element5			= char(39)+N''+char(39)								-- free slot, not use
				,@field_num_cust_element1			= N'CONVERT(numeric,src.segment_id)'									-- segment_id
				,@field_num_cust_element2			= N'CONVERT(numeric,src.remaining_maturity)'							-- if deal: remaining_maturity
				,@field_num_cust_element3			= N'CONVERT(numeric,src.remaining_term)'								-- if deal: remaining_term
				,@field_num_cust_element4			= N'CONVERT(numeric,@param_stage)'										-- if deal: current stage?
				,@field_num_cust_element5			= N'0.0'											-- free slot, not use
				,@field_last_modified				= N'CURRENT_TIMESTAMP'
				,@field_modified_by					= N'@proc_name'
		;		

		select @nsql_insert = 'INSERT INTO t_etl_adj_data_flow_set_flow
					   (adjustment_id, line_nr, flag, line_status, retry_count, line_date, entity, object_origin, object_key_type, object_key_value, flow_type, flow_set_id, scenario_id, amount_type, start_validity_date, end_validity_date, calculation_date, flow_date, currency, amount, flow_set_char_cust_element1, flow_set_char_cust_element2, flow_set_char_cust_element3, flow_set_char_cust_element4, flow_set_char_cust_element5, flow_set_num_cust_element1, flow_set_num_cust_element2, flow_set_num_cust_element3, flow_set_num_cust_element4, flow_set_num_cust_element5, last_modified, modified_by)'
		;
		select @nsql_table = N''																	+char(13)+char(10)+
			N'from '+@param_src_table_name+' src
				join t_ecl_segmentation seg on src.deal_id=seg.deal_tfi_id
				and seg.lot_id=@max_lot_id'
		;
		select @nsql_filter = N''																	+char(13)+char(10)+
			N'where src.deal_id is not null'														+char(13)+char(10)+
				N'and src.as_of_date=@cob_date'														+char(13)+char(10)+
				CASE WHEN @is_nrt_smes=1 
					THEN N'and src.ead_method not in (''GCA'',''NO ECL'')'
				ELSE '' END
		--nrt	-- temporay filter until FP not duplicate
		--		and deal_id not in (''FP001356500179'',''FP001359501124'',''FP001359501221'',''FP001359502065'',''FP001359501166'',''FP001359501182'',''FP001359501205'')';
		;
		select @nsql_select = char(13)+char(10)+case @param_is_test when 0 then 'select ' else 'select top 300 ' end
		;
		select @nsql_column = N''																	+char(13)+char(10)+
				@field_adjustment_id   					+'	as adjustment_id,'						+char(13)+char(10)+
				@field_line_nr         					+'	as line_nr,'							+char(13)+char(10)+
				@field_flag            					+'	as flag,'								+char(13)+char(10)+
				@field_line_status     					+'	as line_status,'						+char(13)+char(10)+
				@field_retry_count     					+'	as retry_count,'						+char(13)+char(10)+
				@field_line_date       					+'	as line_date,'							+char(13)+char(10)+
				@field_entity          					+'	as entity,'								+char(13)+char(10)+
				@field_object_origin   					+'	as object_origin,'						+char(13)+char(10)+
				@field_object_key_type 					+'	as object_key_type,'					+char(13)+char(10)+
				@field_object_key_value					+'	as object_key_value,'					+char(13)+char(10)+
				@field_flow_type       					+'	as flow_type,'							+char(13)+char(10)+
				@field_flow_set_id						+'	as flow_set_id,'						+char(13)+char(10)+
				@field_scenario_id						+'	as scenario_id,'						+char(13)+char(10)+
				@field_amount_type						+'	as amount_type,'						+char(13)+char(10)+
				@field_start_validity_date				+'	as start_validity_date,'				+char(13)+char(10)+
				@field_end_validity_date				+'	as end_validity_date,'					+char(13)+char(10)+
				@field_calculation_date					+'	as calculation_date,'					+char(13)+char(10)+
				@field_flow_date						+'	as flow_date,'							+char(13)+char(10)+
				@field_currency							+'	as currency,'							+char(13)+char(10)+
				@field_amount							+'	as amount,'								+char(13)+char(10)+
				@field_char_cust_element1				+'	as char_cust_element1,'					+char(13)+char(10)+
				@field_char_cust_element2				+'	as char_cust_element2,'					+char(13)+char(10)+
				@field_char_cust_element3				+'	as char_cust_element3,'					+char(13)+char(10)+
				@field_char_cust_element4				+'	as char_cust_element4,'					+char(13)+char(10)+
				@field_char_cust_element5				+'	as char_cust_element5,'					+char(13)+char(10)+
				@field_num_cust_element1				+'	as num_cust_element1,'					+char(13)+char(10)+
				@field_num_cust_element2				+'	as num_cust_element2,'					+char(13)+char(10)+
				@field_num_cust_element3				+'	as num_cust_element3,'					+char(13)+char(10)+
				@field_num_cust_element4				+'	as num_cust_element4,'					+char(13)+char(10)+
				@field_num_cust_element5				+'	as num_cust_element5,'					+char(13)+char(10)+
				@field_last_modified					+'	as last_modified,'						+char(13)+char(10)+
				@field_modified_by						+'	as modified_by';
		
		-- param definition
		set @paramDef = N'@proc_name			sysname
						,@param_scenario_id		d_name
						,@param_stage			d_name
						,@param_src_value_type	d_name
						,@entity				d_entity
						,@adjustment_id			d_id
						,@cob_date				datetime
						,@max_date				datetime
						,@max_lot_id			d_id';
		-- end prepare dynamic t-sql
		
		-- final t-sql
		set @nsql = @nsql_insert + @nsql_select + @nsql_column + @nsql_table + @nsql_filter;

		-- check final t-sql length
		IF len(@nsql) > 4000 or @nsql is null
		BEGIN
			set @ale_message = N'SQL string is over than 4000 character or null.';
			set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, len(@nsql) as len_nsql
				, @nsql_insert as nsql_insert, @nsql_select as nsql_select
				, @nsql_column as nsql_column, @nsql_table as nsql_table
				, @nsql_filter as nsql_filter FOR XML PATH);

			EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
						@event_type                = N'E',
						@context                   = @ale_context,
						@message                   = @ale_message,
						@event_type_detail         = N'E',
						--@correlation_guid          = @ale_correlation_guid OUTPUT,
						--@timestamp_utc             = N'',
						--@event_log_guid            = @ale_event_log_guid OUTPUT,
						@use_synchronous_logging   = 0,
						@print_ind                 = @param_is_test,
						@print_level               = 0,
						@calling_proc_name         = @proc_name;

			PRINT '@nsql_insert:'	+ISNULL(@nsql_insert,'NULL')
			PRINT '@nsql_select:'	+ISNULL(@nsql_select,'NULL')
			PRINT '@nsql_column:'	+ISNULL(@nsql_column,'NULL')
			PRINT '@nsql_table:'	+ISNULL(@nsql_table,'NULL')
			PRINT '@nsql_filter:'	+ISNULL(@nsql_filter,'NULL')

			set @error_msg = @ale_message
			RAISERROR (@error_msg, 16, 1, @proc_name)
		END

		--add logging here
		set @ale_message = N'Inserting '+ @src_table_name1+','+@src_table_name2+','+@src_table_name3 + ' to ' + @table_type_name+' (adjustment_id:'+CONVERT(NVARCHAR,@adjustment_id)+')';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @nsql as nsql, @paramDef as paramDef FOR XML PATH);

		EXEC p_ale_log @event_name            = N'CREATE_ADJUSTMENT', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT 
					@event_type                = N'I',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'I',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = @param_is_test,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;

		RAISERROR(@ale_message,10,1,@proc_name);

		-- insert data with sp_executesql
		exec sp_executesql @nsql ,@paramDef ,@proc_name				=@proc_name
											,@param_scenario_id		=@param_scenario_id
											,@param_stage			=@param_stage			
											,@param_src_value_type	=@param_src_value_type	
											,@entity				=@entity
											,@adjustment_id			=@adjustment_id	
											,@cob_date				=@cob_date				
											,@max_date				=@max_date				
											,@max_lot_id			=@max_lot_id
		;

		-- Create adjustmnet_id and run HMS
		declare @comment	d_long_description		= N'EAD|'+@param_src_table_name+N'|'+@param_scenario_name
		-- get table type id
		SELECT @adj_type_id   = (SELECT typ.type_id FROM t_adj_type typ JOIN t_adj_category cat ON cat.category_id = typ.category_id WHERE typ.type_name = @table_type_name);
		-- prepare adjustment before load to stg
		set @adjustment_id = null;
		EXEC @status = [dbo].[p_adj_save]
								@adjustment_id = @adjustment_id OUTPUT,
								@type_id = @adj_type_id,
								@reporting_date = @cob_date,
								@comment = @comment
		;

		-- update adjustment_id if init with 0
		update t_etl_adj_data_flow_set_flow
		set  adjustment_id	= @adjustment_id
		where adjustment_id = 0;

		--add logging here
		set @ale_message = N'Running HMS.';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value, @adjustment_id as adjustment_id FOR XML PATH);

		EXEC p_ale_log @event_name            = N'START_ADJUSTMENT', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
					@event_type                = N'I',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'I',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = @param_is_test,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;

		RAISERROR(@ale_message,10,1,@proc_name);

		-- run hms
		exec p_etl_adj_data_flow_tbank	@execution_date	= @cob_date, 
										@cleanup_type = 2,
										@cleanup_staging = 0,
										@only_cleanup_changed = 1,
										@use_delta_load = 1, 
										@adjustment_id	= @adjustment_id,
										@entity			= @entity,  
										@log_output     = 0
		;

		declare @hms_result nvarchar(max);
		select @hms_result = FORMATMESSAGE('Inserted %i flow to t_flow_set and %i flow date to t_flow' ,count(distinct fs.flow_set_id), count(f.flow_date))
		from t_flow_set fs join
			t_flow f on fs.flow_set_id=f.flow_set_id
		where exists (
			select etl.object_key_value
			from t_etl_adj_data_flow_set_flow etl
			where etl.adjustment_id=@adjustment_id
				and etl.object_key_value=fs.object_key_value
		);
		RAISERROR(@hms_result,10,1,@proc_name);

		COMMIT;
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK;

		--add logging here
		set @ale_message = @proc_name + N' error at line ' + CONVERT(NVARCHAR, ERROR_LINE()+@adj_err_line) + ': ' + ERROR_MESSAGE() + ' (error number ' + CONVERT(NVARCHAR, ERROR_NUMBER()) + ').';
		set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

		EXEC p_ale_log @event_name            = N'ETL_ERROR', --ETL_ERROR --PROC_START --PROC_FINISH
					@event_type                = N'E',
					@context                   = @ale_context,
					@message                   = @ale_message,
					@event_type_detail         = N'E',
					--@correlation_guid          = @ale_correlation_guid OUTPUT,
					--@timestamp_utc             = N'',
					--@event_log_guid            = @ale_event_log_guid OUTPUT,
					@use_synchronous_logging   = 0,
					@print_ind                 = 1,
					@print_level               = 0,
					@calling_proc_name         = @proc_name;

		SELECT @error_msg =  @ale_message
				,@error_severity = ERROR_SEVERITY()
				,@error_state	 = ERROR_STATE();
		RAISERROR(@error_msg, @error_severity, @error_state, @proc_name)
	END CATCH

	--add logging here
	set @ale_message = N'End: ' + @proc_name;
	set @ale_context = (SELECT @ale_message AS message, @proc_name AS context_value FOR XML PATH);

	EXEC p_ale_log @event_name            = N'PROC_FINISH', --ETL_ERROR --PROC_START --PROC_FINISH --CREATE_ADJUSTMENT
				@event_type                = N'I',
				@context                   = @ale_context,
				@message                   = @ale_message,
				@event_type_detail         = N'I',
				--@correlation_guid          = @ale_correlation_guid OUTPUT,
				--@timestamp_utc             = N'',
				--@event_log_guid            = @ale_event_log_guid OUTPUT,
				@use_synchronous_logging   = 0,
				@print_ind                 = 1,
				@print_level               = 0,
				@calling_proc_name         = @proc_name;

-- Return succes
RETURN (0) 

GO
IF EXISTS (SELECT * FROM sys.procedures WHERE name = N'p_stg_pop_ead_main_deal_1_point' AND modify_date > create_date AND modify_date > DATEADD(s, -1, CURRENT_TIMESTAMP) AND schema_id = SCHEMA_ID('dbo'))
BEGIN
    PRINT N'Procedure [dbo].[p_stg_pop_ead_main_deal_1_point] has been altered...'
END ELSE BEGIN
    PRINT N'Procedure [dbo].[p_stg_pop_ead_main_deal_1_point] has NOT been altered due to errors!'
END
GO
