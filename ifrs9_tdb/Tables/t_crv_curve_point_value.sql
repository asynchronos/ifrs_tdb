 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_crv_curve_point_value')
  BEGIN
    PRINT N'Dropping table [dbo].t_crv_curve_point_value ...'
    DROP TABLE [dbo].t_crv_curve_point_value
  END
GO
 
PRINT N'Creating table [dbo].t_crv_curve_point_value...'
-- ******************************************************************************
-- * Purpose: This table contains the values that are associated with a curve point for a given curve, for the period defined by the start validity date and end validity date. *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_crv_curve_point_value (
                                       curve_point_value_id      d_id                                 NOT NULL,
                                       curve_id                  d_id                                 NOT NULL
                                                                 CONSTRAINT uq1_crv_curve_point_value
                                                                 UNIQUE NONCLUSTERED   (curve_id,
                                                                                        curve_point_value_id,
                                                                                        end_validity_date),
                                       start_validity_date       d_validity_date                      NOT NULL
                                                                 CONSTRAINT pk_crv_curve_point_value
                                                                 PRIMARY KEY CLUSTERED (curve_id,
                                                                                        curve_point_value_id,
                                                                                        start_validity_date)
                                                                 CONSTRAINT df1_crv_curve_point_value
                                                                 DEFAULT ([dbo].[fn_min_date_default]()),
                                       end_validity_date         d_validity_date                      NOT NULL
                                                                 CONSTRAINT df2_crv_curve_point_value
                                                                 DEFAULT ([dbo].[fn_max_date_default]()),
                                       term_start_date           DATETIME                             NULL,
                                       term_nbr_of_time_units    d_nbr_of_units                       NULL,
                                       term_time_unit            d_time_unit                          NULL,
                                       value_perc                d_rate                               NULL,
                                       value_amount              d_amount                             NULL,
                                       value_origin              d_value_origin                       NOT NULL
                                                                 CONSTRAINT df3_crv_curve_point_value
                                                                 DEFAULT (N'INPUT'),
                                       interpolation_method      d_crv_interpolation_method           NULL,
                                       compounding_frequency     d_compounding_period                 NOT NULL
                                                                 CONSTRAINT df4_crv_curve_point_value
                                                                 DEFAULT (N'Continuous'),
                                       last_modified             d_last_modified                      NOT NULL
                                                                 CONSTRAINT df5_crv_curve_point_value
                                                                 DEFAULT (getdate()),
                                       modified_by               d_user                               NOT NULL
                                                                 CONSTRAINT df6_crv_curve_point_value
                                                                 DEFAULT ([dbo].[fn_user_default]())
                                     )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_crv_curve_point_value')
  BEGIN
    PRINT N'Table [dbo].t_crv_curve_point_value has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_crv_curve_point_value has NOT been created due to errors...'
  END
GO
