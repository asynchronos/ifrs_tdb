 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_pop_data_config_tbank')
  BEGIN
    PRINT N'Dropping table [dbo].t_pop_data_config_tbank ...'
    DROP TABLE [dbo].t_pop_data_config_tbank
  END
GO
 
PRINT N'Creating table [dbo].t_pop_data_config_tbank...'
-- ******************************************************************************
-- * Purpose:                                                                   *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_pop_data_config_tbank (
                                       pop_data_conf_id     d_id IDENTITY(1,1) NOT NULL
                                                            CONSTRAINT pk_pop_data_config_tbank
                                                            PRIMARY KEY NONCLUSTERED (pop_data_conf_id),
                                       scenario_id          d_id               NOT NULL
                                                            CONSTRAINT uq1_pop_data_config_tbank
                                                            UNIQUE NONCLUSTERED   (scenario_id,
                                                                                   value_type,
                                                                                   model_name,
                                                                                   src_table_name),
                                       scenario_name        d_name             NOT NULL,
                                       stage                d_id               NOT NULL,
                                       value_type           d_name             NOT NULL,
                                       model_name           d_name             NOT NULL,
                                       src_table_name       d_name             NOT NULL,
                                       src_field_name       d_name             NOT NULL,
                                       proc_name            d_name             NOT NULL,
                                       target_table_name    d_name             NULL,
                                       target_field_name    d_name             NULL,
                                       remark               d_name             NULL,
                                       last_modified        DATETIME           NOT NULL
                                                            CONSTRAINT df1_pop_data_config_tbank
                                                            DEFAULT (getdate()),
                                       modified_by          d_user             NOT NULL
                                                            CONSTRAINT df2_pop_data_config_tbank
                                                            DEFAULT ([dbo].[fn_user_default]())
                                     )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_pop_data_config_tbank')
  BEGIN
    PRINT N'Table [dbo].t_pop_data_config_tbank has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_pop_data_config_tbank has NOT been created due to errors...'
  END
GO
