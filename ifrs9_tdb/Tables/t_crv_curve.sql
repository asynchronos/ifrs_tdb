﻿ 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_crv_curve')
  BEGIN
    PRINT N'Dropping table [dbo].t_crv_curve ...'
    DROP TABLE [dbo].t_crv_curve
  END
GO
 
PRINT N'Creating table [dbo].t_crv_curve...'
-- ******************************************************************************
-- * Purpose: This table defines the different curves that apply in OneSumX, related to a number of curve types such as: yield curves, liquidity curves, etc.A curve might be composed of several components, for instance, it can be the combination of a base curve and a spread curve. In this case the composition of the different curve components is stored in table t_crv_curve_component. *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_crv_curve (
                           curve_id                      d_id                  IDENTITY(1,1) NOT NULL
                                                         CONSTRAINT pk_crv_curve
                                                         PRIMARY KEY NONCLUSTERED (curve_id),
                           curve_code                    d_curve_code                        NOT NULL
                                                         CONSTRAINT uq1_crv_curve
                                                         UNIQUE CLUSTERED   (curve_code),
                           curve_name                    d_name                              NOT NULL,
                           curve_desc                    d_description                       NOT NULL,
                           curve_type                    d_curve_type                        NOT NULL,
                           source_system                 d_source_system                     NOT NULL,
                           curve_begin_date              DATETIME                            NOT NULL,
                           curve_end_date                DATETIME                            NOT NULL,
                           curve_value_type              d_value_type                        NOT NULL
                                                         CONSTRAINT df1_crv_curve
                                                         DEFAULT (N'NA'),
                           usage_type                    d_usage_type                        NULL,
                           curve_reference               d_reference_code                    NULL,
                           forward_nbr_of_time_units     d_nbr_of_units                      NULL,
                           forward_time_unit             d_time_unit                         NULL,
                           currency                      d_currency                          NULL,
                           day_count_convention          d_day_count_convention              NULL,
                           business_day_convention       d_business_day_convention           NULL,
                           calendar                      d_calendar                          NULL,
                           is_defined_by_curve_points    d_bit                               NOT NULL,
                           is_locked                     d_bit                               NOT NULL
                                                         CONSTRAINT df2_crv_curve
                                                         DEFAULT ((0)),
                           char_cust_element1            d_char_cust_element1                NOT NULL
                                                         CONSTRAINT df5_crv_curve
                                                         DEFAULT (N''),
                           char_cust_element2            d_char_cust_element2                NOT NULL
                                                         CONSTRAINT df6_crv_curve
                                                         DEFAULT (N''),
                           char_cust_element3            d_char_cust_element3                NOT NULL
                                                         CONSTRAINT df7_crv_curve
                                                         DEFAULT (N''),
                           char_cust_element4            d_char_cust_element4                NOT NULL
                                                         CONSTRAINT df8_crv_curve
                                                         DEFAULT (N''),
                           char_cust_element5            d_char_cust_element5                NOT NULL
                                                         CONSTRAINT df9_crv_curve
                                                         DEFAULT (N''),
                           num_cust_element1             d_num_cust_element1                 NOT NULL
                                                         CONSTRAINT df10_crv_curve
                                                         DEFAULT ((0)),
                           num_cust_element2             d_num_cust_element2                 NOT NULL
                                                         CONSTRAINT df11_crv_curve
                                                         DEFAULT ((0)),
                           num_cust_element3             d_num_cust_element3                 NOT NULL
                                                         CONSTRAINT df12_crv_curve
                                                         DEFAULT ((0)),
                           num_cust_element4             d_num_cust_element4                 NOT NULL
                                                         CONSTRAINT df13_crv_curve
                                                         DEFAULT ((0)),
                           num_cust_element5             d_num_cust_element5                 NOT NULL
                                                         CONSTRAINT df14_crv_curve
                                                         DEFAULT ((0)),
                           last_modified                 d_last_modified                     NOT NULL
                                                         CONSTRAINT df3_crv_curve
                                                         DEFAULT (getdate()),
                           modified_by                   d_user                              NOT NULL
                                                         CONSTRAINT df4_crv_curve
                                                         DEFAULT ([dbo].[fn_user_default]())
                         )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_crv_curve')
  BEGIN
    PRINT N'Table [dbo].t_crv_curve has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_crv_curve has NOT been created due to errors...'
  END
GO
