 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_etl_adj_data_crv_curve_validation')
  BEGIN
    PRINT N'Dropping table [dbo].t_etl_adj_data_crv_curve_validation ...'
    DROP TABLE [dbo].t_etl_adj_data_crv_curve_validation
  END
GO
 
PRINT N'Creating table [dbo].t_etl_adj_data_crv_curve_validation...'
-- ******************************************************************************
-- * Purpose: This table holds the latest invalid records of the ETL flow set flow table on loan transactions. The invalid records are stored together with a validation severity level code. *
-- *                                                                            *
-- * Notes:                                                                     *
-- ******************************************************************************
 
CREATE TABLE [dbo].t_etl_adj_data_crv_curve_validation (
                                                       record_id                    d_id                         NOT NULL
                                                                                    CONSTRAINT pk_etl_adj_data_crv_curve_validation
                                                                                    PRIMARY KEY CLUSTERED (record_id),
                                                       adjustment_id                d_id                         NOT NULL,
                                                       line_nr                      d_id                         NOT NULL,
                                                       validated_column_list        d_long_description           NULL,
                                                       validation_severity_level    d_id                         NOT NULL,
                                                       validation_desc              d_long_description           NULL,
                                                       job_log_id                   d_id                         NOT NULL,
                                                       step_log_id                  d_id                         NOT NULL,
                                                       last_modified                d_last_modified              NOT NULL
                                                                                    CONSTRAINT df1_etl_adj_data_crv_curve_validation
                                                                                    DEFAULT (getdate()),
                                                       modified_by                  d_user                       NOT NULL
                                                                                    CONSTRAINT df2_etl_adj_data_crv_curve_validation
                                                                                    DEFAULT ([dbo].[fn_user_default]())
                                                     )
GO
 
 
IF EXISTS (select * from sys.tables where schema_id = (SELECT schema_id FROM sys.schemas WHERE name = 'dbo') AND name = N't_etl_adj_data_crv_curve_validation')
  BEGIN
    PRINT N'Table [dbo].t_etl_adj_data_crv_curve_validation has been created...'
  END
ELSE
  BEGIN
    PRINT N'Table [dbo].t_etl_adj_data_crv_curve_validation has NOT been created due to errors...'
  END
GO
