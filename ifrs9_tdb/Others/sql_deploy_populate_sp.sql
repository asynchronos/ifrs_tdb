﻿
-- create table
--:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Tables\t_etl_adj_data_crv_curve_validation.sql"
--:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Tables\t_pop_data_config_tbank.sql"

-- init data
--:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Initial_data\dat_t_pop_data_config_tbank.sql"

-- create main populate procedure
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_main.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_main_cc_fp.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_main_nrt_smes.sql"

:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\PD\p_stg_pit_pd_main.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\PD\p_stg_pit_pd_main_cc_fp.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\PD\p_stg_pit_pd_main_deal_1_point.sql"

:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\FLOW_SET_FLOW\EAD\p_stg_pop_ead_main_deal_1_point.sql"

:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\p_stg_pit_del_crv.sql"

-- create segment populate procedure
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_cc_fp.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\PD\p_stg_pit_pd_cc_fp.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\FLOW_SET_FLOW\EAD\p_stg_pop_ead_cc_fp.sql"

:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_hl.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\PD\p_stg_pit_pd_hl.sql"

:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_hp.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\PD\p_stg_pit_pd_hp.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\FLOW_SET_FLOW\EAD\p_stg_pop_ead_hp.sql"

:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_nonretail.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_smes.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\PD\p_stg_pit_pd_master_scale.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\PD\p_stg_pit_pd_nonretail_smes_cumulative.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\FLOW_SET_FLOW\EAD\p_stg_pop_ead_nonretail_smes.sql"

:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\p_stg_pit_pdlgd_constant.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_mrta.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_spl.sql"
:R "D:\bik\workspaces\ifrs9_tdb\ifrs9_tdb\Stored Procedures\CRV_CURVE\LGD\p_stg_pit_lgd_staff_hl.sql"

