﻿
declare @cursor_sp_name	d_name;

declare cleansing_sp_cursor cursor local static for 
select name
from sys.procedures
where schema_id = SCHEMA_ID('dbo')
--AND name like 'p_stg%' -- for list the name you want to find, please double check before add in the below list
AND name in ( -- now 65 procedure
 'p_stg_cc_pit_lgd_all'
,'p_stg_fp_pit_lgd_all'
,'p_stg_fp_pit_pd_all'
,'p_stg_hl_pit_lgd_all'
,'p_stg_hl_pit_lgd_stage2'
,'p_stg_hl_pit_lgd_stage2_BAU'
,'p_stg_hl_pit_pd_all'
,'p_stg_hp_cyc_pit_lgd_all'
,'p_stg_hp_cyc_pit_pd_all'
,'p_stg_hp_juristics_pit_pd_all'
,'p_stg_hp_new_pit_lgd_all'
,'p_stg_hp_new_pit_pd_all'
,'p_stg_hp_use_pit_lgd_all'
,'p_stg_hp_use_pit_pd_all'
,'p_stg_mrta_pit_lgd_all'
,'p_stg_pit_del_crv'
,'p_stg_pit_lgd'
,'p_stg_pit_lgd_cc'
,'p_stg_pit_lgd_cc_fp'
,'p_stg_pit_lgd_fp'
,'p_stg_pit_lgd_hl'
,'p_stg_pit_lgd_hp'
,'p_stg_pit_lgd_hp_cyc'
,'p_stg_pit_lgd_hp_new'
,'p_stg_pit_lgd_hp_use'
,'p_stg_pit_lgd_main'
,'p_stg_pit_lgd_main_cc_fp'
,'p_stg_pit_lgd_main_nrt_smes'
,'p_stg_pit_lgd_mrta'
,'p_stg_pit_lgd_nonretail'
,'p_stg_pit_lgd_smes'
,'p_stg_pit_lgd_spl'
,'p_stg_pit_lgd_staff'
,'p_stg_pit_lgd_staff_hl'
,'p_stg_pit_lgd_staff_hp'
,'p_stg_pit_pd'
,'p_stg_pit_pd_cc'
,'p_stg_pit_pd_cc_fp'
,'p_stg_pit_pd_constant'
,'p_stg_pit_pd_fp'
,'p_stg_pit_pd_hl'
,'p_stg_pit_pd_hp'
,'p_stg_pit_pd_hp_cyc'
,'p_stg_pit_pd_hp_juristics'
,'p_stg_pit_pd_hp_new'
,'p_stg_pit_pd_hp_use'
,'p_stg_pit_pd_hpju'
,'p_stg_pit_pd_main'
,'p_stg_pit_pd_main_cc_fp'
,'p_stg_pit_pd_main_deal_1_point'
,'p_stg_pit_pd_main_tm'
,'p_stg_pit_pd_master_scale'
,'p_stg_pit_pd_master_scale_cumulative'
,'p_stg_pit_pd_nonretail_cumulative'
,'p_stg_pit_pd_nonretail_smes_cumulative'
,'p_stg_pit_pd_smes'
,'p_stg_pit_pd_spl'
,'p_stg_pit_pd_tm'
,'p_stg_pit_pd_upl'
,'p_stg_pit_pdlgd_constant'
,'p_stg_pop_ead_cc_fp'
,'p_stg_pop_ead_hp'
,'p_stg_pop_ead_main_deal_1_point'
,'p_stg_pop_ead_nonretail_smes'
,'p_stg_smes_pit_pd_all'
,'p_stg_spl_pit_lgd_all'
,'p_stg_spl_pit_pd_all'
,'p_stg_staff_pit_lgd_all'
,'p_stg_upl_pit_pd_all'
)

open cleansing_sp_cursor
fetch next from cleansing_sp_cursor into @cursor_sp_name

-- looping for drop procedure
While @@FETCH_STATUS = 0
BEGIN
		IF EXISTS (SELECT * FROM sys.procedures WHERE name = @cursor_sp_name AND schema_id = SCHEMA_ID('dbo'))
		  BEGIN
		    PRINT N'Droping procedure [dbo].['+@cursor_sp_name+'] ...'
		    EXEC (N'DROP PROCEDURE [dbo].['+@cursor_sp_name+']')
		    WAITFOR DELAY N'00:00:00.003'
		  END
		PRINT N'Droped procedure [dbo].['+@cursor_sp_name+']...'
		--GO
		
	fetch next from cleansing_sp_cursor into @cursor_sp_name
END
		
close cleansing_sp_cursor
deallocate cleansing_sp_cursor