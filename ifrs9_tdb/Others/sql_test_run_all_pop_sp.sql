﻿
declare @reporting_date		datetime	= '2018-03-31';
declare @param_is_test		bit			= 1;

/*
-- delete all curve in live table
-- PD
EXEC p_stg_pit_del_crv @param_reporting_date   = @reporting_date,
                       @param_crv_type         = 8,
                       @param_is_test          = 0 
-- LGD
EXEC p_stg_pit_del_crv @param_reporting_date   = @reporting_date,
                       @param_crv_type         = 9,
                       @param_is_test          = 0 

-- delete all flow in live table 
-- *** only CC/FP, HP. for Non Retail the program support testing with generate "TEST-" prefix

-- delete t_flow of product xx,yy,zz
delete t_flow
where exists(
	select fs.flow_set_id
	from t_flow_set fs
	where exists(
		select distinct seg.item_object_key_value
		from t_ecl_segmentation seg
		where seg.segment_pool_id in (8,9,91,92,93,94,95) -- delete by pool that exists in t_ecl_segmentation
			and seg.item_object_key_value=fs.object_key_value
	) 
	and fs.flow_set_id=t_flow.flow_set_id
)
;
-- delete t_flow of product xx,yy,zz
delete t_flow_set
where exists(
	select distinct seg.item_object_key_value
	from t_ecl_segmentation seg
	where seg.segment_pool_id in (8,9,91,92,93,94,95) -- delete by pool that exists in t_ecl_segmentation
		and seg.item_object_key_value=t_flow_set.object_key_value
)

-- delete t_flow by char_cust_element3, for clear data from old procedure
-- use this only in FAT and SIT3
delete t_flow
where exists(
	select fs.flow_set_id
	from t_flow_set fs
	where char_cust_element3 in ('CC','FP','HP-CYC','HP-Juristic','HP-NEW','HP-USE','SMEs') -- Not delete NRT because if come from normal term loan
	and fs.flow_set_id=t_flow.flow_set_id
)
delete t_flow_set
where exists(
	select distinct seg.item_object_key_value
	from t_ecl_segmentation seg
	where char_cust_element3 in ('CC','FP','HP-CYC','HP-Juristic','HP-NEW','HP-USE','SMEs') -- Not delete NRT because if come from normal term loan
)

*/

---- move ead normal term loan
---- move to live table
---- run hms using auto cleanup
----declare @cob_date datetime	= '2018-03-31';
----declare @adjustment_id d_id = 11; -- NRT
--exec p_etl_adj_data_flow_tbank	@execution_date	= @reporting_date,
--								@cleanup_type = 2,
--								@cleanup_staging = 0,
--								@only_cleanup_changed = 1,
--								@use_delta_load = 1, 
--								@adjustment_id	= 11,--@adjustment_id,
--								@entity			= N'TBANK',  
--								@log_output     = 0
--;

-- move to live table
-- run hms using auto cleanup
--declare @cob_date datetime	= '2018-03-31';
--declare @adjustment_id d_id = 1; -- HL
--exec p_etl_adj_data_flow_tbank	@execution_date	= @reporting_date,
--								@cleanup_type = 2,
--								@cleanup_staging = 0,
--								@only_cleanup_changed = 1,
--								@use_delta_load = 1, 
--								@adjustment_id	= 1,--@adjustment_id,
--								@entity			= N'TBANK',  
--								@log_output     = 0
--;


------ start test all populate procedure
----[ECL_07.2 R_EAD_FP CCF]
--EXEC p_stg_pop_ead_cc_fp			@param_reporting_date   = @reporting_date,
									--@param_is_test          = @param_is_test


----[ECL_07.4 R_EAD_HP_GCA_Stage123]
--EXEC p_stg_pop_ead_hp				@param_reporting_date   = @reporting_date,
--									@param_is_test          = @param_is_test


--[ECL_09.1 R_TM_PD_TO_T_CURVE]									
EXEC p_stg_pit_pdlgd_constant		@param_reporting_date   = @reporting_date,
									@param_is_test          = @param_is_test

EXEC p_stg_pit_pd_cc_fp				@param_reporting_date   = @reporting_date,
									@param_is_test          = @param_is_test

EXEC p_stg_pit_pd_hl				@param_reporting_date   = @reporting_date,
									@param_is_test          = @param_is_test

EXEC p_stg_pit_pd_hp				@param_reporting_date   = @reporting_date,
									@param_is_test          = @param_is_test



--[ECL_09.2 R_LGD_TO_T_CURVE]
EXEC p_stg_pit_lgd_cc_fp			@param_reporting_date   = @reporting_date,
									@param_is_test          = @param_is_test

EXEC p_stg_pit_lgd_hl				@param_reporting_date   = @reporting_date,
									@param_is_test          = @param_is_test

EXEC p_stg_pit_lgd_hp				@param_reporting_date   = @reporting_date,
									@param_is_test          = @param_is_test

EXEC p_stg_pit_lgd_mrta				@param_reporting_date   = @reporting_date,
									@param_is_test          = @param_is_test

EXEC p_stg_pit_lgd_spl				@param_reporting_date   = @reporting_date,
									@param_is_test          = @param_is_test

EXEC p_stg_pit_lgd_staff_hl			@param_reporting_date   = @reporting_date,
									@param_is_test          = @param_is_test


----[ECL_11.0 NR_PD]
EXEC p_stg_pit_pd_master_scale		@param_reporting_date   = @reporting_date, 
									@param_is_test          = @param_is_test

EXEC p_stg_pit_pd_nonretail_smes_cumulative		@param_reporting_date   = @reporting_date, 
									@param_is_test          = @param_is_test


----[ECL_12.0 NR_LGD]
EXEC p_stg_pit_lgd_nonretail		@param_reporting_date   = @reporting_date, 
									@param_is_test          = @param_is_test

EXEC p_stg_pit_lgd_smes				@param_reporting_date   = @reporting_date, 
									@param_is_test          = @param_is_test


----[ECL_13.1 NR_EAD_NonRetail_SMEs]
--EXEC p_stg_pop_ead_nonretail_smes	@param_reporting_date   = @reporting_date, 
--									@param_is_test          = @param_is_test


