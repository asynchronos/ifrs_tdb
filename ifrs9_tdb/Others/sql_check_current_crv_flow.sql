﻿
declare @cob_date	datetime	=	'2018-03-31';

select crv.char_cust_element3, crv.char_cust_element2, count(crv.curve_id) cnt_curve_code
from t_crv_curve crv
where @cob_date between crv.curve_begin_date and crv.curve_end_date
group by crv.char_cust_element3, crv.char_cust_element2
;


select fs.char_cust_element3, fs.char_cust_element2, count(fs.flow_set_id) cnt_flow_set
from t_flow_set fs
where @cob_date between fs.start_validity_date and fs.end_validity_date
group by fs.char_cust_element3, fs.char_cust_element2
;

