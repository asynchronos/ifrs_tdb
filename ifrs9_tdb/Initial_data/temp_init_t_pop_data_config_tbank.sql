
select 'Before Insert..' as pre_insert
select *
from  t_pop_data_config_tbank
;
/*
scenario_id	scenario_name
3	Stage 1 BAU
4	Stage 1 DOWNTURN
5	Stage 1 UPTURN
51	Stage 1 NON FL
54	Stage 2 Specific Treatment Group BAU
55	Stage 2 Specific Treatment Group DOWNTURN
56	Stage 2 Specific Treatment Group UPTURN
57	Stage 2 Specific Treatment Group NON FL
110	Stage 3 Specific Treatment Group

case s.scenario_name
	when 'Stage 1 BAU'										then	'pd_flat_stage1'
	when 'Stage 1 DOWNTURN'									then	'pd_worst_stage1'
	when 'Stage 1 UPTURN'									then	'pd_best_stage1'
	when 'Stage 1 NON FL'									then	'pd_stage1'
	when 'Stage 2 Specific Treatment Group BAU'				then	'pd_flat_stage2'
	when 'Stage 2 Specific Treatment Group DOWNTURN'		then	'pd_worst_stage2'
	when 'Stage 2 Specific Treatment Group UPTURN'			then	'pd_best_stage2'
	when 'Stage 2 Specific Treatment Group NON FL'			then	'pd_stage2'
	when 'Stage 3 Specific Treatment Group'					then	'pd_stage2'
	else 'unknown' end as src_field_name

*/

declare @value_type				d_name		= --N'PD';
											  --N'LGD';
											  N'EAD';
declare @model_name				d_name		= N'NonRetail&SMEs';
											  --N'HP';
declare @proc_name				d_name		= N'p_stg_pop_ead_nonretail_smes';
declare @src_table_name1		d_name		= N't_nonretail_ead_tbank';
declare @src_table_name2		d_name		= N't_nonretail_ead_tbank';
declare @src_table_name3		d_name		= N't_nonretail_ead_tbank';
declare @src_field_name_sce3	d_name		= N'lgd';
declare @src_field_name_sce4	d_name		= N'lgd';
declare @src_field_name_sce5	d_name		= N'lgd';
declare @src_field_name_sce51	d_name		= N'ead_stage1';
declare @src_field_name_sce54	d_name		= N'lgd';
declare @src_field_name_sce55	d_name		= N'lgd';
declare @src_field_name_sce56	d_name		= N'lgd';
declare @src_field_name_sce57	d_name		= N'ead_stage2';
declare @src_field_name_sce110	d_name		= N'ead_stage3';
declare @target_table_name		d_name		= N't_etl_adj_data_flow_set_flow';
declare @target_field_name		d_name		= --N'value_perc1';
											  --N'value_amount1';
											  N'amount';
declare @remark					d_name		= null;
											  --N'It is a PD Cumulative at deal level for Non Retail&SMEs';

-- [T-SQL Insert Query:] (vertical)
------------------------------------------------------------------------------------------------------------------------
--INSERT INTO t_pop_data_config_tbank
--       (scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name, proc_name, target_table_name, target_field_name, remark, last_modified, modified_by)
--SELECT scenario_id         = 3,
--       scenario_name       = N'Stage 1 BAU',
--       stage               = 1,
--       value_type          = @value_type,
--       model_name          = @model_name,
--       src_table_name      = @src_table_name1,
--       src_field_name      = @src_field_name_sce3,
--       proc_name           = @proc_name,
--       target_table_name   = @target_table_name,
--       target_field_name   = @target_field_name,
--       remark              = @remark,
--       last_modified       = CURRENT_TIMESTAMP,
--       modified_by         = dbo.fn_user() 
--;
---- [T-SQL Insert Query:] (vertical)
--------------------------------------------------------------------------------------------------------------------------
--INSERT INTO t_pop_data_config_tbank
--       (scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name, proc_name, target_table_name, target_field_name, remark, last_modified, modified_by)
--SELECT scenario_id         = 4,
--       scenario_name       = N'Stage 1 DOWNTURN',
--       stage               = 1,
--       value_type          = @value_type,
--       model_name          = @model_name,
--       src_table_name      = @src_table_name1,
--       src_field_name      = @src_field_name_sce4,
--       proc_name           = @proc_name,
--       target_table_name   = @target_table_name,
--       target_field_name   = @target_field_name,
--       remark              = @remark,
--       last_modified       = CURRENT_TIMESTAMP,
--       modified_by         = dbo.fn_user() 
--;
---- [T-SQL Insert Query:] (vertical)
--------------------------------------------------------------------------------------------------------------------------
--INSERT INTO t_pop_data_config_tbank
--       (scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name, proc_name, target_table_name, target_field_name, remark, last_modified, modified_by)
--SELECT scenario_id         = 5,
--       scenario_name       = N'Stage 1 UPTURN',
--       stage               = 1,
--       value_type          = @value_type,
--       model_name          = @model_name,
--       src_table_name      = @src_table_name1,
--       src_field_name      = @src_field_name_sce5,
--       proc_name           = @proc_name,
--       target_table_name   = @target_table_name,
--       target_field_name   = @target_field_name,
--       remark              = @remark,
--       last_modified       = CURRENT_TIMESTAMP,
--       modified_by         = dbo.fn_user() 
--;
-- [T-SQL Insert Query:] (vertical)
------------------------------------------------------------------------------------------------------------------------
INSERT INTO t_pop_data_config_tbank
       (scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name, proc_name, target_table_name, target_field_name, remark, last_modified, modified_by)
SELECT scenario_id         = 51,
       scenario_name       = N'Stage 1 NON FL',
       stage               = 1,
       value_type          = @value_type,
       model_name          = @model_name,
       src_table_name      = @src_table_name1,
       src_field_name      = @src_field_name_sce51,
       proc_name           = @proc_name,
       target_table_name   = @target_table_name,
       target_field_name   = @target_field_name,
       remark              = @remark,
       last_modified       = CURRENT_TIMESTAMP,
       modified_by         = dbo.fn_user() 
;
---- [T-SQL Insert Query:] (vertical)
--------------------------------------------------------------------------------------------------------------------------
--INSERT INTO t_pop_data_config_tbank
--       (scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name, proc_name, target_table_name, target_field_name, remark, last_modified, modified_by)
--SELECT scenario_id         = 54,
--       scenario_name       = N'Stage 2 Specific Treatment Group BAU',
--       stage               = 2,
--       value_type          = @value_type,
--       model_name          = @model_name,
--       src_table_name      = @src_table_name2,
--       src_field_name      = @src_field_name_sce54,
--       proc_name           = @proc_name,
--       target_table_name   = @target_table_name,
--       target_field_name   = @target_field_name,
--       remark              = @remark,
--       last_modified       = CURRENT_TIMESTAMP,
--       modified_by         = dbo.fn_user() 
--;
---- [T-SQL Insert Query:] (vertical)
--------------------------------------------------------------------------------------------------------------------------
--INSERT INTO t_pop_data_config_tbank
--       (scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name, proc_name, target_table_name, target_field_name, remark, last_modified, modified_by)
--SELECT scenario_id         = 55,
--       scenario_name       = N'Stage 2 Specific Treatment Group DOWNTURN',
--       stage               = 2,
--       value_type          = @value_type,
--       model_name          = @model_name,
--       src_table_name      = @src_table_name2,
--       src_field_name      = @src_field_name_sce55,
--       proc_name           = @proc_name,
--       target_table_name   = @target_table_name,
--       target_field_name   = @target_field_name,
--       remark              = @remark,
--       last_modified       = CURRENT_TIMESTAMP,
--       modified_by         = dbo.fn_user() 
--;
---- [T-SQL Insert Query:] (vertical)
--------------------------------------------------------------------------------------------------------------------------
--INSERT INTO t_pop_data_config_tbank
--       (scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name, proc_name, target_table_name, target_field_name, remark, last_modified, modified_by)
--SELECT scenario_id         = 56,
--       scenario_name       = N'Stage 2 Specific Treatment Group UPTURN',
--       stage               = 2,
--       value_type          = @value_type,
--       model_name          = @model_name,
--       src_table_name      = @src_table_name2,
--       src_field_name      = @src_field_name_sce56,
--       proc_name           = @proc_name,
--       target_table_name   = @target_table_name,
--       target_field_name   = @target_field_name,
--       remark              = @remark,
--       last_modified       = CURRENT_TIMESTAMP,
--       modified_by         = dbo.fn_user() 
--;
-- [T-SQL Insert Query:] (vertical)
------------------------------------------------------------------------------------------------------------------------
INSERT INTO t_pop_data_config_tbank
       (scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name, proc_name, target_table_name, target_field_name, remark, last_modified, modified_by)
SELECT scenario_id         = 57,
       scenario_name       = N'Stage 2 Specific Treatment Group NON FL',
       stage               = 2,
       value_type          = @value_type,
       model_name          = @model_name,
       src_table_name      = @src_table_name2,
       src_field_name      = @src_field_name_sce57,
       proc_name           = @proc_name,
       target_table_name   = @target_table_name,
       target_field_name   = @target_field_name,
       remark              = @remark,
       last_modified       = CURRENT_TIMESTAMP,
       modified_by         = dbo.fn_user() 
;
-- [T-SQL Insert Query:] (vertical)
------------------------------------------------------------------------------------------------------------------------
INSERT INTO t_pop_data_config_tbank
       (scenario_id, scenario_name, stage, value_type, model_name, src_table_name, src_field_name, proc_name, target_table_name, target_field_name, remark, last_modified, modified_by)
SELECT scenario_id         = 110,
       scenario_name       = N'Stage 3 Specific Treatment Group',
       stage               = 3,
       value_type          = @value_type,
       model_name          = @model_name,
       src_table_name      = @src_table_name3,
       src_field_name      = @src_field_name_sce110,
       proc_name           = @proc_name,
       target_table_name   = @target_table_name,
       target_field_name   = @target_field_name,
       remark              = @remark,
       last_modified       = CURRENT_TIMESTAMP,
       modified_by         = dbo.fn_user() 
;

select 'After Inserted..' as post_insert
select *
from  t_pop_data_config_tbank
;

